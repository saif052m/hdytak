package com.deecoders.hdytak.util;

import android.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by Usman K on 6/23/2017.
 */

public class Constants {
    public static String TAG = "tag";
    public static final String backgrounds = "https://www.hdytak.com/en/Mobile/backgrounds";
    public static final String portraitImage = "https://www.hdytak.com/uploads/mobile_app_uploads/640x1024_intro.png";
    public static final String landscapeImage = "https://www.hdytak.com/uploads/mobile_app_uploads/1024x640_intro.png";
    public static final String icons = "https://www.hdytak.com/uploads/mobile_app_uploads";
    public static final String colors = "https://www.hdytak.com/en/Mobile/app_colors";
    public static final String activeContent = "https://www.hdytak.com/en/Mobile/active_content";
    public static final String signup = "https://www.hdytak.com/en/Mobile/signup";
    public static final String signin = "https://www.hdytak.com/en/Mobile/signin";
    public static final String social_sigin = "https://www.hdytak.com/en/Mobile/social_sigin";
    public static final String forgotPassword = "https://www.hdytak.com/en/Mobile/forgot_password";
    public static final String startCampaign = "https://www.hdytak.com/en/Mobile/start_campaign";
    public static final String product_categories = "https://www.hdytak.com/en/Mobile/product_categories";
    public static final String product_sub_categories = "https://www.hdytak.com/en/Mobile/product_sub_categories";
    public static final String products = "https://www.hdytak.com/en/Mobile/products";
    public static final String campaigns = "https://www.hdytak.com/en/Mobile/campaigns";
    public static final String product = "https://www.hdytak.com/en/Mobile/product";
    public static final String productCroppedUrl = "https://www.hdytak.com/uploads/products_uploads/350x350/";
    public static final String productOriginalUrl = "https://www.hdytak.com/uploads/products_uploads/";
    public static final String product_gallery = "https://www.hdytak.com/en/Mobile/product_gallery";
    public static final String productGalleryCroppedUrl= "https://www.hdytak.com/uploads/products_gallery_uploads/350x350/";
    public static final String productGalleryOriginalUrl = "https://www.hdytak.com/uploads/products_gallery_uploads/";
    public static final String product_rating = "https://www.hdytak.com/en/Mobile/product_rate";
    public static final String product_accessories = "https://www.hdytak.com/en/Mobile/product_accessories";
    public static final String add_to_campaign = "https://www.hdytak.com/en/Mobile/add_to_campaign";
    public static final String remove_from_campaign = "https://www.hdytak.com/en/Mobile/remove_from_campaign";
    public static final String remove_item_from_campaign = "https://www.hdytak.com/en/Mobile/remove_item_from_campaign";
    public static final String add_to_campaign_with_accessories = "https://www.hdytak.com/en/Mobile/add_to_campaign_with_accessories";
    public static final String campaign_categories = "https://www.hdytak.com/en/Mobile/campaign_categories";
    public static final String campaign_sub_categories = "https://www.hdytak.com/en/Mobile/campaign_sub_categories";
    public static final String campaign_search = "https://www.hdytak.com/en/Mobile/campaign_search";
    public static final String user_account = "https://www.hdytak.com/en/Mobile/user_account";
    public static final String update_account = "https://www.hdytak.com/en/Mobile/update_account";
    public static final String countries = "https://www.hdytak.com/en/Mobile/countries";
    public static final String add_item_to_campaign = "https://www.hdytak.com/en/Mobile/add_item_to_campaign";
    public static final String remove_product_from_campaign = "https://www.hdytak.com/en/Mobile/remove_product_from_campaign";
    public static final String camaign_total_paid_remaining = "https://www.hdytak.com/en/Mobile/camaign_total_paid_remaining";
    public static final String croppedProductImageUrl = "https://www.hdytak.com/uploads/products_uploads/350x350/";
    public static final String originalProductImageUrl = "https://www.hdytak.com/uploads/products_uploads/";
    public static final String croppedItemsImageUrl = "https://www.hdytak.com/uploads/campaign_products_uploads/350x350/";
    public static final String originalItemsImageUrl = "https://www.hdytak.com/uploads/campaign_products_uploads/";
    public static final String edit_campaign = "https://www.hdytak.com/en/Mobile/edit_campaign";
    public static final String send_email_invitations = "https://www.hdytak.com/en/Mobile/send_email_invitations";
    public static final String send_sms_invitation = "https://www.hdytak.com/en/Mobile/send_sms_invitation";
    public static final String pay_campaign = "https://www.hdytak.com/en/Mobile/pay_campaign";
    public static final String payment_return = "https://www.hdytak.com/en/Mobile/payment_return";
    public static final String paypal_return = "https://www.hdytak.com/en/Mobile/paypal_return";
    public static final String payment_result = "https://www.hdytak.com/en/Mobile/payment_result";
    public static final String terms = "https://www.hdytak.com/en/Mobile/terms_conditions";
    public static final String endCampaign = "https://www.hdytak.com/en/Mobile/end_campaign";
    public static final String campaign_details = "https://www.hdytak.com/en/Mobile/campaign_details";
    public static final String campaignImageUrl = "https://www.hdytak.com/uploads/campaign_uploads/";
    public static final String campaign_products = "https://www.hdytak.com/en/Mobile/campaign_products";
    public static final String productShareUrl = "https://www.hdytak.com/en/products/details/";
    public static final String campaignShareUrl = "https://www.hdytak.com/en/campaign/details/";
    public static final String campaign_payments = "https://www.hdytak.com/en/Mobile/campaign_payments";
    public static final String campaign_details_by_url = "https://www.hdytak.com/en/Mobile/campaign_details_by_url";
    public static final String all_campaigns_paginate = "https://www.hdytak.com/en/Mobile/all_campaigns_paginate/";
    public static final String all_campaigns = "https://www.hdytak.com/en/Mobile/all_campaigns";
    public static final String send_review_url = "https://www.hdytak.com/en/Mobile/send_review";

    public static HashMap<String, String> getSHA256() {
        HashMap<String, String> hashMap = new HashMap<>();
        String username = "baarr@%hra#!taaa";
        String password = "aa3zwht!@taa#wleea";
        int code = new Random().nextInt(99999)+new Random().nextInt(99999);
        String newkey = "decryptkey="+code+"username="+username+"access_password="+password+"toaccessdata";
        MessageDigest digest=null;
        String hash;
        try {
            digest = MessageDigest.getInstance("SHA-256");
            digest.update(newkey.getBytes());
            hash = bytesToHexString(digest.digest());

            hashMap.put("username", username);
            hashMap.put("access_password", password);
            hashMap.put("key", ""+code);
            hashMap.put("signature", hash);

            return hashMap;
        } catch (NoSuchAlgorithmException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        return null;
    }

    private static String bytesToHexString(byte[] bytes) {
        // http://stackoverflow.com/questions/332079
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    public static String getFormatedAmount(String amount_){
        Log.e("tag", "input: "+amount_);
        if(amount_ == null){
            return "0.00";
        }
        else if(amount_.isEmpty()) {
            amount_ = "0.00";
            return amount_;
        }
        else if(amount_.startsWith("0")){
            amount_ = "0.00";
            return amount_;
        }
        amount_ = amount_.replace(",","");
        double amount = Double.parseDouble(amount_);
        DecimalFormat formatter = new DecimalFormat("#,###.00");
        String formatted = formatter.format(amount);
        Log.e("tag", "output: "+formatted);
        return formatted;
    }
}