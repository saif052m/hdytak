package com.deecoders.hdytak.util;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.deecoders.hdytak.R;
import com.deecoders.hdytak.activity.Splash;
import com.deecoders.hdytak.activity.Transparent;
import com.deecoders.hdytak.receiver.MyReceiver;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Calendar;
import java.util.Random;

/**
 * Created by Kunwar's on 27-Jan-17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e("message", "new message came");
        if(remoteMessage.getData() != null){
            String msg = remoteMessage.getData().get("alert_message");
            if(msg != null)
                showSimpleNotification(remoteMessage);
        }
    }

    public void showSimpleNotification(RemoteMessage remoteMessage){
        Log.e("message", "notification came");
        // show transparent screen
        Intent b = new Intent(this, Transparent.class);
        b.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(b);

        // on click open activities screen
        Intent intent = new Intent(this, Splash.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Random r = new Random();
        PendingIntent pendingIntent = PendingIntent.getActivity(this, r.nextInt(10000), intent, PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
        notificationBuilder.setContentTitle("HDYTAK: Payment Alert");
        notificationBuilder.setContentText(remoteMessage.getData().get("alert_message"));
        notificationBuilder.setAutoCancel(true);
        notificationBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
        notificationBuilder.setVibrate(new long[] {1000, 1000});
        //notificationBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setColor(getResources().getColor(R.color.textColorBlue));
            notificationBuilder.setSmallIcon(R.mipmap.gift);
            notificationBuilder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.app_icon));
        }
        else {
            notificationBuilder.setSmallIcon(R.drawable.app_icon);
        }
        notificationBuilder.setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(r.nextInt(10000), notificationBuilder.build());
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.e("tag", "onTaskRemoved");
        Intent myIntent = new Intent(getBaseContext(), MyReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(), 0, myIntent, 0);
        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.SECOND, 10);
        long interval = 6 * 1000; //
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), interval, pendingIntent);
        super.onTaskRemoved(rootIntent);
    }
}
