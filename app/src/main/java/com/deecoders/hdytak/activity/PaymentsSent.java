package com.deecoders.hdytak.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.deecoders.hdytak.R;
import com.deecoders.hdytak.adapter.MyCampaignProductAdapter;
import com.deecoders.hdytak.model.CampaignModel;
import com.deecoders.hdytak.model.ProductModel;
import com.deecoders.hdytak.network.CustomRequest;
import com.deecoders.hdytak.network.LruBitmapCache;
import com.deecoders.hdytak.network.VolleyLibrary;
import com.deecoders.hdytak.util.CircularNetworkImageView;
import com.deecoders.hdytak.util.Constants;
import com.deecoders.hdytak.util.MyPref;
import com.deecoders.hdytak.view.ExpandableHeightGridView;
import com.deecoders.hdytak.view.MyEditText;
import com.deecoders.hdytak.view.MyTextView;
import com.devspark.appmsg.AppMsg;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentsSent extends BaseActivity {
    ArrayList<ProductModel> productModels = new ArrayList<>();
    MyCampaignProductAdapter productAdapter;
    @BindView(R.id.profile_image)
    CircularNetworkImageView profileImage;
    @BindView(R.id.title)
    MyTextView title;
    @BindView(R.id.event)
    MyTextView event;
    @BindView(R.id.gridView)
    ExpandableHeightGridView gridView;
    @BindView(R.id.total)
    MyTextView total;
    @BindView(R.id.paid)
    MyTextView paid;
    @BindView(R.id.remaining)
    MyTextView remaining;
    @BindView(R.id.sent_item)
    MyTextView sentItem;
    @BindView(R.id.container)
    ViewGroup container;
    @BindView(R.id.email)
    MyEditText email;
    @BindView(R.id.send_email)
    MyTextView sendEmail;
    @BindView(R.id.name)
    MyEditText name;
    @BindView(R.id.code)
    ViewGroup code;
    @BindView(R.id.country_code)
    BetterSpinner country;
    @BindView(R.id.mobile)
    MyEditText mobile;
    @BindView(R.id.send_sms)
    MyTextView sendSms;
    CampaignModel campaignModel;
    @BindView(R.id.codeTxt)
    MyTextView codeTxt;
    ArrayList<String> countryNames = new ArrayList<>();
    ArrayList<String> countryCodes = new ArrayList<>();
    ArrayList<String> countryIds = new ArrayList<>();
    boolean foreground;
    int selectedCodeIndex = -1;

    @BindView(R.id.totalPanel)
    LinearLayout totalPanel;
    @BindView(R.id.paidPanel)
    LinearLayout paidPanel;
    @BindView(R.id.remainingPanel)
    LinearLayout remainingPanel;
    @BindView(R.id.line)
    View line;
    String show_paid_ = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        replaceContentLayout(R.layout.activity_payments_sent);

        ButterKnife.bind(this);

        ViewGroup main = (ViewGroup) findViewById(R.id.main);

        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            //code for portrait mode
            getWindow().setBackgroundDrawableResource(R.drawable.background);
        } else {
            //code for landscape mode
            getWindow().setBackgroundDrawableResource(R.drawable.landscape);
        }

        campaignModel = (CampaignModel) getIntent().getSerializableExtra("camp_model");
        if(campaignModel == null){
            AppMsg.makeText(this, "Unable to load campaign data!", AppMsg.STYLE_ALERT).show();
            return;
        }

        campaignDetails();
        campaignProducts();
        campaignTotalPaidRemaining();
        sentPayments();
        loadingCountries();

        sendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (email.getText().toString().isEmpty()) {
                    AppMsg.makeText(PaymentsSent.this, "Enter email!", AppMsg.STYLE_ALERT).show();
                    return;
                }
                sendEmailOnServer();
            }
        });

        sendSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (name.getText().toString().isEmpty()) {
                    AppMsg.makeText(PaymentsSent.this, "Enter name!", AppMsg.STYLE_ALERT).show();
                } else if (country.getText().toString().isEmpty()) {
                    AppMsg.makeText(PaymentsSent.this, "Select country!", AppMsg.STYLE_ALERT).show();
                } else if (!countryNames.contains(country.getText().toString())) {
                    AppMsg.makeText(PaymentsSent.this, "Invalid country!", AppMsg.STYLE_ALERT).show();
                } else if (mobile.getText().toString().isEmpty()) {
                    AppMsg.makeText(PaymentsSent.this, "Enter mobile!!", AppMsg.STYLE_ALERT).show();
                } else {
                    sendSmsOnServer();
                }
            }
        });
    }

    private void sentPayments() {
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("campaign_id", ""+campaignModel.getId());
        hashMap.put("member_id", ""+MyPref.getId(this));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.campaign_payments, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("sent", response.toString());
                try {
                    String status = response.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray payments = response.getJSONArray("payments_sent");
                        StringBuilder builder = new StringBuilder();
                        for (int i = 0; i < payments.length(); i++) {
                            String amount = payments.getJSONObject(i).getString("amount");
                            String name = payments.getJSONObject(i).getString("name");
                            builder.append(name+": "+String.format("%.2f", Double.parseDouble(amount))+"$");
                            builder.append("\n");
                        }

                        if(payments.length() != 0)
                            sentItem.setText(builder.toString());
                        else
                            sentItem.setText("No payments yet");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (country != null)
                            loadingCountries();
                    }
                }, 1000);
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueueWithoutInternetCheck(customRequest, "", false);
    }

    private void sendSmsOnServer() {
        showProgress(false);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("campaign_id", "" + campaignModel.getId());
        hashMap.put("name", "" + name.getText().toString());
        int index = countryNames.indexOf(country.getText().toString());
        hashMap.put("country", "" + countryIds.get(index));
        String code = countryCodes.get(index);
        hashMap.put("phone",  code + mobile.getText().toString());

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.send_sms_invitation, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(false);
                try {
                    String status = response.getString("status");
                    String message = response.getString("message");
                    if (status.equalsIgnoreCase("success")) {
                        AppMsg.makeText(PaymentsSent.this, "" + message, AppMsg.STYLE_INFO).show();
                    } else {
                        AppMsg.makeText(PaymentsSent.this, "" + message, AppMsg.STYLE_ALERT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    private void sendEmailOnServer() {
        showProgress(false);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("campaign_id", "" + campaignModel.getId());
        hashMap.put("email", "" + email.getText().toString());

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.send_email_invitations, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(false);
                try {
                    String status = response.getString("status");
                    String message = response.getString("message");
                    if (status.equalsIgnoreCase("success")) {
                        AppMsg.makeText(PaymentsSent.this, "" + message, AppMsg.STYLE_INFO).show();
                    } else {
                        AppMsg.makeText(PaymentsSent.this, "" + message, AppMsg.STYLE_ALERT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile("[a-zA-Z0-9+._%-+]{1,256}" +
            "@" +
            "[a-zA-Z0-9][a-zA-Z0-9-]{0,64}" +
            "(" +
            "." +
            "[a-zA-Z0-9][a-zA-Z0-9-]{0,25}" +
            ")+");

    public static boolean checkEmail(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    public void finish(View view) {
        finish();
    }

    public void openMenu(View view) {
        super.drawerToggle();
    }

    public void shareProduct(View view) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, campaignModel.getTitle());
        i.putExtra(Intent.EXTRA_TEXT, Constants.campaignShareUrl + campaignModel.getUrlTitle());
        startActivity(Intent.createChooser(i, "Share Campaign"));
    }

    private void loadingCountries() {
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.countries, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("loadingCountries", response.toString());
                try {
                    if (countryNames.size() > 0) countryNames.clear();
                    if (countryCodes.size() > 0) countryCodes.clear();
                    countryIds.clear();

                    String status = response.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray countries = response.getJSONArray("countries");
                        for (int i = 0; i < countries.length(); i++) {
                            String countryId = countries.getJSONObject(i).getString("id");
                            String country = countries.getJSONObject(i).getString("title");
                            String code = countries.getJSONObject(i).getString("phonecode");
                            countryNames.add(country);
                            countryCodes.add(code);
                            countryIds.add(countryId);
                        }
                        //CountryAdapter adapter = new CountryAdapter(PaymentsSent.this, R.layout.country, R.id.title, countryNames);
                        //ArrayAdapter<> adapter1 = new ArrayAdapter(PaymentsSent.this, )
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(PaymentsSent.this, android.R.layout.simple_spinner_dropdown_item, countryNames);
                        country.setAdapter(dataAdapter);
                        //Log.e("tag", "country: "+MyPref.getCountry(getApplicationContext()));
                        //int index = countryNames.indexOf(MyPref.getCountry(getApplicationContext()));
                        //country.setSelection(index);
                        /*country.setOnItemClickListener(new A54dapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Log.e("tag", "click");
                                String cntry = country.getText().toString();
                                int index = countryNames.indexOf(cntry);
                                codeTxt.setText(countryCodes.get(index));
                            }
                        });*/
                        code.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showCodesDialog(1);
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("loadingCountries", error.toString());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (country != null)
                            loadingCountries();
                    }
                }, 1000);
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueueWithoutInternetCheck(customRequest, "", false);
    }

    private void showCodesDialog(final int type) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose");
        String[] codes = new String[countryCodes.size()];
        for (int i = 0; i < countryCodes.size(); i++) {
            codes[i] = countryCodes.get(i);
        }
        builder.setSingleChoiceItems(codes, selectedCodeIndex, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedCodeIndex = which;
                if(type == 1) {
                    codeTxt.setText(countryCodes.get(which));
                    //country.setText(countryNames.get(which));
                }
                dialog.dismiss();
            }
        });
        builder.create();
        builder.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        foreground = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        foreground = false;
    }

    private void campaignDetails() {
        showProgress(true);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("campaign_id", "" + campaignModel.getId());

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.campaign_details, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(true);
                try {
                    JSONObject data = response.getJSONObject("campaign");
                    title.setText(data.getString("title"));
                    event.setText(data.getString("description"));
                    RequestQueue mRequestQueue = Volley.newRequestQueue(PaymentsSent.this);
                    ImageLoader mImageLoader = new ImageLoader(mRequestQueue, new LruBitmapCache());
                    profileImage.setImageUrl(Constants.campaignImageUrl + data.getString("image"), mImageLoader);

                    show_paid_ = data.getString("show_paid");
                } catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    private void campaignProducts() {
        showProgress(false);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("campaign_id", "" + campaignModel.getId());
        hashMap.put("member_id", "" + MyPref.getId(this));

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.campaign_products, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(false);
                try {
                    String status = response.getString("status");
                    if (status.equals("success")) {
                        productModels.clear();
                        JSONArray products = response.getJSONArray("campaign_products");
                        JSONArray items = response.getJSONArray("campaign_items");
                        for (int i = 0; i < items.length(); i++) {
                            //id, created_date, title, salling_price, description, image;
                            ProductModel model = new ProductModel();
                            model.setId(items.getJSONObject(i).getString("id"));
                            model.setCreated_date(items.getJSONObject(i).getString("created_date"));
                            model.setTitle(items.getJSONObject(i).getString("title"));
                            model.setSalling_price(items.getJSONObject(i).getString("salling_price"));
                            model.setDescription(items.getJSONObject(i).getString("description"));
                            model.setImage(items.getJSONObject(i).getString("image"));
                            model.setType(1);
                            productModels.add(model);
                        }
                        for (int i = 0; i < products.length(); i++) {
                            //id, created_date, title, salling_price, description, image;
                            ProductModel model = new ProductModel();
                            model.setId(products.getJSONObject(i).getString("id"));
                            model.setCreated_date(products.getJSONObject(i).getString("created_date"));
                            model.setTitle(products.getJSONObject(i).getString("title"));
                            model.setSalling_price(products.getJSONObject(i).getString("price"));
                            model.setDescription(products.getJSONObject(i).getString("description"));
                            model.setImage(products.getJSONObject(i).getString("image"));
                            model.setType(0);
                            productModels.add(model);
                        }

                        Log.e("tag", "size: " + productModels.size());

                        if(productModels.size()==1)
                            gridView.setNumColumns(1);
                        else
                            gridView.setNumColumns(2);

                        productAdapter = new MyCampaignProductAdapter(PaymentsSent.this, R.layout.product_item1, productModels, false);

                        gridView.setAdapter(productAdapter);
                        gridView.setExpanded(true);
                        final ScrollView scrollView = findViewById(R.id.scrollView);
                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {
                                scrollView.scrollTo(0, 0);
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    private void campaignTotalPaidRemaining() {
        //showProgress();
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("campaign_id", "" + campaignModel.getId());
        hashMap.put("member_id", "" + MyPref.getId(this));

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.camaign_total_paid_remaining, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                //hideProgress();
                try {
                    String status = response.getString("status");
                    if (status.equals("success")) {
                        String total_ = response.getJSONObject("result").getString("total");
                        String paid_ = response.getJSONObject("result").getString("paid");
                        String remaining_ = response.getJSONObject("result").getString("remaining_value");
                        total.setText(Constants.getFormatedAmount(total_) + "$");
                        paid.setText(Constants.getFormatedAmount(paid_) + "$");
                        remaining.setText(Constants.getFormatedAmount(remaining_) + "$");

                        totalPanel.setVisibility(View.VISIBLE);
                        paidPanel.setVisibility(View.VISIBLE);
                        remainingPanel.setVisibility(View.VISIBLE);
                        line.setVisibility(View.VISIBLE);

                        if(total_ == null){
                            totalPanel.setVisibility(View.GONE);
                        }
                        else if(total_.isEmpty()){
                            totalPanel.setVisibility(View.GONE);
                        }

                        if(paid_ == null){
                            paidPanel.setVisibility(View.GONE);
                        }
                        else if(paid_.isEmpty()){
                            paidPanel.setVisibility(View.GONE);
                        }

                        if(remaining_ == null){
                            remainingPanel.setVisibility(View.GONE);
                        }
                        else if(remaining_.isEmpty()){
                            remainingPanel.setVisibility(View.GONE);
                        }

                        if(show_paid_.equals("0")){
                            paidPanel.setVisibility(View.GONE);
                        }

                        if(!totalPanel.isShown() && !paidPanel.isShown() && !remainingPanel.isShown()){
                            line.setVisibility(View.GONE);
                        }

                        // loop
                        if (foreground) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    campaignTotalPaidRemaining();
                                }
                            }, 5000);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //hideProgress();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }
}
