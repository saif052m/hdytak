package com.deecoders.hdytak.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deecoders.hdytak.R;
import com.deecoders.hdytak.adapter.SubCategoryAdapter;
import com.deecoders.hdytak.model.SubCategoryModel;
import com.deecoders.hdytak.network.CustomRequest;
import com.deecoders.hdytak.network.VolleyLibrary;
import com.deecoders.hdytak.util.Constants;
import com.deecoders.hdytak.view.ExpandableHeightGridView;
import com.deecoders.hdytak.view.MyTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubCategories extends BaseActivity {
    ExpandableHeightGridView listView;
    SubCategoryAdapter subCategoryAdapter;
    ArrayList<SubCategoryModel> subcategoriesModels = new ArrayList<>();
    @BindView(R.id.text)
    MyTextView text;
    @BindView(R.id.title)
    MyTextView title;
    @BindView(R.id.arrow)
    ImageView arrow;
    @BindView(R.id.main)
    RelativeLayout main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.replaceContentLayout(R.layout.activity_sub_categories);

        ButterKnife.bind(this);

        ViewGroup main = (ViewGroup) findViewById(R.id.main);

        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            //code for portrait mode
            getWindow().setBackgroundDrawableResource(R.drawable.background);
        } else {
            //code for landscape mode
            getWindow().setBackgroundDrawableResource(R.drawable.landscape);
        }

        listView = (ExpandableHeightGridView) findViewById(R.id.listView);

        final ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView);
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0, 0);
            }
        });

        String catId = getIntent().getStringExtra("cat_id");
        String catName = getIntent().getStringExtra("cat_name");
        String campCatId = getIntent().getStringExtra("camp_cat_id");
        String campCatName = getIntent().getStringExtra("camp_cat_name");

        if(catId != null) {
            // product categories
            title.setText("Products");
            text.setText(catName);
            getSubCategories(catId);
        }
        else if(campCatId != null){
            // camp. categories
            title.setText("Campaigns");
            text.setText(campCatName);
            getCampSubCategories(campCatId);
        }

    }

    private void getCampSubCategories(String catId) {
        showProgress(true);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("category_id", catId);
        Log.e("tag", "id: " + catId);

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.campaign_sub_categories, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(true);
                try {
                    String status = response.getString("status");
                    String msg = response.getString("message");
                    if (status.equalsIgnoreCase("success")) {
                        subcategoriesModels.clear();
                        JSONArray categories = response.getJSONArray("campaign_sub_categories");
                        for (int i = 0; i < categories.length(); i++) {
                            JSONObject object = categories.getJSONObject(i);
                            SubCategoryModel categoryModel = new SubCategoryModel();
                            categoryModel.setId(object.getString("id"));
                            categoryModel.setDate(object.getString("created_date"));
                            categoryModel.setUrlTitle(object.getString("url_title"));
                            categoryModel.setTitle(object.getString("title"));
                            categoryModel.setActive(object.getString("active"));
                            subcategoriesModels.add(categoryModel);
                        }
                        subCategoryAdapter = new SubCategoryAdapter(SubCategories.this, R.layout.category_group_item, subcategoriesModels);
                        listView.setAdapter(subCategoryAdapter);
                        listView.setExpanded(true);
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                                Intent intent = new Intent(SubCategories.this, CampaignDetails.class);
                                intent.putExtra("camp_subcat_id", subcategoriesModels.get(pos).getId());
                                intent.putExtra("camp_subcat_name", subcategoriesModels.get(pos).getTitle());
                                intent.putExtra("camp_cat_name", getIntent().getStringExtra("camp_cat_name"));
                                startActivity(intent);
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    private void getSubCategories(String catId) {
        showProgress(true);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("category_id", catId);
        Log.e("tag", "id: " + catId);

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.product_sub_categories, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(true);
                try {
                    String status = response.getString("status");
                    String msg = response.getString("message");
                    if (status.equalsIgnoreCase("success")) {
                        subcategoriesModels.clear();
                        JSONArray categories = response.getJSONArray("product_sub_categories");
                        for (int i = 0; i < categories.length(); i++) {
                            JSONObject object = categories.getJSONObject(i);
                            SubCategoryModel categoryModel = new SubCategoryModel();
                            categoryModel.setId(object.getString("id"));
                            categoryModel.setDate(object.getString("created_date"));
                            categoryModel.setUrlTitle(object.getString("url_title"));
                            categoryModel.setTitle(object.getString("title"));
                            categoryModel.setActive(object.getString("active"));
                            subcategoriesModels.add(categoryModel);
                        }
                        subCategoryAdapter = new SubCategoryAdapter(SubCategories.this, R.layout.category_group_item, subcategoriesModels);
                        listView.setAdapter(subCategoryAdapter);
                        listView.setExpanded(true);
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                                Intent intent = new Intent(SubCategories.this, Products.class);
                                intent.putExtra("subcat_id", subcategoriesModels.get(pos).getId());
                                intent.putExtra("subcat_name", subcategoriesModels.get(pos).getTitle());
                                intent.putExtra("cat_name", getIntent().getStringExtra("cat_name"));
                                startActivity(intent);
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    public void finish(View view) {
        finish();
    }

    public void openMenu(View view) {
        super.drawerToggle();
    }
}
