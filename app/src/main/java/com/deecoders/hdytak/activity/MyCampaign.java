package com.deecoders.hdytak.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.deecoders.hdytak.R;
import com.deecoders.hdytak.adapter.MyCampaignProductAdapter;
import com.deecoders.hdytak.model.CampaignModel;
import com.deecoders.hdytak.model.ProductModel;
import com.deecoders.hdytak.network.CustomRequest;
import com.deecoders.hdytak.network.LruBitmapCache;
import com.deecoders.hdytak.network.VolleyLibrary;
import com.deecoders.hdytak.util.CircularNetworkImageView;
import com.deecoders.hdytak.util.Constants;
import com.deecoders.hdytak.util.MyPref;
import com.deecoders.hdytak.view.ExpandableHeightGridView;
import com.deecoders.hdytak.view.MyTextView;
import com.devspark.appmsg.AppMsg;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyCampaign extends BaseActivity {
    ArrayList<ProductModel> productModels = new ArrayList<>();
    MyCampaignProductAdapter productAdapter;
    @BindView(R.id.profile_image)
    CircularNetworkImageView profileImage;
    @BindView(R.id.title)
    MyTextView title;
    @BindView(R.id.event)
    MyTextView event;
    @BindView(R.id.gridView)
    ExpandableHeightGridView gridView;
    @BindView(R.id.total)
    MyTextView total;
    @BindView(R.id.paid)
    MyTextView paid;
    @BindView(R.id.remaining)
    MyTextView remaining;
    @BindView(R.id.edit_campaign)
    MyTextView editCampaign;
    @BindView(R.id.add_items)
    MyTextView addItems;
    @BindView(R.id.payment_page)
    MyTextView paymentPage;
    @BindView(R.id.share)
    MyTextView share;
    @BindView(R.id.end_campaign)
    MyTextView endCampaign;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.main)
    RelativeLayout main;
    String title_, desc_, image_, visible_ = "0", payment_ = "0", unlimited_ = "0", show_paid_ = "0", selectedCatId, selectedSubcatId;
    boolean foreground;
    CampaignModel campaignModel;

    @BindView(R.id.totalPanel)
    LinearLayout totalPanel;
    @BindView(R.id.paidPanel)
    LinearLayout paidPanel;
    @BindView(R.id.remainingPanel)
    LinearLayout remainingPanel;
    @BindView(R.id.line)
    View line;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.replaceContentLayout(R.layout.activity_my_campaign);

        ButterKnife.bind(this);

        ViewGroup main = (ViewGroup) findViewById(R.id.main);

        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            getWindow().setBackgroundDrawableResource(R.drawable.background);
        } else {
            //code for landscape mode
            getWindow().setBackgroundDrawableResource(R.drawable.landscape);
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 11);
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 12);
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 13);
        }

        campaignDetails(true);
        accountInfo();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 11);
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 12);
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 13);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        foreground = true;
        campaignDetails(false);
        campaignProducts();
        campaignTotalPaidRemaining();
    }

    private void fillDummy() {
        productModels.add(new ProductModel());
        productModels.add(new ProductModel());
        productModels.add(new ProductModel());
        productAdapter = new MyCampaignProductAdapter(MyCampaign.this, R.layout.product_item1, productModels, true);
        gridView.setAdapter(productAdapter);
        gridView.setExpanded(true);
        final ScrollView scrollView = findViewById(R.id.scrollView);
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0, 0);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        foreground = false;
    }

    public void finish(View view) {
        finish();
    }

    public void openMenu(View view) {
        super.drawerToggle();
    }

    public void openPaymentScreen(View view) {
        if (campaignModel == null) {
            AppMsg.makeText(this, "Please wait...", AppMsg.STYLE_ALERT).show();
            return;
        }
        Intent intent = new Intent(this, PayNow.class);
        intent.putExtra("camp_model", campaignModel);
        startActivity(intent);
    }

    public void openEditCompaign(View view) {
        Intent intent = new Intent(this, EditCampaign.class);
        intent.putExtra("desc", desc_);
        intent.putExtra("image", image_);
        intent.putExtra("visible", visible_);
        intent.putExtra("payment", payment_);
        intent.putExtra("unlimited", unlimited_);
        intent.putExtra("show_paid", show_paid_);
        intent.putExtra("selected_cat", selectedCatId);
        intent.putExtra("selected_sub_cat", selectedSubcatId);
        startActivity(intent);
    }

    public void openAddItem(View view) {
        Intent intent = new Intent(this, AddItem.class);
        intent.putExtra("title", title_);
        intent.putExtra("desc", desc_);
        intent.putExtra("image", image_);
        startActivity(intent);
    }

    public void openDetailsShare(View view) {
        if (campaignModel == null) {
            AppMsg.makeText(this, "Please wait...", AppMsg.STYLE_ALERT).show();
            return;
        }
        Intent intent = new Intent(this, PaymentsSent.class);
        intent.putExtra("camp_model", campaignModel);
        startActivity(intent);
    }

    private void campaignDetails(final boolean b) {
        if(b)
            showProgress(true);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if (hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("campaign_id", "" + MyPref.getCampId(this));

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.campaign_details, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                if(b)
                    hideProgress(true);
                try {
                    JSONObject data = response.getJSONObject("campaign");
                    title_ = data.getString("title");
                    desc_ = data.getString("description");
                    image_ = data.getString("image");
                    visible_ = data.getString("visible");
                    payment_ = data.getString("one_payment");
                    selectedCatId = data.getString("campaign_categories_table");
                    selectedSubcatId = data.getString("campaign_sub_categories_table");

                    campaignModel = new CampaignModel();
                    campaignModel.setId(data.getString("id"));
                    campaignModel.setTitle(data.getString("title"));
                    campaignModel.setDescription(data.getString("description"));
                    campaignModel.setImage(data.getString("image"));
                    campaignModel.setUrlTitle(data.getString("url_title"));
                    campaignModel.setVisible(data.getString("visible"));
                    campaignModel.setOnePayment(data.getString("one_payment"));
                    campaignModel.setQrImage(data.getString("qr_image"));

                    title.setText(title_);
                    event.setText(desc_);
                    RequestQueue mRequestQueue = Volley.newRequestQueue(MyCampaign.this);
                    ImageLoader mImageLoader = new ImageLoader(mRequestQueue, new LruBitmapCache());
                    profileImage.setImageUrl(Constants.campaignImageUrl + image_, mImageLoader);

                    unlimited_ = data.getString("unlimited");
                    show_paid_ = data.getString("show_paid");

                    if(data.getString("active").equals("0")){
                        MyPref.setCampId(MyCampaign.this, null);
                        startActivity(new Intent(MyCampaign.this, CreateCampaign.class));
                        finish();
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                    if(b)
                        showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(b) {
                    hideProgress(false);
                    showFailedAlert();
                }
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    private void campaignProducts() {
        HashMap<String, String> hashMap = Constants.getSHA256();
        if (hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("campaign_id", "" + MyPref.getCampId(this));
        hashMap.put("member_id", "" + MyPref.getId(this));

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.campaign_products, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                try {
                    String status = response.getString("status");
                    if (status.equals("success")) {
                        productModels.clear();
                        JSONArray products = response.getJSONArray("campaign_products");
                        JSONArray items = response.getJSONArray("campaign_items");
                        for (int i = 0; i < items.length(); i++) {
                            //id, created_date, title, salling_price, description, image;
                            ProductModel model = new ProductModel();
                            model.setId(items.getJSONObject(i).getString("id"));
                            model.setCreated_date(items.getJSONObject(i).getString("created_date"));
                            model.setTitle(items.getJSONObject(i).getString("title"));
                            model.setSalling_price(items.getJSONObject(i).getString("salling_price"));
                            model.setDescription(items.getJSONObject(i).getString("description"));
                            model.setImage(items.getJSONObject(i).getString("image"));
                            model.setType(1);
                            productModels.add(model);
                        }
                        for (int i = 0; i < products.length(); i++) {
                            //id, created_date, title, salling_price, description, image;
                            ProductModel model = new ProductModel();
                            model.setId(products.getJSONObject(i).getString("id"));
                            model.setCreated_date(products.getJSONObject(i).getString("created_date"));
                            model.setTitle(products.getJSONObject(i).getString("title"));
                            model.setSalling_price(products.getJSONObject(i).getString("price"));
                            model.setDescription(products.getJSONObject(i).getString("description"));
                            model.setImage(products.getJSONObject(i).getString("image"));
                            model.setType(0);
                            productModels.add(model);
                        }

                        Log.e("tag", "size: " + productModels.size());

                        if (productModels.size() == 1)
                            gridView.setNumColumns(1);
                        else
                            gridView.setNumColumns(2);

                        if(productAdapter == null) {
                            productAdapter = new MyCampaignProductAdapter(MyCampaign.this, R.layout.product_item1, productModels, true);
                            gridView.setAdapter(productAdapter);
                            gridView.setExpanded(true);
                        }
                        else{
                            productAdapter.setModels(productModels);
                        }

                        final ScrollView scrollView = findViewById(R.id.scrollView);
                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {
                                scrollView.scrollTo(0, 0);
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    private void campaignTotalPaidRemaining() {
        //showProgress();
        HashMap<String, String> hashMap = Constants.getSHA256();
        if (hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("campaign_id", "" + MyPref.getCampId(this));
        hashMap.put("member_id", "" + MyPref.getId(this));

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.camaign_total_paid_remaining, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                //hideProgress();
                try {
                    String status = response.getString("status");
                    if (status.equals("success")) {
                        String total_ = response.getJSONObject("result").getString("total");
                        String paid_ = response.getJSONObject("result").getString("paid");
                        String remaining_ = response.getJSONObject("result").getString("remaining_value");

                        total.setText(Constants.getFormatedAmount(total_) + "$");
                        paid.setText(Constants.getFormatedAmount(paid_) + "$");
                        remaining.setText(Constants.getFormatedAmount(remaining_) + "$");

                        totalPanel.setVisibility(View.VISIBLE);
                        paidPanel.setVisibility(View.VISIBLE);
                        remainingPanel.setVisibility(View.VISIBLE);
                        line.setVisibility(View.VISIBLE);

                        if(total_ == null){
                            totalPanel.setVisibility(View.GONE);
                        }
                        else if(total_.isEmpty()){
                            totalPanel.setVisibility(View.GONE);
                        }

                        if(paid_ == null){
                            paidPanel.setVisibility(View.GONE);
                        }
                        else if(paid_.isEmpty()){
                            paidPanel.setVisibility(View.GONE);
                        }

                        if(remaining_ == null){
                            remainingPanel.setVisibility(View.GONE);
                        }
                        else if(remaining_.isEmpty()){
                            remainingPanel.setVisibility(View.GONE);
                        }

                        if(show_paid_.equals("0")){
                            paidPanel.setVisibility(View.GONE);
                        }

                        if(!totalPanel.isShown() && !paidPanel.isShown() && !remainingPanel.isShown()){
                            line.setVisibility(View.GONE);
                        }

                        // loop
                        if (foreground) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    campaignTotalPaidRemaining();
                                }
                            }, 5000);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //hideProgress();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    private void endCampaign() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirm End Campaign!");
        builder.setMessage("Are you sure you want to end this campaign?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                showProgress(false);
                HashMap<String, String> hashMap = Constants.getSHA256();
                if (hashMap == null)
                    hashMap = new HashMap<>();
                hashMap.put("campaign_id", "" + MyPref.getCampId(MyCampaign.this));
                hashMap.put("member_id", "" + MyPref.getId(MyCampaign.this));

                System.out.println(Arrays.asList(hashMap));

                CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.endCampaign, hashMap, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("success", response.toString());
                        hideProgress(false);
                        try {
                            String status = response.getString("status");
                            String msg = response.getString("message");
                            if (status.equals("success")) {
                                String campId = MyPref.getCampId(MyCampaign.this);
                                MyPref.setCampId(MyCampaign.this, null);
                                finishAffinity();
                                Toast.makeText(MyCampaign.this, "" + msg, Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(MyCampaign.this, Review.class);
                                intent.putExtra("camp_id", campId);
                                startActivity(intent);
                            } else {
                                AppMsg.makeText(MyCampaign.this, "" + msg, AppMsg.STYLE_ALERT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            showFailedAlert();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress(false);
                        showFailedAlert();
                        Log.e("error", error.toString());
                    }
                });
                VolleyLibrary.getInstance(MyCampaign.this).addToRequestQueue(customRequest, "", false);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void endCampaign(View view) {
        endCampaign();
    }

    private void accountInfo() {
        //showProgress();
        HashMap<String, String> hashMap = Constants.getSHA256();
        if (hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("member_id", "" + MyPref.getId(this));

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.user_account, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                //hideProgress();
                try {
                    JSONObject data = response.getJSONObject("user_account");
                    MyPref.setUsername(getApplicationContext(), data.getString("first_name") + " " + data.getString("last_name"));
                    MyPref.setEmail(getApplicationContext(), data.getString("email"));
                    MyPref.setCountry(getApplicationContext(), response.getJSONObject("country").getString("title"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //hideProgress();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }
}
