package com.deecoders.hdytak.activity

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.deecoders.hdytak.R
import com.deecoders.hdytak.network.CustomRequest
import com.deecoders.hdytak.network.VolleyLibrary
import com.deecoders.hdytak.util.Constants
import com.deecoders.hdytak.util.MyPref
import kotlinx.android.synthetic.main.activity_review.*
import org.json.JSONException
import java.util.*

class Review : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        replaceContentLayout(R.layout.activity_review)

        var orientation = this.resources.configuration.orientation;
        if(orientation == Configuration.ORIENTATION_PORTRAIT){
            window.setBackgroundDrawableResource(R.drawable.background)
        }
        else{
            window.setBackgroundDrawableResource(R.drawable.landscape)
        }

        send.setOnClickListener {
            sendRequest()
        }

        startCampaign.setOnClickListener {
            endCampaign()
        }
    }

    private fun endCampaign() {
        startActivity(Intent(this@Review, CreateCampaign::class.java))
        finish()
    }

    fun sendRequest(){
        showProgress(false)
        var hashmap = Constants.getSHA256()
        if(hashmap == null){
            hashmap = HashMap();
        }
        Log.e("tag", "cam. id: "+intent.getStringExtra("camp_id"))
        Log.e("tag", "id: "+MyPref.getId(this))
        Log.e("tag", "review: "+review.text.toString())
        hashmap.put("campaign_id", intent.getStringExtra("camp_id"));
        hashmap.put("member_id", MyPref.getId(this))
        if(!review.text.toString().isEmpty())
            hashmap.put("review", review.text.toString())

        val customRequest = CustomRequest(Request.Method.POST, Constants.send_review_url, hashmap, Response.Listener { response ->
            Log.e("success", response.toString())
            hideProgress(false)
            try {
                val status = response.getString("status")
                val msg = response.getString("message")
                if (status.equals("success", ignoreCase = true)) {
                    Toast.makeText(this@Review, "" + msg, Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this@Review, "" + msg, Toast.LENGTH_SHORT).show()
                }
                endCampaign()
            } catch (e: JSONException) {
                e.printStackTrace()
                showFailedAlert()
            }
        }, Response.ErrorListener { error ->
            hideProgress(false)
            showFailedAlert()
            Log.e("error", error.toString())
        })
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false)
    }
}
