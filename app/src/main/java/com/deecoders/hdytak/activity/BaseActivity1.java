package com.deecoders.hdytak.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.blikoon.qrcodescanner.QrCodeActivity;
import com.deecoders.hdytak.R;
import com.deecoders.hdytak.model.CampaignModel;
import com.deecoders.hdytak.network.CustomRequest;
import com.deecoders.hdytak.network.VolleyLibrary;
import com.deecoders.hdytak.util.Constants;
import com.devspark.appmsg.AppMsg;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

public class BaseActivity1 extends AppCompatActivity {
    DrawerLayout drawerLayout;
    boolean open;
    ProgressBar progressBar;
    ViewGroup container;
    private static final int REQUEST_CODE_QR_SCAN = 101;
    private FrameLayout contentframe;
    private com.airbnb.lottie.LottieAnimationView animationview;
    private DrawerLayout drawerlayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base1);

        this.animationview = (LottieAnimationView) findViewById(R.id.animation_view);

        container = (ViewGroup)findViewById(R.id.container);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    public void showSuccessToast(String msg){
        AppMsg.makeText(this, msg, AppMsg.STYLE_INFO).show();
    }

    public void showErrorToast(String msg){
        AppMsg.makeText(this, msg, AppMsg.STYLE_ALERT).show();
    }

    protected void replaceContentLayout(int sourceId) {
        View contentLayout = findViewById(R.id.content_frame);
        ViewGroup parent = (ViewGroup) contentLayout.getParent();
        int index = parent.indexOfChild(contentLayout);
        parent.removeView(contentLayout);
        contentLayout = getLayoutInflater().inflate(sourceId, parent, false);
        parent.addView(contentLayout, index);
    }

    public void openDrawer() {
        open = true;
        drawerLayout.openDrawer(GravityCompat.START);
    }

    public void closeDrawer() {
        open = false;
        drawerLayout.closeDrawer(GravityCompat.START);
    }

    public void drawerToggle() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            closeDrawer();
        } else {
            openDrawer();
        }
    }

    public void showProgress(boolean hidePanel) {
        animationview.setVisibility(View.VISIBLE);
        if(hidePanel)
            container.setVisibility(View.GONE);
    }

    public void hideProgress(boolean showPanel) {
        animationview.setVisibility(View.INVISIBLE);
        if(showPanel)
            container.setVisibility(View.VISIBLE);
    }

    public void showFailedAlert(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Request Failed!")
                .setMessage("Request failed due to some network problem.")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            closeDrawer();
        } else {
            finish();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.e("tag", "onConfigurationChanged");
        container = (ViewGroup)findViewById(R.id.container);
        if(container == null)
            return;

        Display display = getWindowManager().getDefaultDisplay();
        int width = display.getWidth();

        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            //code for portrait mode
            getWindow().setBackgroundDrawableResource(R.drawable.background);
            container.getLayoutParams().width = width;
        } else {
            //code for landscape mode
            getWindow().setBackgroundDrawableResource(R.drawable.landscape);
            container.getLayoutParams().width = (int) (width / 1.5);
        }
    }

    public void scanQRCode(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA}, 11);
        }
        else {
            closeDrawer();
            Intent i = new Intent(this, QrCodeActivity.class);
            startActivityForResult(i, REQUEST_CODE_QR_SCAN);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode != Activity.RESULT_OK) {
            Log.e("tag", "COULD NOT GET A GOOD RESULT.");
            if(data==null)
                return;
            //Getting the passed result
            String result = data.getStringExtra("com.blikoon.qrcodescanner.error_decoding_image");
            if( result!=null) {
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Scan Error");
                alertDialog.setMessage("QR Code could not be scanned");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
            return;

        }
        else if(requestCode == REQUEST_CODE_QR_SCAN) {
            if(data==null)
                return;
            //Getting the passed result
            String result = data.getStringExtra("com.blikoon.qrcodescanner.got_qr_scan_relult");
            Log.e("scan","Have scan result in your app activity :"+ result);
            sendRequest(result);
        }
        else{
            Log.e("scan","cancelled");
        }
    }

    private void sendRequest(String url) {
        showProgress(false);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("url", ""+url);

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.campaign_details_by_url, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("scan", response.toString());
                hideProgress(false);
                try {
                    String status = response.getString("status");
                    String msg = response.getString("message");
                    if (status.equalsIgnoreCase("success")) {
                        //AppMsg.makeText(BaseActivity.this, ""+msg, AppMsg.STYLE_INFO).show();
                        JSONObject campaign = response.getJSONObject("campaign");
                        CampaignModel model = new CampaignModel();
                        model.setId(campaign.getString("id"));
                        model.setCreated_date(campaign.getString("created_date"));
                        model.setTitle(campaign.getString("title"));
                        model.setDescription(campaign.getString("description"));
                        model.setImage(campaign.getString("image"));
                        model.setUrlTitle(campaign.getString("url_title"));
                        model.setVisible(campaign.getString("visible"));
                        model.setOnePayment(campaign.getString("one_payment"));
                        model.setQrImage(campaign.getString("qr_image"));
                        Intent intent = new Intent(BaseActivity1.this, PayNow.class);
                        intent.putExtra("camp_model", model);
                        startActivity(intent);
                    }
                    else{
                        AppMsg.makeText(BaseActivity1.this, ""+msg, AppMsg.STYLE_ALERT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }
}
