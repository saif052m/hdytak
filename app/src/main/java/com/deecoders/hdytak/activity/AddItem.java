package com.deecoders.hdytak.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.deecoders.hdytak.R;
import com.deecoders.hdytak.network.CustomRequest;
import com.deecoders.hdytak.network.LruBitmapCache;
import com.deecoders.hdytak.network.VolleyLibrary;
import com.deecoders.hdytak.util.CircularNetworkImageView;
import com.deecoders.hdytak.util.Constants;
import com.deecoders.hdytak.util.GetFilePathFromDevice;
import com.deecoders.hdytak.util.MyPref;
import com.deecoders.hdytak.view.MyCheckBox;
import com.deecoders.hdytak.view.MyEditText;
import com.deecoders.hdytak.view.MyTextView;
import com.devspark.appmsg.AppMsg;
import com.theartofdev.edmodo.cropper.CropImage;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddItem extends BaseActivity {
    @BindView(R.id.profile_image)
    CircularNetworkImageView profileImage;
    @BindView(R.id.title)
    MyTextView title;
    @BindView(R.id.event)
    MyTextView event;
    @BindView(R.id.add_product)
    MyTextView addProduct;
    @BindView(R.id.or)
    MyTextView or;
    @BindView(R.id.add_item)
    MyTextView addItem;
    @BindView(R.id.desc)
    MyTextView desc;
    @BindView(R.id.item_name)
    MyEditText itemName;
    @BindView(R.id.price)
    MyEditText price;
    @BindView(R.id.image)
    TextView image;
    @BindView(R.id.shop_name)
    MyEditText shopName;
    @BindView(R.id.submit)
    MyTextView submit;
    @BindView(R.id.main)
    RelativeLayout main;
    @BindView(R.id.container)
    ViewGroup container;
    @BindView(R.id.photo)
    ImageView photo;
    @BindView(R.id.agree)
    MyCheckBox agree;
    File cameraFile;
    String filePath;
    Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        replaceContentLayout(R.layout.activity_add_item);

        ButterKnife.bind(this);

        ViewGroup main = (ViewGroup) findViewById(R.id.main);

        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            //code for portrait mode
            getWindow().setBackgroundDrawableResource(R.drawable.background);
        } else {
            //code for landscape mode
            getWindow().setBackgroundDrawableResource(R.drawable.landscape);
        }

        String desc_ = getIntent().getStringExtra("desc");
        String image_ = getIntent().getStringExtra("image");
        String title_ = getIntent().getStringExtra("title");

        RequestQueue mRequestQueue = Volley.newRequestQueue(this);
        ImageLoader mImageLoader = new ImageLoader(mRequestQueue, new LruBitmapCache());
        profileImage.setImageUrl(Constants.campaignImageUrl+image_, mImageLoader);

        title.setText(title_);
        event.setText(desc_);

        loadActive();
    }

    public void finish(View view) {
        finish();
    }

    public void openMenu(View view) {
        super.drawerToggle();
    }

    public void openCategories(View view) {
        startActivity(new Intent(this, Categories.class));
    }

    public void openMyCampaign(View view) {
        // add item and back to my campaign
        if(itemName.getText().toString().isEmpty()){
            AppMsg.makeText(this, "Enter item name!", AppMsg.STYLE_ALERT).show();
        }
        else if(price.getText().toString().isEmpty()){
            AppMsg.makeText(this, "Enter item price!", AppMsg.STYLE_ALERT).show();
        }
        else if(filePath == null){
            AppMsg.makeText(this, "Select item image!", AppMsg.STYLE_ALERT).show();
        }
        else if(shopName.getText().toString().isEmpty()){
            AppMsg.makeText(this, "Enter shop name and address!", AppMsg.STYLE_ALERT).show();
        }
        else if(!agree.isChecked()){
            AppMsg.makeText(this, "Please agree to terms!", AppMsg.STYLE_ALERT).show();
        }
        else{
            new UploadTask().execute();
        }
    }

    private void loadActive() {
        showProgress(true);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.activeContent, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(true);
                try {
                    String status = response.getString("status");
                    String msg = response.getString("message");
                    if(status.equals("success")){
                        JSONArray array = response.getJSONArray("active_content");
                        JSONObject item = array.getJSONObject(0);
                        JSONObject product = array.getJSONObject(1);
                        String productActive = product.getString("active");
                        String itemActive = item.getString("active");
                        if(productActive.equals("1") && itemActive.equals("1")){
                            addProduct.setVisibility(View.VISIBLE);
                            or.setVisibility(View.VISIBLE);
                            container.setVisibility(View.VISIBLE);
                        }
                        else if(productActive.equals("1") && itemActive.equals("0")){
                            or.setVisibility(View.GONE);
                            container.setVisibility(View.GONE);
                        }
                        else if(productActive.equals("0") && itemActive.equals("1")){
                            addProduct.setVisibility(View.GONE);
                            or.setVisibility(View.GONE);
                        }
                        else if(productActive.equals("0") && itemActive.equals("0")){
                            addProduct.setVisibility(View.GONE);
                            or.setVisibility(View.GONE);
                            container.setVisibility(View.GONE);
                        }
                    }
                    else{
                        AppMsg.makeText(AddItem.this, ""+msg, AppMsg.STYLE_ALERT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                Log.e("error", error.toString());
                showFailedAlert();
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    public void showPopup(View view) {
        final CharSequence options[] = new CharSequence[] {"Camera", "Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Source");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(options[which].equals("Camera")){
                    openCamera();
                }
                else{
                    openGallery();
                }
            }
        });
        builder.show();
    }

    private void openGallery() {
        // gallery code
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 11);
        }
        else {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            try {
                startActivityForResult(intent, 100);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(this, "Please install a File Manager.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void openCamera() {
        File extDir = Environment.getExternalStorageDirectory();
        Random r = new Random();
        String name = (r.nextInt(100000000)+r.nextInt(100000000))+".jpg";
        cameraFile = new File(extDir, name);
        if(checkExternalStorage()){
            String text ="writing into externanal storage";
            FileOutputStream fos;
            try {
                fos = new FileOutputStream(cameraFile);
                fos.write(text.getBytes());
                fos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(cameraFile.isFile()){
                Log.e("tag", "is file");
                Intent cameraintent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                cameraintent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cameraFile));
                startActivityForResult(cameraintent, 200);
            }
            else{
                Log.e("tag", "oops not file");
            }
        }
        else{
            AppMsg.makeText(this, "External storage not available!", AppMsg.STYLE_ALERT).show();
        }
    }

    public boolean checkExternalStorage() {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 100:
                if(resultCode==RESULT_OK){
                    if(data != null) {
                        Log.e("tag", "uri: "+data.getData());
                        Uri uri = data.getData();
                        String path = data.getData().getPath();
                        if (Build.VERSION.SDK_INT < 11) {
                            filePath = GetFilePathFromDevice.getPath(this, uri);;
                            //filePath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, data.getData());

                        } else if (Build.VERSION.SDK_INT < 19) {
                            //filePath = RealPathUtil.getRealPathFromURI_API11to18(this, data.getData());
                            filePath = GetFilePathFromDevice.getPath(this, uri);
                            ;
                        } else {
                            filePath = GetFilePathFromDevice.getPath(this, uri);
                            ;
                            //filePath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());
                        }
                        Log.e("File path", "File Path: " + filePath);
                        Bitmap bmp = BitmapFactory.decodeFile(filePath);
                        if(bmp != null) {
                            showSmallIcon();
                            showImageCropper(uri);
                        }
                    }
                }
                break;
            case 200:
                if(data != null) {
                    Uri uri = null;
                    if (data.getData() == null) {
                        //from camera
                        if(cameraFile == null)
                            return;
                        uri = Uri.fromFile(cameraFile);
                    }
                    else{
                        uri = data.getData();
                    }
                    Log.e("tag", "uri: "+uri);
                    if (Build.VERSION.SDK_INT < 11){
                        filePath = GetFilePathFromDevice.getPath(this, uri);;
                        //filePath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, data.getData());

                    }else if (Build.VERSION.SDK_INT < 19){
                        //filePath = RealPathUtil.getRealPathFromURI_API11to18(this, data.getData());
                        filePath = GetFilePathFromDevice.getPath(this, uri);;
                    }
                    else{
                        filePath = GetFilePathFromDevice.getPath(this, uri);;
                        //filePath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());
                    }
                    Log.e("File path", "File Path: " + filePath);

                    Bitmap bmp = BitmapFactory.decodeFile(filePath);
                    if(bmp != null) {
                        showSmallIcon();
                        showImageCropper(uri);
                    }
                }
                else{
                    Uri uri = null;
                    if(cameraFile == null) {
                        AppMsg.makeText(this, "Unable to fetch photo!", AppMsg.STYLE_ALERT).show();
                        return;
                    }
                    uri = Uri.fromFile(cameraFile);
                    Log.e("tag", "uri: "+uri);
                    if (Build.VERSION.SDK_INT < 11){
                        filePath = GetFilePathFromDevice.getPath(this, uri);;
                        //filePath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, data.getData());
                    }
                    else if (Build.VERSION.SDK_INT < 19){
                        //filePath = RealPathUtil.getRealPathFromURI_API11to18(this, data.getData());
                        filePath = GetFilePathFromDevice.getPath(this, uri);;
                    }
                    else{
                        filePath = GetFilePathFromDevice.getPath(this, uri);;
                        //filePath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());
                    }

                    Log.e("File path", "File Path: " + filePath);
                    Bitmap bmp = BitmapFactory.decodeFile(filePath);
                    if(bmp != null) {
                        showSmallIcon();
                        showImageCropper(uri);
                    }
                }
                break;
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri uri = result.getUri();
                    Log.e("tag", "uri: "+uri);
                    if (Build.VERSION.SDK_INT < 11){
                        filePath = GetFilePathFromDevice.getPath(this, uri);;
                        //filePath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, data.getData());
                    }
                    else if (Build.VERSION.SDK_INT < 19){
                        //filePath = RealPathUtil.getRealPathFromURI_API11to18(this, data.getData());
                        filePath = GetFilePathFromDevice.getPath(this, uri);;
                    }
                    else{
                        filePath = GetFilePathFromDevice.getPath(this, uri);;
                        //filePath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());
                    }
                    Bitmap bmp = BitmapFactory.decodeFile(filePath);
                    Log.e("tagg", "w: "+bmp.getWidth());
                    Log.e("tagg", "h: "+bmp.getHeight());
                    if(bmp.getWidth()<500 || bmp.getHeight()<500){
                        filePath = null;
                        photo.setImageBitmap(null);
                        AppMsg.makeText(AddItem.this, "Image too small. Should be at least 500x500!", AppMsg.STYLE_ALERT).show();
                    }
                    if(bmp.getWidth()>2500 && bmp.getHeight()>2500){
                        filePath = null;
                        photo.setImageBitmap(null);
                        AppMsg.makeText(AddItem.this, "Image too large!", AppMsg.STYLE_ALERT).show();
                    }
                }
                else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                    showImageCropper(uri);
                }
                else{
                    Log.e("tagg", "cropped cancelled");
                    Toast.makeText(this, "Cropping is required for better image usage!", Toast.LENGTH_SHORT).show();
                    showImageCropper(uri);
                }

            default:
                break;
        }

    }

    public void terms(View view) {
        startActivity(new Intent(this, TermsAndConditions.class));
    }

    public class UploadTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            String response = submitCampaign();
            return response;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(false);
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            Log.e("server response", ""+response);
            hideProgress(false);
            if(response != null) {
                try {
                    JSONObject object = new JSONObject(response);
                    String status = object.getString("status");
                    String msg = object.getString("message");
                    if (status.equalsIgnoreCase("success")) {
                        Toast.makeText(AddItem.this, "" + msg, Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        AppMsg.makeText(AddItem.this, "" + msg, AppMsg.STYLE_ALERT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
            else{
                showFailedAlert();
            }
        }
    }

    public static Bitmap getResizedBitmap(Bitmap image, int newHeight, int newWidth) {
        int width = image.getWidth();
        int height = image.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // create a matrix for the manipulation
        Matrix matrix = new Matrix();
        // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);
        // recreate the new Bitmap
        Bitmap resizedBitmap = Bitmap.createBitmap(image, 0, 0, width, height,
                matrix, false);
        return resizedBitmap;
    }

    private String submitCampaign() {
        //Looper.prepare();
        MultipartEntity multiPart = new MultipartEntity();

        File sourceFile = new File(filePath);

        try {
            // sending parameters
            HashMap<String, String> hashMap = Constants.getSHA256();
            if(hashMap != null){
                multiPart.addPart("username", new StringBody(hashMap.get("username")));
                multiPart.addPart("access_password", new StringBody(hashMap.get("access_password")));
                multiPart.addPart("key", new StringBody(hashMap.get("key")));
                multiPart.addPart("signature", new StringBody(hashMap.get("signature")));
            }
            multiPart.addPart("member_id", new StringBody(MyPref.getId(this)));
            multiPart.addPart("campaign_id", new StringBody(MyPref.getCampId(this)));
            multiPart.addPart("title", new StringBody(itemName.getText().toString()));
            multiPart.addPart("description", new StringBody(shopName.getText().toString()));
            multiPart.addPart("price", new StringBody(price.getText().toString()));
            if(agree.isChecked())
                multiPart.addPart("agree", new StringBody("1"));
            else
                multiPart.addPart("agree", new StringBody("0"));

            if(sourceFile != null)
                multiPart.addPart("image", new FileBody(sourceFile));

            String response = multipost(Constants.add_item_to_campaign, multiPart);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
        }

        //Looper.loop();

        return null;
    }

    private static String multipost(String urlString, MultipartEntity reqEntity) {
        try {
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(30000);
            conn.setRequestMethod("POST");
            conn.setUseCaches(false);
            //conn.setChunkedStreamingMode(1024);
            conn.setDoInput(true);
            conn.setDoOutput(true);

            conn.setRequestProperty("connection", "Keep-Alive");
            conn.addRequestProperty("Content-length", reqEntity.getContentLength()+"");
            conn.setRequestProperty("Cache-Control", "no-cache");
            conn.addRequestProperty(reqEntity.getContentType().getName(), reqEntity.getContentType().getValue());

            OutputStream os = conn.getOutputStream();
            reqEntity.writeTo(conn.getOutputStream());
            os.close();
            conn.connect();
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                return readStream(conn.getInputStream());
            }
            else{
                Log.e("tag", "code: "+conn.getResponseCode());
                Log.e("tag", "msg: "+conn.getResponseMessage());
                Log.e("tag", "cannot read stream");
            }
        }
        catch (Exception e) {
            Log.e("Uploading", "multipart post error " + e + "(" + urlString + ")");
        }
        return null;
    }

    private static String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuilder builder = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return builder.toString();
    }

    private File convertBitmapIntoFile(Bitmap bitmap){
        Random r = new Random();
        int no = 10000 + r.nextInt(99999)+r.nextInt(99999);
        File filesDir = getFilesDir();
        File imageFile = new File(filesDir, no + ".jpg");
        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            //Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
        return imageFile;
    }

    private void showSmallIcon() {
        photo.setImageResource(R.drawable.tick);
    }

    private void showImageCropper(Uri uri) {
        Log.e("tagg", "uri1: "+uri);
        this.uri = uri;
        CropImage.activity(uri)
                .setMinCropResultSize(500, 500)
                .setMaxCropResultSize(2500, 2500)
                .start(this);
    }

    /*private File getCompressedFile(String filePath) {
        try {
            Bitmap bmp = BitmapFactory.decodeFile(filePath);
            bmp = bmpAfterRotationCheck(bmp, filePath);
            int width = 500, height = 500;
            if(bmp.getWidth()<500){
                width = bmp.getWidth();
            }
            if(bmp.getHeight()<500){
                height = bmp.getHeight();
            }
            bmp = getResizedBitmap(bmp, height, width);
            File sourceFile = convertBitmapIntoFile(bmp);
            if (sourceFile.isFile()) {
                Log.e("tag", "is file");
            }
            else {
                Log.e("tag", "not a file");
            }
            return sourceFile;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private Bitmap bmpAfterRotationCheck(Bitmap bmp, String filePath) {
        try {
            ExifInterface exif = new ExifInterface(filePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
            Log.e("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            }
            else if (orientation == 3) {
                matrix.postRotate(180);
            }
            else if (orientation == 8) {
                matrix.postRotate(270);
            }
            bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true); // rotating bitmap
            return bmp;
        }
        catch (Exception e) {
        }
        return bmp;
    }*/
}
