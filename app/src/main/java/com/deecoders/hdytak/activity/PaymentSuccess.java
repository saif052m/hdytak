package com.deecoders.hdytak.activity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deecoders.hdytak.R;
import com.deecoders.hdytak.network.CustomRequest;
import com.deecoders.hdytak.network.VolleyLibrary;
import com.deecoders.hdytak.util.Constants;
import com.deecoders.hdytak.view.MyTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentSuccess extends BaseActivity {
    String orderNo_, amount_;
    @BindView(R.id.title)
    MyTextView title;
    @BindView(R.id.status)
    MyTextView status;
    @BindView(R.id.order)
    MyTextView order;
    @BindView(R.id.amount)
    MyTextView amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        replaceContentLayout(R.layout.activity_payment_success);

        ButterKnife.bind(this);

        ViewGroup main = (ViewGroup) findViewById(R.id.main);

        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            //code for portrait mode
            getWindow().setBackgroundDrawableResource(R.drawable.background);
        } else {
            //code for landscape mode
            getWindow().setBackgroundDrawableResource(R.drawable.landscape);
        }

        orderNo_ = getIntent().getStringExtra("orderNo");
        amount_ = getIntent().getStringExtra("amount");

        checkPaymentSuccess();
    }

    public void finish(View view) {
        finish();
    }

    public void openMenu(View view) {
        super.drawerToggle();
    }

    private void checkPaymentSuccess() {
        showProgress(true);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if (hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("order_number", "" + getIntent().getStringExtra("orderNo"));

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.payment_result, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(true);
                String status_ = null, msg = null;
                try {
                    status_ = response.getString("status");
                    msg = response.getString("message");
                    if (status_.toLowerCase().equals("success")) {
                        title.setText("CONGRATULATIONS");
                        status.setText(""+msg);
                        order.setText("PAYMENT NUMBER : "+orderNo_);
                        amount.setText("PAYMENT VALUE : "+amount_+"$");
                    }
                    else {
                        title.setText("PAYMENT FAILED");
                        title.setTextColor(getResources().getColor(R.color.pink));
                        status.setText(""+msg);
                        status.setTextColor(getResources().getColor(R.color.pink));
                        order.setText("PAYMENT NUMBER : "+orderNo_);
                        order.setTextColor(getResources().getColor(R.color.pink));
                        amount.setText("PAYMENT VALUE : "+amount_+"$");
                        amount.setTextColor(getResources().getColor(R.color.pink));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }
}
