package com.deecoders.hdytak.activity

import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.View
import com.android.volley.Request
import com.android.volley.Response
import com.deecoders.hdytak.R
import com.deecoders.hdytak.network.CustomRequest
import com.deecoders.hdytak.network.VolleyLibrary
import com.deecoders.hdytak.util.Constants
import com.devspark.appmsg.AppMsg
import kotlinx.android.synthetic.main.activity_forgot_password.*
import org.json.JSONException
import java.util.*

class KotlinTest : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        replaceContentLayout(R.layout.activity_forgot_password)

        var orientation = this.resources.configuration.orientation;
        if(orientation == Configuration.ORIENTATION_PORTRAIT){
            window.setBackgroundDrawableResource(R.drawable.background)
        }
        else{
            window.setBackgroundDrawableResource(R.drawable.landscape)
        }

        submit.setOnClickListener {
            if(email.text.isEmpty()){
                AppMsg.makeText(this@KotlinTest, "Please enter email!", AppMsg.STYLE_ALERT).show()
            }
            else{
                sendRequest()
            }
        }
    }

    fun sendRequest(){
        showProgress(false)
        var hashmap = Constants.getSHA256()
        if(hashmap == null){
            hashmap = HashMap();
        }
        hashmap.put("email", email.text.toString())

        println(hashmap.get("email"))
        println(hashmap.get("signature"))

        val customRequest = CustomRequest(Request.Method.POST, Constants.forgotPassword, hashmap, Response.Listener { response ->
            Log.e("success", response.toString())
            hideProgress(false)
            try {
                val status = response.getString("status")
                val msg = response.getString("message")
                if (status.equals("success", ignoreCase = true)) {
                    AppMsg.makeText(this@KotlinTest, "" + msg, AppMsg.STYLE_INFO).show()
                } else {
                    AppMsg.makeText(this@KotlinTest, "" + msg, AppMsg.STYLE_ALERT).show()
                }
            } catch (e: JSONException) {
                e.printStackTrace()
                showFailedAlert()
            }
        }, Response.ErrorListener { error ->
            hideProgress(false)
            showFailedAlert()
            Log.e("error", error.toString())
        })
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false)
    }

    fun finish(view: View){
        finish()
    }

    fun openMenu(view: View){
        drawerToggle()
    }
}
