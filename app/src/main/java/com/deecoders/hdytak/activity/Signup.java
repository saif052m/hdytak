package com.deecoders.hdytak.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deecoders.hdytak.R;
import com.deecoders.hdytak.network.CustomRequest;
import com.deecoders.hdytak.network.VolleyLibrary;
import com.deecoders.hdytak.util.Constants;
import com.deecoders.hdytak.util.MyPref;
import com.deecoders.hdytak.view.MyCheckBox;
import com.deecoders.hdytak.view.MyEditText;
import com.deecoders.hdytak.view.MyTextView;
import com.devspark.appmsg.AppMsg;
import com.google.firebase.iid.FirebaseInstanceId;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Signup extends BaseActivity {
    @BindView(R.id.fname)
    MyEditText fname;
    @BindView(R.id.countryTxt)
    TextView countryTxt;
    @BindView(R.id.lname)
    MyEditText lname;
    @BindView(R.id.email)
    MyEditText email;
    @BindView(R.id.password)
    MyEditText password;
    @BindView(R.id.code)
    LinearLayout code;
    @BindView(R.id.mobile)
    MyEditText mobile;
    @BindView(R.id.checkbox)
    MyCheckBox checkbox;
    @BindView(R.id.termsAndConditions)
    MyTextView termsAndConditions;
    @BindView(R.id.submit)
    MyTextView submit;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.main)
    RelativeLayout main;
    @BindView(R.id.country)
    BetterSpinner country;
    ArrayList<String> countryNames = new ArrayList<>();
    ArrayList<String> countryCodes = new ArrayList<>();
    ArrayList<String> countryIds = new ArrayList<>();
    @BindView(R.id.codeValue)
    MyTextView codeValue;
    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile("[a-zA-Z0-9+._%-+]{1,256}" +
            "@" +
            "[a-zA-Z0-9][a-zA-Z0-9-]{0,64}" +
            "(" +
            "." +
            "[a-zA-Z0-9][a-zA-Z0-9-]{0,25}" +
            ")+");

    public static final Pattern NAME_PATTERN = Pattern.compile("[a-zA-Z ]*");
    String firebaseToken;
    Handler handler;
    Runnable runnable;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (handler != null) {
            handler.removeCallbacks(runnable);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        replaceContentLayout(R.layout.activity_signup);

        ButterKnife.bind(this);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_PHONE_STATE}, 11);
        }

        ViewGroup main = (ViewGroup) findViewById(R.id.main);

        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            //code for portrait mode
            getWindow().setBackgroundDrawableResource(R.drawable.background);
        } else {
            //code for landscape mode
            getWindow().setBackgroundDrawableResource(R.drawable.landscape);
        }

        code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (countryCodes.size() > 0) {
                    showCodesDialog();
                }
            }
        });

        if(getIntent().getStringExtra("email") != null)
            email.setText(getIntent().getStringExtra("email"));
        if(getIntent().getStringExtra("fname") != null)
            fname.setText(getIntent().getStringExtra("fname"));
        if(getIntent().getStringExtra("lname") != null)
            lname.setText(getIntent().getStringExtra("lname"));

        /*countryNames.add("COUNTRY");
        countryCodes.add("0");
        countryIds.add("0");
        CountryAdapter adapter = new CountryAdapter(Signup.this, R.layout.country, R.id.title, countryNames);
        country.setAdapter(adapter);
        country.setTitle("Select Country");*/

        loadingCountries();
    }

    private void showCodesDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Country Code");
        String[] codes = new String[countryCodes.size()];
        for (int i = 0; i < countryCodes.size(); i++) {
            codes[i] = countryCodes.get(i);
        }
        builder.setSingleChoiceItems(codes, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                codeValue.setText(countryCodes.get(which));
                dialog.dismiss();
            }
        });
        builder.create();
        builder.show();
    }

    public void terms(View view) {
        startActivity(new Intent(this, TermsAndConditions.class));
    }

    public void finish(View view) {
        finish();
    }

    public void openMenu(View view) {
        super.drawerToggle();
    }

    public static boolean checkEmail(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    public static boolean checkForSpecial(String name) {
        return NAME_PATTERN.matcher(name).matches();
    }

    public void signupUser(View view) {
        if (fname.getText().toString().equals("")) {
            AppMsg.makeText(this, "Please enter first name!", AppMsg.STYLE_ALERT).show();
        } else if (lname.getText().toString().equals("")) {
            AppMsg.makeText(this, "Please enter last name!", AppMsg.STYLE_ALERT).show();
        } else if (email.getText().toString().equals("")) {
            AppMsg.makeText(this, "Please enter email!", AppMsg.STYLE_ALERT).show();
        } else if (password.getText().toString().equals("")) {
            AppMsg.makeText(this, "Please enter password!", AppMsg.STYLE_ALERT).show();
        } else if (country.getText().toString().equals("")) {
            AppMsg.makeText(this, "Please enter country!", AppMsg.STYLE_ALERT).show();
        } else if (mobile.getText().toString().equals("")) {
            AppMsg.makeText(this, "Please enter mobile number!", AppMsg.STYLE_ALERT).show();
        } else if (checkForSpecial(fname.getText().toString())) {
            if (checkForSpecial(lname.getText().toString())) {
                if (checkEmail(email.getText().toString())) {
                    if (checkForSpecial(country.getText().toString())) {
                        if (countryNames.contains(country.getText().toString())) {
                            if (password.length() < 6) {
                                AppMsg.makeText(this, "Password should be at least 6 letters! ", AppMsg.STYLE_ALERT).show();
                            } else {
                                if (checkbox.isChecked()) {
                                    registerUser();
                                } else {
                                    AppMsg.makeText(this, "You must accept terms and conditions!", AppMsg.STYLE_ALERT).show();
                                }
                            }
                        } else {
                            AppMsg.makeText(this, "Invalid country name!", AppMsg.STYLE_ALERT).show();
                        }
                    } else {
                        AppMsg.makeText(this, "Country can only contain Alphabets!", AppMsg.STYLE_ALERT).show();
                    }
                } else {
                    AppMsg.makeText(this, "Enter a valid email!", AppMsg.STYLE_ALERT).show();
                }
            } else
                AppMsg.makeText(this, "Last name can only contain Alphabets!", AppMsg.STYLE_ALERT).show();
        } else
            AppMsg.makeText(this, "First name can only contain Alphabets!", AppMsg.STYLE_ALERT).show();
    }

    private void registerUser() {
        showProgress(false);
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                firebaseToken = FirebaseInstanceId.getInstance().getToken();
                if (firebaseToken == null) {
                    handler.postDelayed(runnable, 1000);
                    return;
                }
                if (firebaseToken.equals("")) {
                    handler.postDelayed(runnable, 1000);
                    return;
                }
                // register now
                HashMap<String, String> hashMap = Constants.getSHA256();
                if (hashMap == null)
                    hashMap = new HashMap<>();
                hashMap.put("first_name", fname.getText().toString());
                hashMap.put("last_name", lname.getText().toString());
                hashMap.put("email", email.getText().toString());
                hashMap.put("password", password.getText().toString());
                String code = countryCodes.get(countryNames.indexOf(country.getText().toString()));
                hashMap.put("mobile", code + mobile.getText().toString());
                int index = countryNames.indexOf(country.getText().toString());
                hashMap.put("country", "" + countryIds.get(index));
                final TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
                if (ActivityCompat.checkSelfPermission(Signup.this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                    hashMap.put("device_id", "" + tm.getDeviceId());
                }
                hashMap.put("device_token", ""+firebaseToken);
                hashMap.put("device_type", "android");

                System.out.println(Arrays.asList(hashMap));

                CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.signup, hashMap, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("registerUser", response.toString());
                        hideProgress(false);
                        try {
                            String status = response.getString("status");
                            if (status.equalsIgnoreCase("success")) {
                                String uid = response.getString("memberid");
                                MyPref.setLogin(Signup.this, 1);
                                MyPref.setId(Signup.this, uid);
                                MyPref.setId(Signup.this, uid);
                                String cid = response.getString("campaign_id");
                                MyPref.setCampId(Signup.this, cid);
                                startActivity(new Intent(Signup.this, CreateCampaign.class));
                                finishAffinity();
                            }
                            else{
                                AppMsg.makeText(Signup.this, "Signup failed for some reason. Please try later!", AppMsg.STYLE_ALERT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            showFailedAlert();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress(false);
                        showFailedAlert();
                        Log.e("registerUser", error.toString());
                    }
                });
                VolleyLibrary.getInstance(Signup.this).addToRequestQueue(customRequest, "", false);
            }
        };
        handler.postDelayed(runnable, 10);
    }

    private void loadingCountries() {
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.countries, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("loadingCountries", response.toString());
                try {
                    if (countryNames.size() > 0) countryNames.clear();
                    if (countryCodes.size() > 0) countryCodes.clear();
                    countryIds.clear();

                    String status = response.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray countries = response.getJSONArray("countries");
                        for (int i = 0; i < countries.length(); i++) {
                            String country = countries.getJSONObject(i).getString("title");
                            String code = countries.getJSONObject(i).getString("phonecode");
                            countryNames.add(country);
                            countryCodes.add(code);
                            String countryId = countries.getJSONObject(i).getString("id");
                            countryIds.add(countryId);
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(Signup.this, android.R.layout.simple_spinner_dropdown_item, countryNames);
                        country.setAdapter(adapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("loadingCountries", error.toString());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(country != null)
                            loadingCountries();
                    }
                }, 1000);
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueueWithoutInternetCheck(customRequest, "", false);
    }
}
