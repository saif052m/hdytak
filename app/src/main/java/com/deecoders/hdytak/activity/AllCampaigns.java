package com.deecoders.hdytak.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deecoders.hdytak.R;
import com.deecoders.hdytak.adapter.CampaignDetailsAdapter;
import com.deecoders.hdytak.model.CampaignModel;
import com.deecoders.hdytak.network.CustomRequest;
import com.deecoders.hdytak.network.VolleyLibrary;
import com.deecoders.hdytak.util.Constants;
import com.deecoders.hdytak.view.ExpandableHeightGridView;
import com.deecoders.hdytak.view.MyTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AllCampaigns extends BaseActivity {
    ExpandableHeightGridView gridView;
    CampaignDetailsAdapter campaignDetailsAdapter;
    ArrayList<CampaignModel> campaignModels = new ArrayList<>();
    @BindView(R.id.title1)
    MyTextView title1;
    @BindView(R.id.title2)
    MyTextView title2;
    @BindView(R.id.loadMore)
    MyTextView loadMore;
    @BindView(R.id.search)
    EditText search;
    @BindView(R.id.submit)
    ImageView submit;
    boolean loading = false;
    int offset = 6;
    boolean firstLoad = true;
    @BindView(R.id.noResult)
    TextView noResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.replaceContentLayout(R.layout.activity_all_campaigns);

        ButterKnife.bind(this);

        ViewGroup main = (ViewGroup) findViewById(R.id.main);

        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            //code for portrait mode
            getWindow().setBackgroundDrawableResource(R.drawable.background);
        } else {
            //code for landscape mode
            getWindow().setBackgroundDrawableResource(R.drawable.landscape);
        }

        gridView = (ExpandableHeightGridView) findViewById(R.id.gridView);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(search.getText().toString().isEmpty())
                    return;
                searchCampaigns(search.getText().toString());
            }
        });

        allCampaigns(true);
    }

    private void allCampaigns(final boolean flag) {
        loading = true;
        showProgress(flag);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();

        String url = Constants.all_campaigns_paginate;
        if(offset != 6)
            url = Constants.all_campaigns_paginate + offset;

        Log.e("tag", "url: "+url);

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, url, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(flag);
                try {
                    String status = response.getString("status");
                    if (status.equals("success")) {
                        JSONArray campaigns = response.getJSONArray("campaigns");
                        final int oldSize, newSize;
                        oldSize = campaignModels.size();
                        for (int i = 0; i < campaigns.length(); i++) {
                            //id, created_date, title, salling_price, description, image;
                            CampaignModel model = new CampaignModel();
                            model.setId(campaigns.getJSONObject(i).getString("id"));
                            model.setCreated_date(campaigns.getJSONObject(i).getString("created_date"));
                            model.setTitle(campaigns.getJSONObject(i).getString("title"));
                            model.setDescription(campaigns.getJSONObject(i).getString("description"));
                            model.setImage(campaigns.getJSONObject(i).getString("image"));
                            model.setUrlTitle(campaigns.getJSONObject(i).getString("url_title"));
                            model.setVisible(campaigns.getJSONObject(i).getString("visible"));
                            model.setOnePayment(campaigns.getJSONObject(i).getString("one_payment"));
                            model.setQrImage(campaigns.getJSONObject(i).getString("qr_image"));
                            campaignModels.add(model);
                        }
                        newSize = campaignModels.size();

                        if(oldSize == newSize){
                            loadMore.setVisibility(View.GONE);
                        }

                        if(campaignModels.size() % 6 != 0){
                            loadMore.setVisibility(View.GONE);
                        }

                        if(campaignModels.size()==1)
                            gridView.setNumColumns(1);
                        else
                            gridView.setNumColumns(2);

                        if(campaignDetailsAdapter == null) {
                            campaignDetailsAdapter = new CampaignDetailsAdapter(AllCampaigns.this, R.layout.product_item, campaignModels);
                            gridView.setAdapter(campaignDetailsAdapter);
                            gridView.setExpanded(true);
                        }
                        else{
                            campaignDetailsAdapter.setModels(campaignModels);
                        }

                        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                                Intent intent = new Intent(AllCampaigns.this, PayNow.class);
                                intent.putExtra("camp_model", campaignModels.get(pos));
                                startActivity(intent);
                            }
                        });

                        loading = false;
                        if(offset == 6){
                            offset = 12;
                        }
                        else {
                            offset = offset + 12;
                        }

                        if(campaignModels.size() == 0)
                            noResult.setVisibility(View.VISIBLE);
                        else
                            noResult.setVisibility(View.GONE);
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    private void searchCampaigns(String keyword) {
        showProgress(false);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("keyword", "" + keyword);

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.campaign_search, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(false);
                try {
                    campaignModels.clear();
                    JSONArray campaigns = response.getJSONArray("campaign_search");
                    for (int i = 0; i < campaigns.length(); i++) {
                        //id, created_date, title, salling_price, description, image;
                        CampaignModel model = new CampaignModel();
                        model.setId(campaigns.getJSONObject(i).getString("id"));
                        model.setCreated_date(campaigns.getJSONObject(i).getString("created_date"));
                        model.setTitle(campaigns.getJSONObject(i).getString("title"));
                        model.setDescription(campaigns.getJSONObject(i).getString("description"));
                        model.setImage(campaigns.getJSONObject(i).getString("image"));
                        model.setUrlTitle(campaigns.getJSONObject(i).getString("url_title"));
                        model.setVisible(campaigns.getJSONObject(i).getString("visible"));
                        model.setOnePayment(campaigns.getJSONObject(i).getString("one_payment"));
                        model.setQrImage(campaigns.getJSONObject(i).getString("qr_image"));
                        campaignModels.add(model);
                    }

                    if(campaignModels.size()==1)
                        gridView.setNumColumns(1);
                    else
                        gridView.setNumColumns(2);

                    if (campaignDetailsAdapter != null) {
                        campaignDetailsAdapter.setModels(campaignModels);
                    } else {
                        campaignDetailsAdapter = new CampaignDetailsAdapter(AllCampaigns.this, R.layout.product_item, campaignModels);
                        gridView.setAdapter(campaignDetailsAdapter);
                        gridView.setExpanded(true);
                    }

                    gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                            Intent intent = new Intent(AllCampaigns.this, PayNow.class);
                            intent.putExtra("camp_model", campaignModels.get(pos));
                            startActivity(intent);
                        }
                    });

                    if(campaignModels.size() == 0)
                        noResult.setVisibility(View.VISIBLE);
                    else
                        noResult.setVisibility(View.GONE);

                    if(campaignModels.size() % 6 != 0){
                        loadMore.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    public void finish(View view) {
        finish();
    }

    public void openMenu(View view) {
        super.drawerToggle();
    }

    public void openCampaign(View view) {
        startActivity(new Intent(this, MyCampaign.class));
    }

    public void loadMore(View view) {
        loading = true;
        showProgress(false);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();

        String url = Constants.all_campaigns_paginate;
        if(offset != 6)
            url = Constants.all_campaigns_paginate + offset;

        Log.e("tag", "url: "+url);

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, url, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(false);
                try {
                    String status = response.getString("status");
                    if (status.equals("success")) {
                        JSONArray campaigns = response.getJSONArray("campaigns");
                        final int oldSize, newSize;
                        oldSize = campaignModels.size();
                        for (int i = 0; i < campaigns.length(); i++) {
                            //id, created_date, title, salling_price, description, image;
                            CampaignModel model = new CampaignModel();
                            model.setId(campaigns.getJSONObject(i).getString("id"));
                            model.setCreated_date(campaigns.getJSONObject(i).getString("created_date"));
                            model.setTitle(campaigns.getJSONObject(i).getString("title"));
                            model.setDescription(campaigns.getJSONObject(i).getString("description"));
                            model.setImage(campaigns.getJSONObject(i).getString("image"));
                            model.setUrlTitle(campaigns.getJSONObject(i).getString("url_title"));
                            model.setVisible(campaigns.getJSONObject(i).getString("visible"));
                            model.setOnePayment(campaigns.getJSONObject(i).getString("one_payment"));
                            model.setQrImage(campaigns.getJSONObject(i).getString("qr_image"));
                            campaignModels.add(model);
                        }
                        newSize = campaignModels.size();

                        if(oldSize == newSize){
                            loadMore.setVisibility(View.GONE);
                        }

                        if(campaignModels.size()==1)
                            gridView.setNumColumns(1);
                        else
                            gridView.setNumColumns(2);

                        if(campaignDetailsAdapter == null) {
                            campaignDetailsAdapter = new CampaignDetailsAdapter(AllCampaigns.this, R.layout.product_item, campaignModels);
                            gridView.setAdapter(campaignDetailsAdapter);
                            gridView.setExpanded(true);
                        }
                        else{
                            campaignDetailsAdapter.setModels(campaignModels);
                        }

                        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                                Intent intent = new Intent(AllCampaigns.this, PayNow.class);
                                intent.putExtra("camp_model", campaignModels.get(pos));
                                startActivity(intent);
                            }
                        });

                        loading = false;
                        if(offset == 6){
                            offset = 12;
                        }
                        else {
                            offset = offset + 12;
                        }

                        if(campaignModels.size() == 0)
                            noResult.setVisibility(View.VISIBLE);
                        else
                            noResult.setVisibility(View.GONE);

                        if(campaignModels.size() % 6 != 0){
                            loadMore.setVisibility(View.GONE);
                        }
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }
}
