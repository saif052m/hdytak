package com.deecoders.hdytak.activity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.deecoders.hdytak.R;
import com.deecoders.hdytak.model.CampaignModel;
import com.deecoders.hdytak.network.CustomRequest;
import com.deecoders.hdytak.network.LruBitmapCache;
import com.deecoders.hdytak.network.VolleyLibrary;
import com.deecoders.hdytak.util.Constants;
import com.deecoders.hdytak.util.MyPref;
import com.google.firebase.FirebaseApp;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;




public class Splash extends AppCompatActivity {
    NetworkImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if(getIntent().hasExtra("paramm")){
            finish();
            return;
        }

        //showTestNotification();

        FirebaseApp.initializeApp(this);

        Log.e("tag", "test: "+Constants.getFormatedAmount("100000.25"));

        // url clicked in sms
        Uri data = getIntent().getData();
        if(data != null) {
            String strData = data.toString();
            if (strData != null) {
                if (strData.contains("https://www.hdytak.com")) {
                    Log.e("tag", "url: " + strData);
                    getCampDetailsFromUrl(strData);
                    return;
                }
            }
        }

        ViewGroup main = (ViewGroup)findViewById(R.id.main);
        imageView = (NetworkImageView)findViewById(R.id.imageView);

        RequestQueue mRequestQueue = Volley.newRequestQueue(this);
        ImageLoader mImageLoader = new ImageLoader(mRequestQueue, new LruBitmapCache());

        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            //code for portrait mode
            //imageView.setImageUrl(Constants.portraitImage, mImageLoader);
        } else {
            //code for landscape mode
            //imageView.setImageUrl(Constants.landscapeImage, mImageLoader);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(MyPref.getLogin(Splash.this) == 0) {
                    startActivity(new Intent(Splash.this, Campaigns.class));
                    finish();
                }
                else{
                    startActivity(new Intent(Splash.this, CreateCampaign.class));
                    finish();
                }
            }
        }, 3000);
    }

    private void getCampDetailsFromUrl(String url) {
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("url", ""+url);

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.campaign_details_by_url, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("scan", response.toString());
                try {
                    String status = response.getString("status");
                    String msg = response.getString("message");
                    if (status.equalsIgnoreCase("success")) {
                        //AppMsg.makeText(BaseActivity.this, ""+msg, AppMsg.STYLE_INFO).show();
                        JSONObject campaign = response.getJSONObject("campaign");
                        CampaignModel model = new CampaignModel();
                        model.setId(campaign.getString("id"));
                        model.setCreated_date(campaign.getString("created_date"));
                        model.setTitle(campaign.getString("title"));
                        model.setDescription(campaign.getString("description"));
                        model.setImage(campaign.getString("image"));
                        model.setUrlTitle(campaign.getString("url_title"));
                        model.setVisible(campaign.getString("visible"));
                        model.setOnePayment(campaign.getString("one_payment"));
                        model.setQrImage(campaign.getString("qr_image"));
                        Intent intent = new Intent(Splash.this, PayNow.class);
                        intent.putExtra("camp_model", model);
                        startActivity(intent);
                        finish();
                    }
                    else{
                        //Toast.makeText(Splash.this, "There is some problem loading campaign. Please try later.", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    private void loadingBackground(){
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("page_id", "8");
        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.backgrounds, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("loadingBackground", response.toString());
                try {
                    String status = response.getString("status");
                    if(status.equalsIgnoreCase("success")){
                        String active = response.getJSONObject("backgrounds").getString("active");
                        if(active.equalsIgnoreCase("1")){
                            String image = response.getJSONObject("backgrounds").getString("image");
                            String image_landscape = response.getJSONObject("backgrounds").getString("image_landscape");
                        }
                        else{
                            // do nothing
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("loadingBackground", error.toString());
            }
        });
        VolleyLibrary.getInstance(Splash.this).addToRequestQueue(customRequest, "", false);
    }

    public void showTestNotification(){
        Log.e("message", "notification came");
        // show transparent screen
        Intent b = new Intent(this, Transparent.class);
        b.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(b);

        // on click open activities screen
        Intent intent = new Intent(this, Splash.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Random r = new Random();
        PendingIntent pendingIntent = PendingIntent.getActivity(this, r.nextInt(10000), intent, PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
        notificationBuilder.setContentTitle("HDYTAK: Payment Alert");
        notificationBuilder.setContentText("hello tesing");
        notificationBuilder.setAutoCancel(true);
        notificationBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
        //notificationBuilder.setVibrate(new long[] {1000, 1000, 1000, 1000, 1000});
        notificationBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setColor(getResources().getColor(R.color.textColorBlue));
            notificationBuilder.setSmallIcon(R.mipmap.gift);
            notificationBuilder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.app_icon));
        }
        else {
            notificationBuilder.setSmallIcon(R.drawable.app_icon);
        }
        notificationBuilder.setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(r.nextInt(10000), notificationBuilder.build());
    }
}
