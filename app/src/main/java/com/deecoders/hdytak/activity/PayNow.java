package com.deecoders.hdytak.activity;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.deecoders.hdytak.R;
import com.deecoders.hdytak.adapter.MyCampaignProductAdapter;
import com.deecoders.hdytak.model.CampaignModel;
import com.deecoders.hdytak.model.ProductModel;
import com.deecoders.hdytak.network.CustomRequest;
import com.deecoders.hdytak.network.LruBitmapCache;
import com.deecoders.hdytak.network.VolleyLibrary;
import com.deecoders.hdytak.util.CircularNetworkImageView;
import com.deecoders.hdytak.util.Constants;
import com.deecoders.hdytak.util.CustomNetworkImageView;
import com.deecoders.hdytak.util.MyPref;
import com.deecoders.hdytak.view.ExpandableHeightGridView;
import com.deecoders.hdytak.view.MyAutoCompleteTextView;
import com.deecoders.hdytak.view.MyEditText;
import com.deecoders.hdytak.view.MyTextView;
import com.devspark.appmsg.AppMsg;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PayNow extends BaseActivity {
    ArrayList<ProductModel> productModels = new ArrayList<>();
    MyCampaignProductAdapter productAdapter;
    @BindView(R.id.profile_image)
    CircularNetworkImageView profileImage;
    @BindView(R.id.title)
    MyTextView title;
    @BindView(R.id.event)
    MyTextView event;
    @BindView(R.id.gridView)
    ExpandableHeightGridView gridView;
    @BindView(R.id.total)
    MyTextView total;
    @BindView(R.id.paid)
    MyTextView paid;
    @BindView(R.id.remaining)
    MyTextView remaining;
    @BindView(R.id.amount)
    MyEditText amount;
    @BindView(R.id.fname)
    MyEditText fname;
    @BindView(R.id.lname)
    MyEditText lname;
    @BindView(R.id.email)
    MyEditText email;
    @BindView(R.id.code)
    LinearLayout code;
    @BindView(R.id.country)
    MyAutoCompleteTextView country;
    @BindView(R.id.mobile)
    MyEditText mobile;
    @BindView(R.id.city)
    MyEditText city;
    @BindView(R.id.state)
    MyEditText state;
    @BindView(R.id.address1)
    MyEditText address1;
    @BindView(R.id.address2)
    MyEditText address2;
    @BindView(R.id.zipcode)
    MyEditText zipcode;
    @BindView(R.id.pay_now)
    MyTextView payNow;
    @BindView(R.id.email_)
    MyEditText email_;
    @BindView(R.id.send_email)
    MyTextView sendEmail;
    @BindView(R.id.name)
    MyEditText name;
    @BindView(R.id.code_)
    LinearLayout code_;
    @BindView(R.id.country_code)
    BetterSpinner country_;
    @BindView(R.id.mobile_)
    MyEditText mobile_;
    @BindView(R.id.send_sms)
    MyTextView sendSms;
    CampaignModel campaignModel;
    boolean foreground;
    @BindView(R.id.codeTxt)
    MyTextView codeTxt;
    @BindView(R.id.codeTxt_)
    MyTextView codeTxt_;
    @BindView(R.id.qrCodePanel)
    ViewGroup qrCodePanel;
    @BindView(R.id.qr_image)
    CustomNetworkImageView qrImage;

    ArrayList<String> countryNames = new ArrayList<>();
    ArrayList<String> countryCodes = new ArrayList<>();
    ArrayList<String> countryIds = new ArrayList<>();
    int selectedCodeIndex = -1;
    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile("[a-zA-Z0-9+._%-+]{1,256}" +
            "@" +
            "[a-zA-Z0-9][a-zA-Z0-9-]{0,64}" +
            "(" +
            "." +
            "[a-zA-Z0-9][a-zA-Z0-9-]{0,25}" +
            ")+");

    public static final Pattern NAME_PATTERN = Pattern.compile("[a-zA-Z ]*");
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;
    // note that these credentials will differ between live & sandbox environments.
    private static final String CONFIG_CLIENT_ID = "AVXdb8gfB2Uqws5S_N7VWctTIV9q8SR_91oC-CS1c4kJBJy_IUKzQ15RL3w7dupUd7R27_zlvXwxpwzg";
    private static final int REQUEST_CODE_PAYMENT = 1;
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID);
    String paypalOrderNo, payfortOrderNo, url;

    @BindView(R.id.totalPanel)
    LinearLayout totalPanel;
    @BindView(R.id.paidPanel)
    LinearLayout paidPanel;
    @BindView(R.id.remainingPanel)
    LinearLayout remainingPanel;
    boolean canPay = true;
    @BindView(R.id.line)
    View line;
    @BindView(R.id.download)
    TextView download;
    String show_paid_ = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        replaceContentLayout(R.layout.activity_pay_now);

        ButterKnife.bind(this);

        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_PHONE_STATE}, 27);
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 28);
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 29);
        }

        ViewGroup main = (ViewGroup) findViewById(R.id.main);

        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            //code for portrait mode
            getWindow().setBackgroundDrawableResource(R.drawable.background);
        } else {
            //code for landscape mode
            getWindow().setBackgroundDrawableResource(R.drawable.landscape);
        }

        campaignModel = (CampaignModel) getIntent().getSerializableExtra("camp_model");
        if (campaignModel == null) {
            AppMsg.makeText(this, "Unable to load campaign data!", AppMsg.STYLE_ALERT).show();
            return;
        }

        if (MyPref.getUsername(this) != null)
            fname.setText(MyPref.getUsername(this));
        if (MyPref.getEmail(this) != null)
            email.setText(MyPref.getEmail(this));

        if (campaignModel.getQrImage() != null) {
            if (!campaignModel.getQrImage().isEmpty()) {
                qrCodePanel.setVisibility(View.VISIBLE);
                RequestQueue mRequestQueue = Volley.newRequestQueue(PayNow.this);
                ImageLoader mImageLoader = new ImageLoader(mRequestQueue, new LruBitmapCache());
                qrImage.setImageUrl(Constants.campaignImageUrl + campaignModel.getQrImage(), mImageLoader);
                qrImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!canPay) {
                            AppMsg.makeText(PayNow.this, "Sorry! this campaign is closed.", AppMsg.STYLE_ALERT).show();
                            return;
                        }
                        downloadImageInGallery(Constants.campaignImageUrl + campaignModel.getQrImage());
                        Toast.makeText(PayNow.this, "Downloading...", Toast.LENGTH_SHORT).show();
                    }

                    private void downloadImageInGallery(String url) {
                        Random r = new Random();
                        String fileName = "QR" + r.nextInt(10000) + r.nextInt(10000) + r.nextInt(10000) + ".";
                        DownloadManager dm = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                        Uri downloadUri = Uri.parse(url);
                        DownloadManager.Request request = new DownloadManager.Request(downloadUri);
                        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                                .setAllowedOverRoaming(false)
                                .setTitle("Downloading QR Image")
                                .setMimeType("image/jpeg")
                                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                                .setDestinationInExternalPublicDir(Environment.DIRECTORY_PICTURES, File.separator + fileName);
                        dm.enqueue(request);
                    }
                });
                download.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!canPay) {
                            AppMsg.makeText(PayNow.this, "Sorry! this campaign is closed.", AppMsg.STYLE_ALERT).show();
                            return;
                        }
                        downloadImageInGallery(Constants.campaignImageUrl + campaignModel.getQrImage());
                        Toast.makeText(PayNow.this, "Downloading...", Toast.LENGTH_SHORT).show();
                    }

                    private void downloadImageInGallery(String url) {
                        Random r = new Random();
                        String fileName = "QR" + r.nextInt(10000) + r.nextInt(10000) + r.nextInt(10000) + ".";
                        DownloadManager dm = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                        Uri downloadUri = Uri.parse(url);
                        DownloadManager.Request request = new DownloadManager.Request(downloadUri);
                        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                                .setAllowedOverRoaming(false)
                                .setTitle("Downloading QR Image")
                                .setMimeType("image/jpeg")
                                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                                .setDestinationInExternalPublicDir(Environment.DIRECTORY_PICTURES, File.separator + fileName);
                        dm.enqueue(request);
                    }
                });
            }
        }

        campaignDetails();
        campaignProducts();
        campaignTotalPaidRemaining();
        loadingCountries();

        payNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!canPay) {
                    AppMsg.makeText(PayNow.this, "Sorry! cannot pay.", AppMsg.STYLE_ALERT).show();
                    return;
                }
                makePayment();
            }
        });

        sendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!canPay) {
                    AppMsg.makeText(PayNow.this, "Sorry! this campaign is closed.", AppMsg.STYLE_ALERT).show();
                    return;
                }

                if (email_.getText().toString().isEmpty()) {
                    AppMsg.makeText(PayNow.this, "Enter email!", AppMsg.STYLE_ALERT).show();
                    return;
                }
                sendEmailOnServer();
            }
        });

        sendSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!canPay) {
                    AppMsg.makeText(PayNow.this, "Sorry! this campaign is closed.", AppMsg.STYLE_ALERT).show();
                    return;
                }

                if (name.getText().toString().isEmpty()) {
                    AppMsg.makeText(PayNow.this, "Enter name!", AppMsg.STYLE_ALERT).show();
                } else if (country_.getText().toString().isEmpty()) {
                    AppMsg.makeText(PayNow.this, "Select country!", AppMsg.STYLE_ALERT).show();
                } else if (!countryNames.contains(country_.getText().toString())) {
                    AppMsg.makeText(PayNow.this, "Invalid country!", AppMsg.STYLE_ALERT).show();
                } else if (mobile_.getText().toString().isEmpty()) {
                    AppMsg.makeText(PayNow.this, "Enter mobile!!", AppMsg.STYLE_ALERT).show();
                } else {
                    sendSmsOnServer();
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_PHONE_STATE}, 27);
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 28);
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 29);
        }
    }

    private void sendSmsOnServer() {
        showProgress(false);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if (hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("campaign_id", "" + campaignModel.getId());
        hashMap.put("name", "" + name.getText().toString());
        int index = countryNames.indexOf(country_.getText().toString());
        hashMap.put("country", "" + countryIds.get(index));
        String code = countryCodes.get(index);
        hashMap.put("phone", code + mobile_.getText().toString());

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.send_sms_invitation, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(false);
                try {
                    String status = response.getString("status");
                    String message = response.getString("message");
                    if (status.equalsIgnoreCase("success")) {
                        AppMsg.makeText(PayNow.this, "" + message, AppMsg.STYLE_INFO).show();
                    } else {
                        AppMsg.makeText(PayNow.this, "" + message, AppMsg.STYLE_ALERT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    private void sendEmailOnServer() {
        showProgress(false);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if (hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("campaign_id", "" + campaignModel.getId());
        hashMap.put("email", "" + email.getText().toString());

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.send_email_invitations, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(false);
                try {
                    String status = response.getString("status");
                    String message = response.getString("message");
                    if (status.equalsIgnoreCase("success")) {
                        AppMsg.makeText(PayNow.this, "" + message, AppMsg.STYLE_INFO).show();
                    } else {
                        AppMsg.makeText(PayNow.this, "" + message, AppMsg.STYLE_ALERT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    public static boolean checkEmail(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    private void makePayment() {
        if (amount.getText().toString().isEmpty()) {
            AppMsg.makeText(this, "Enter amount!", AppMsg.STYLE_ALERT).show();
        } else if (fname.getText().toString().isEmpty()) {
            AppMsg.makeText(this, "Enter name!", AppMsg.STYLE_ALERT).show();
        } else if (email.getText().toString().isEmpty()) {
            AppMsg.makeText(this, "Enter email!", AppMsg.STYLE_ALERT).show();
        } else if (!checkEmail(email.getText().toString())) {
            AppMsg.makeText(this, "Invalid email!", AppMsg.STYLE_ALERT).show();
        } else {
            makePaymentOnServer(0);
        }
    }

    private void makePaymentOnServer(final int type) {
        showProgress(false);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if (hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("campaign_id", "" + campaignModel.getId());
        hashMap.put("amount", "" + amount.getText().toString());
        hashMap.put("name", "" + fname.getText().toString());
        hashMap.put("email", "" + email.getText().toString());
        if (type == 0)
            hashMap.put("type", "payfort");
        else
            hashMap.put("type", "paypal");

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.pay_campaign, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(false);
                try {
                    if (type == 0) {
                        payfortOrderNo = response.getString("merchant_reference");
                        String content = response.getString("html");
                        Intent intent = new Intent(PayNow.this, WebPayment.class);
                        intent.putExtra("content", content);
                        intent.putExtra("orderNo", payfortOrderNo);
                        intent.putExtra("amount", amount.getText().toString());
                        startActivity(intent);
                    } else {
                        paypalOrderNo = response.getString("merchant_reference");
                        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);
                        Intent intent = new Intent(PayNow.this, PaymentActivity.class);
                        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
                        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
                        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    private void loadingCountries() {
        HashMap<String, String> hashMap = Constants.getSHA256();
        if (hashMap == null)
            hashMap = new HashMap<>();
        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.countries, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("loadingCountries", response.toString());
                try {
                    if (countryNames.size() > 0) countryNames.clear();
                    if (countryCodes.size() > 0) countryCodes.clear();
                    countryIds.clear();

                    String status = response.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray countries = response.getJSONArray("countries");
                        for (int i = 0; i < countries.length(); i++) {
                            String countryId = countries.getJSONObject(i).getString("id");
                            String country = countries.getJSONObject(i).getString("title");
                            String code = countries.getJSONObject(i).getString("phonecode");
                            countryNames.add(country);
                            countryCodes.add(code);
                            countryIds.add(countryId);
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(PayNow.this, android.R.layout.simple_spinner_dropdown_item, countryNames);
                        country.setAdapter(adapter);
                        country.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Log.e("tag", "click");
                                String cntry = country.getText().toString();
                                int index = countryNames.indexOf(cntry);
                                codeTxt.setText(countryCodes.get(index));
                            }
                        });
                        code.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showCodesDialog(1);
                            }
                        });

                        country_.setAdapter(adapter);

//                        country_.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                            @Override
//                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                                Log.e("tag", "click");
//                                String cntry = country_.getText().toString();
//                                int index = countryNames.indexOf(cntry);
//                                codeTxt_.setText(countryCodes.get(index));
//                            }
//                        });
//                        code_.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                showCodesDialog(2);
//                            }
//                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("loadingCountries", error.toString());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (country != null)
                            loadingCountries();
                    }
                }, 1000);
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueueWithoutInternetCheck(customRequest, "", false);
    }

    private void showCodesDialog(final int type) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose");
        String[] codes = new String[countryCodes.size()];
        for (int i = 0; i < countryCodes.size(); i++) {
            codes[i] = countryCodes.get(i);
        }
        builder.setSingleChoiceItems(codes, selectedCodeIndex, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedCodeIndex = which;
                if (type == 1) {
                    codeTxt.setText(countryCodes.get(which));
                    country.setText(countryNames.get(which));
                }
                if (type == 2) {
                    codeTxt_.setText(countryCodes.get(which));
                    country_.setText(countryNames.get(which));
                }
                dialog.dismiss();
            }
        });
        builder.create();
        builder.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        foreground = true;
        //if(payfortOrderNo != null){
        //    checkPaymentSuccess();
        //}
    }

    private void checkPaymentSuccess() {
        showProgress(false);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if (hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("order_number", "" + payfortOrderNo);

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.payment_result, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(false);
                String status = null, msg = null;
                try {
                    status = response.getString("status");
                    msg = response.getString("message");
                    if (status.equals("success")) {
                        AppMsg.makeText(PayNow.this, "" + msg, AppMsg.STYLE_INFO).show();
                    } else {
                        AppMsg.makeText(PayNow.this, "" + msg, AppMsg.STYLE_ALERT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    @Override
    protected void onPause() {
        super.onPause();
        foreground = false;
    }

    private void campaignDetails() {
        showProgress(true);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if (hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("campaign_id", "" + campaignModel.getId());

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.campaign_details, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(true);
                try {
                    JSONObject data = response.getJSONObject("campaign");
                    title.setText(data.getString("title"));
                    event.setText(data.getString("description"));
                    campaignModel.setQrImage(data.getString("qr_image"));
                    RequestQueue mRequestQueue = Volley.newRequestQueue(PayNow.this);
                    ImageLoader mImageLoader = new ImageLoader(mRequestQueue, new LruBitmapCache());
                    profileImage.setImageUrl(Constants.campaignImageUrl + data.getString("image"), mImageLoader);
                    if (campaignModel.getQrImage() != null) {
                        if (!campaignModel.getQrImage().isEmpty()) {
                            qrCodePanel.setVisibility(View.VISIBLE);
                            qrImage.setImageUrl(Constants.campaignImageUrl + campaignModel.getQrImage(), mImageLoader);
                        }
                    }

                    if (data.getString("active").equals("0")) {
                        canPay = false;
                    }

                    show_paid_ = data.getString("show_paid");
                } catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    private void campaignProducts() {
        showProgress(false);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if (hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("campaign_id", "" + campaignModel.getId());
        hashMap.put("member_id", "" + MyPref.getId(this));

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.campaign_products, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(false);
                try {
                    String status = response.getString("status");
                    if (status.equals("success")) {
                        productModels.clear();
                        JSONArray products = response.getJSONArray("campaign_products");
                        JSONArray items = response.getJSONArray("campaign_items");
                        for (int i = 0; i < items.length(); i++) {
                            //id, created_date, title, salling_price, description, image;
                            ProductModel model = new ProductModel();
                            model.setId(items.getJSONObject(i).getString("id"));
                            model.setCreated_date(items.getJSONObject(i).getString("created_date"));
                            model.setTitle(items.getJSONObject(i).getString("title"));
                            model.setSalling_price(items.getJSONObject(i).getString("salling_price"));
                            model.setDescription(items.getJSONObject(i).getString("description"));
                            model.setImage(items.getJSONObject(i).getString("image"));
                            model.setType(1);
                            productModels.add(model);
                        }
                        for (int i = 0; i < products.length(); i++) {
                            //id, created_date, title, salling_price, description, image;
                            ProductModel model = new ProductModel();
                            model.setId(products.getJSONObject(i).getString("id"));
                            model.setCreated_date(products.getJSONObject(i).getString("created_date"));
                            model.setTitle(products.getJSONObject(i).getString("title"));
                            model.setSalling_price(products.getJSONObject(i).getString("price"));
                            model.setDescription(products.getJSONObject(i).getString("description"));
                            model.setImage(products.getJSONObject(i).getString("image"));
                            model.setType(0);
                            productModels.add(model);
                        }

                        Log.e("tag", "size: " + productModels.size());

                        if (productModels.size() == 1)
                            gridView.setNumColumns(1);
                        else
                            gridView.setNumColumns(2);

                        productAdapter = new MyCampaignProductAdapter(PayNow.this, R.layout.product_item1, productModels, false);

                        gridView.setAdapter(productAdapter);
                        gridView.setExpanded(true);
                        final ScrollView scrollView = findViewById(R.id.scrollView);
                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {
                                scrollView.scrollTo(0, 0);
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    private void campaignTotalPaidRemaining() {
        //showProgress();
        HashMap<String, String> hashMap = Constants.getSHA256();
        if (hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("campaign_id", "" + campaignModel.getId());
        hashMap.put("member_id", "" + MyPref.getId(this));

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.camaign_total_paid_remaining, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                //hideProgress();
                try {
                    String status = response.getString("status");
                    if (status.equals("success")) {
                        String total_ = response.getJSONObject("result").getString("total");
                        String paid_ = response.getJSONObject("result").getString("paid");
                        String remaining_ = response.getJSONObject("result").getString("remaining_value");
                        total.setText(Constants.getFormatedAmount(total_) + "$");
                        paid.setText(Constants.getFormatedAmount(paid_) + "$");
                        remaining.setText(Constants.getFormatedAmount(remaining_) + "$");

                        totalPanel.setVisibility(View.VISIBLE);
                        paidPanel.setVisibility(View.VISIBLE);
                        remainingPanel.setVisibility(View.VISIBLE);
                        line.setVisibility(View.VISIBLE);

                        if (total_ == null) {
                            totalPanel.setVisibility(View.GONE);
                        } else if (total_.isEmpty()) {
                            totalPanel.setVisibility(View.GONE);
                        }

                        if (paid_ == null) {
                            paidPanel.setVisibility(View.GONE);
                        } else if (paid_.isEmpty()) {
                            paidPanel.setVisibility(View.GONE);
                        }

                        if (remaining_ == null) {
                            remainingPanel.setVisibility(View.GONE);
                        } else if (remaining_.isEmpty()) {
                            remainingPanel.setVisibility(View.GONE);
                        }

                        if (remaining_ != null) {
                            if (remaining_.startsWith("0")) {
                                canPay = false;
                            }
                        }

                        if (show_paid_.equals("0")) {
                            paidPanel.setVisibility(View.GONE);
                        }

                        if (!totalPanel.isShown() && !paidPanel.isShown() && !remainingPanel.isShown()) {
                            line.setVisibility(View.GONE);
                        }

                        // loop
                        if (foreground) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    campaignTotalPaidRemaining();
                                }
                            }, 5000);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //hideProgress();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    public void finish(View view) {
        finish();
    }

    public void openMenu(View view) {
        super.drawerToggle();
    }

    public void payNow(View view) {
        if (!canPay) {
            AppMsg.makeText(PayNow.this, "Sorry! cannot pay.", AppMsg.STYLE_ALERT).show();
            return;
        }
        startActivity(new Intent(this, PaymentSuccess.class));
    }

    public void shareProduct(View view) {
        if (!canPay) {
            AppMsg.makeText(PayNow.this, "Sorry! cannot share this campaign because it is closed.", AppMsg.STYLE_ALERT).show();
            return;
        }
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, campaignModel.getTitle());
        i.putExtra(Intent.EXTRA_TEXT, Constants.campaignShareUrl + campaignModel.getUrlTitle());
        startActivity(Intent.createChooser(i, "Share Campaign"));
    }

    private void paypalPayment1() {
        if (amount.getText().toString().isEmpty()) {
            AppMsg.makeText(this, "Enter amount!", AppMsg.STYLE_ALERT).show();
        } else if (fname.getText().toString().isEmpty()) {
            AppMsg.makeText(this, "Enter name!", AppMsg.STYLE_ALERT).show();
        } else if (email.getText().toString().isEmpty()) {
            AppMsg.makeText(this, "Enter email!", AppMsg.STYLE_ALERT).show();
        } else if (!checkEmail(email.getText().toString())) {
            AppMsg.makeText(this, "Invalid email!", AppMsg.STYLE_ALERT).show();
        } else {
            makePaymentOnServer(1);
        }
    }

    public void paypalPayment(View view) {
        if (!canPay) {
            AppMsg.makeText(PayNow.this, "Sorry! cannot pay.", AppMsg.STYLE_ALERT).show();
            return;
        }
        paypalPayment1();
    }

    private PayPalPayment getThingToBuy(String paymentIntent) {
        return new PayPalPayment(new BigDecimal(amount.getText().toString()), "USD", campaignModel.getTitle(), paymentIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.e("tag", "" + confirm.toJSONObject().toString(4));
                        Log.e("tag", "" + confirm.getPayment().toJSONObject().toString(4));
                        String tid = confirm.toJSONObject().getJSONObject("response").getString("id");
                        String status = confirm.toJSONObject().getJSONObject("response").getString("state");
                        String create_time = confirm.toJSONObject().getJSONObject("response").getString("create_time");
                        if (status.equalsIgnoreCase("approved")) {
                            //AppMsg.makeText(PayNow.this,"Payment successful!", AppMsg.STYLE_INFO).show();
                            status = "approved";
                            paypalPaymentStatusUpdate(paypalOrderNo, status, tid, create_time);
                            return;
                        } else {
                            //AppMsg.makeText(PayNow.this,"Payment "+status, AppMsg.STYLE_ALERT).show();
                            String status_ = "fail";
                            paypalPaymentStatusUpdate(paypalOrderNo, status_, "0", "");
                        }
                    } catch (JSONException e) {
                        Log.e("tag", "an extremely unlikely failure occurred: ", e);
                        String status = "fail";
                        paypalPaymentStatusUpdate(paypalOrderNo, status, "0", "");
                    }
                } else {
                    Log.e("tag", "confirm null");
                    String status = "fail";
                    paypalPaymentStatusUpdate(paypalOrderNo, status, "0", "");
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.e("", "The user canceled.");
                String status = "fail";
                paypalPaymentStatusUpdate(paypalOrderNo, status, "0", "");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.e("", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
                String status = "fail";
                paypalPaymentStatusUpdate(paypalOrderNo, status, "0", "");
            }
        }
    }

    private void paypalPaymentStatusUpdate(final String orderNo, String status, String tid, String createdTime) {
        showProgress(true);
        final String amount_ = amount.getText().toString();
        HashMap<String, String> hashMap = Constants.getSHA256();
        if (hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("order_number", "" + orderNo);
        hashMap.put("status", "" + status);
        hashMap.put("tid", "" + tid);
        hashMap.put("create_time ", "" + createdTime);
        hashMap.put("amount", "" + amount.getText().toString());
        hashMap.put("name", "" + fname.getText().toString());
        TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        String number = tm.getLine1Number();
        if(number != null)
            hashMap.put("phone", ""+number);

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.paypal_return, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", "TEST: "+response.toString());
                Intent intent = new Intent(PayNow.this, PaymentSuccess.class);
                intent.putExtra("orderNo", orderNo);
                intent.putExtra("amount", amount_);
                startActivity(intent);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        hideProgress(true);
                    }
                }, 500);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    @Override
    public void onDestroy() {
        // Stop service when done
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }
}
