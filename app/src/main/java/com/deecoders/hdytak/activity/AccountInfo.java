package com.deecoders.hdytak.activity;

import android.app.DatePickerDialog;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deecoders.hdytak.R;
import com.deecoders.hdytak.network.CustomRequest;
import com.deecoders.hdytak.network.VolleyLibrary;
import com.deecoders.hdytak.util.Constants;
import com.deecoders.hdytak.util.MyPref;
import com.deecoders.hdytak.view.MyEditText;
import com.deecoders.hdytak.view.MyTextView;
import com.devspark.appmsg.AppMsg;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AccountInfo extends BaseActivity {
    @BindView(R.id.fname)
    MyEditText fname;
    @BindView(R.id.lname)
    MyEditText lname;
    @BindView(R.id.birthday)
    TextView birthday;
    @BindView(R.id.mobile)
    MyEditText mobile;
    @BindView(R.id.email)
    MyEditText email;
    @BindView(R.id.bankName)
    MyEditText bankName;
    @BindView(R.id.branch)
    MyEditText branch;
    @BindView(R.id.accountName)
    MyEditText accountName;
    @BindView(R.id.iban)
    MyEditText iban;
    @BindView(R.id.iban2)
    MyEditText iban2;
    @BindView(R.id.password)
    MyEditText password;
    @BindView(R.id.main)
    RelativeLayout main;
    @BindView(R.id.submit)
    MyTextView submit;
    @BindView(R.id.country)
    BetterSpinner country;
    ArrayList<String> countryNames = new ArrayList<>();
    ArrayList<String> countryCodes = new ArrayList<>();
    ArrayList<String> countryIds = new ArrayList<>();
    String myCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        replaceContentLayout(R.layout.activity_account_info);

        ButterKnife.bind(this);

        ViewGroup main = (ViewGroup) findViewById(R.id.main);

        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            //code for portrait mode
            getWindow().setBackgroundDrawableResource(R.drawable.background);
        }
        else {
            //code for landscape mode
            getWindow().setBackgroundDrawableResource(R.drawable.landscape);
        }

        bankInfo();
        loadingCountries();

        birthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                DatePickerDialog dialog = new DatePickerDialog(AccountInfo.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        String myFormat = "dd/MM/yyyy"; //In which you need put here
                        SimpleDateFormat sdformat = new SimpleDateFormat(myFormat, Locale.US);
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, month);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        birthday.setText(sdformat.format(calendar.getTime()));
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                dialog.show();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!email.getText().toString().isEmpty() && !checkEmail(email.getText().toString())) {
                    AppMsg.makeText(AccountInfo.this, "Invalid email!", AppMsg.STYLE_ALERT).show();
                    return;
                }
                if(!countryNames.contains(country.getText().toString())) {
                    AppMsg.makeText(AccountInfo.this, "Invalid country!", AppMsg.STYLE_ALERT).show();
                    return;
                }
                updateInfo();
            }
        });
    }

    private void loadingCountries() {
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.countries, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("loadingCountries", response.toString());
                try {
                    if (countryNames.size() > 0) countryNames.clear();
                    if (countryCodes.size() > 0) countryCodes.clear();
                    countryIds.clear();

                    String status = response.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray countries = response.getJSONArray("countries");
                        for (int i = 0; i < countries.length(); i++) {
                            String countryId = countries.getJSONObject(i).getString("id");
                            String country = countries.getJSONObject(i).getString("title");
                            String code = countries.getJSONObject(i).getString("phonecode");
                            countryNames.add(country);
                            countryCodes.add(code);
                            countryIds.add(countryId);
                        }
                        //ArrayAdapter<String> adapter = new ArrayAdapter<>(AccountInfo.this, android.R.layout.simple_list_item_1, countryNames);
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(AccountInfo.this, android.R.layout.simple_spinner_dropdown_item, countryNames);
                        country.setAdapter(adapter);
                        if(myCountry != null) {
                            /*int index = countryNames.indexOf(myCountry);
                            Log.e("tag", "index: "+index);
                            Log.e("tag", "size: "+countryNames.size());*/
                            country.setText(myCountry);
                        }
                        String locale;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            locale = getResources().getConfiguration().getLocales().get(0).getDisplayCountry();
                        } else {
                            locale = getResources().getConfiguration().locale.getDisplayCountry();
                        }
                        Log.e("tag", "local Country: "+locale);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("loadingCountries", error.toString());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (country != null)
                            loadingCountries();
                    }
                }, 1000);
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueueWithoutInternetCheck(customRequest, "", false);
    }

    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile("[a-zA-Z0-9+._%-+]{1,256}" +
            "@" +
            "[a-zA-Z0-9][a-zA-Z0-9-]{0,64}" +
            "(" +
            "." +
            "[a-zA-Z0-9][a-zA-Z0-9-]{0,25}" +
            ")+");

    public static boolean checkEmail(String email){
        if(email.isEmpty())
            return false;
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    private void bankInfo() {
        showProgress(true);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();;
        hashMap.put("member_id", ""+ MyPref.getId(this));

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.user_account, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(true);
                try {
                    JSONObject data = response.getJSONObject("user_account");
                    fname.setText(data.getString("first_name"));
                    lname.setText(data.getString("last_name"));
                    birthday.setText(data.getString("birthday"));
                    mobile.setText(data.getString("mobile"));
                    email.setText(data.getString("email"));
                    bankName.setText(data.getString("bank_name"));
                    branch.setText(data.getString("bank_branch"));
                    accountName.setText(data.getString("bank_account_name"));
                    iban.setText(data.getString("bank_iban_usd"));
                    iban2.setText(data.getString("bank_iban_local"));
                    myCountry = response.getJSONObject("country").getString("title");
                    Log.e("tag", "country: "+myCountry);
                    if(countryNames.size() != 0) {
                        //int index = countryNames.indexOf(myCountry);
                        country.setText(myCountry);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    private void updateInfo() {
        showProgress(false);
        final String name_ = fname.getText().toString()+" "+lname.getText().toString();
        final String email_ = email.getText().toString();
        final String country_ = country.getText().toString();
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("member_id", ""+ MyPref.getId(this));
        hashMap.put("first_name", ""+fname.getText().toString());
        hashMap.put("last_name", ""+lname.getText().toString());
        hashMap.put("email", ""+email.getText().toString());
        hashMap.put("password", ""+password.getText().toString());
        hashMap.put("mobile", ""+mobile.getText().toString());
        int index = countryNames.indexOf(country.getText().toString());
        hashMap.put("country", ""+countryIds.get(index));
        hashMap.put("birthday", ""+birthday.getText().toString());
        hashMap.put("bank_name", ""+bankName.getText().toString());
        hashMap.put("bank_branch", ""+branch.getText().toString());
        hashMap.put("bank_account_name", ""+accountName.getText().toString());
        hashMap.put("bank_iban_usd", ""+iban.getText().toString());
        hashMap.put("bank_iban_local", ""+iban2.getText().toString());

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.update_account, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(false);
                try {
                    String status = response.getString("status");
                    String msg = response.getString("message");
                    if (status.equals("success")) {
                        MyPref.setUsername(AccountInfo.this, name_);
                        MyPref.setEmail(AccountInfo.this, email_);
                        MyPref.setCountry(getApplicationContext(), ""+country_);
                        Toast.makeText(AccountInfo.this, ""+msg, Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else{
                        AppMsg.makeText(AccountInfo.this, ""+msg, AppMsg.STYLE_ALERT).show();
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    public void finish(View view) {
        finish();
    }

    public void openMenu(View view) {
        super.drawerToggle();
    }
}
