package com.deecoders.hdytak.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.deecoders.hdytak.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WebPayment extends AppCompatActivity {
    WebView webview;
    @BindView(R.id.container)
    FrameLayout container;
    String orderNo, amount;
    LottieAnimationView animationview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_payment);

        ButterKnife.bind(this);

        this.animationview = findViewById(R.id.animation_view);

        String content = getIntent().getStringExtra("content");
        orderNo = getIntent().getStringExtra("orderNo");
        amount = getIntent().getStringExtra("amount");

        webview = new WebView(this) {
            @Override
            public void postUrl(String url, byte[] postData) {
                System.out.println("postUrl can modified here:" + url);
                super.postUrl(url, postData);
            }
        };
        container.addView(webview);
        webview.loadData(content, "text/html", "UTF-8");
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setSupportZoom(true);
        webview.setWebViewClient(new HelloWebViewClient());
    }

    private class HelloWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Log.e("tag", "loading: " + url);
            animationview.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Log.e("tag", "finished: " + url);
            animationview.setVisibility(View.GONE);
            if(!url.contains("html") && url.contains("payment_return")){
                //Log.e("tag", "contains: " + url);
                Intent intent = new Intent(WebPayment.this, PaymentSuccess.class);
                intent.putExtra("orderNo", orderNo);
                intent.putExtra("amount", amount);
                startActivity(intent);
                finish();
            }
        }
    }

    public void finish(View view) {
        finish();
    }
}
