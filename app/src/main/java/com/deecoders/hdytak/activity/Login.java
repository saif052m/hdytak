package com.deecoders.hdytak.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deecoders.hdytak.R;
import com.deecoders.hdytak.network.CustomRequest;
import com.deecoders.hdytak.network.VolleyLibrary;
import com.deecoders.hdytak.util.Constants;
import com.deecoders.hdytak.util.MyPref;
import com.deecoders.hdytak.view.MyEditText;
import com.deecoders.hdytak.view.MyTextView;
import com.devspark.appmsg.AppMsg;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Login extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener{
    @BindView(R.id.email)
    MyEditText email;
    @BindView(R.id.password)
    MyEditText password;
    @BindView(R.id.submit)
    MyTextView submit;
    @BindView(R.id.forgotpassword)
    MyTextView forgotpassword;
    @BindView(R.id.signup)
    MyTextView signup;
    @BindView(R.id.facebookLogin)
    LoginButton facebookLogin;
    @BindView(R.id.googleSign)
    RelativeLayout googleSign;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.main)
    RelativeLayout main;
    CallbackManager callbackManager;
    GoogleApiClient googleApiClient;
    String email_ = "", fname_, lname_;
    String firebaseToken;
    Handler handler;
    Runnable runnable;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(handler != null){
            handler.removeCallbacks(runnable);
            handler = null;
            runnable = null;
        }
    }

    public void getToken() {
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                String token = FirebaseInstanceId.getInstance().getToken();
                if(token == null){
                    handler.postDelayed(runnable, 1000);
                    return;
                }
                if(token.equals("")){
                    handler.postDelayed(runnable, 1000);
                    return;
                }
                //loginNow(token);
            }
        };
        handler.postDelayed(runnable, 1000);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FirebaseApp.initializeApp(this);
        super.onCreate(savedInstanceState);
        replaceContentLayout(R.layout.activity_login);

        ButterKnife.bind(this);

        firebaseToken = FirebaseInstanceId.getInstance().getToken();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_PHONE_STATE}, 11);
        }

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        callbackManager = CallbackManager.Factory.create();
        facebookLogin.setReadPermissions(Arrays.asList("public_profile", "email"));
        facebookLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                Log.e("tag", "success");
                showProgress(false);
                Profile profile = Profile.getCurrentProfile();
                if (profile != null) {
                    fname_ = profile.getFirstName();
                    lname_ = profile.getLastName();
                } else {
                    ProfileTracker profileTracker = new ProfileTracker() {
                        @Override
                        protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                            Log.e("tag", "onCurrentProfileChanged");
                            this.stopTracking();
                            Profile.setCurrentProfile(currentProfile);
                            Profile profile = Profile.getCurrentProfile();
                            if (profile != null) {
                                fname_ = profile.getFirstName();
                                lname_ = profile.getLastName();
                            }
                        }
                    };
                    profileTracker.startTracking();
                }
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.e("tag", "response: " + object.toString());
                                hideProgress(false);
                                try {
                                    email_ = object.getString("email");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    LoginManager.getInstance().logOut();
                                }
                                socialLogin();
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                //startActivity(new Intent(Registration.this, Registration.class));
            }

            @Override
            public void onError(FacebookException error) {

            }
        });

        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions).build();

        ViewGroup main = (ViewGroup) findViewById(R.id.main);

        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            //code for portrait mode
            getWindow().setBackgroundDrawableResource(R.drawable.background);
        } else {
            //code for landscape mode
            getWindow().setBackgroundDrawableResource(R.drawable.landscape);
        }
    }

    private void socialLogin() {
        showProgress(false);
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                firebaseToken = FirebaseInstanceId.getInstance().getToken();
                if(firebaseToken == null){
                    handler.postDelayed(runnable, 1000);
                    return;
                }
                if(firebaseToken.equals("")){
                    handler.postDelayed(runnable, 1000);
                    return;
                }

                // login now
                HashMap<String, String> hashMap = Constants.getSHA256();
                if(hashMap == null)
                    hashMap = new HashMap<>();
                if(email_ == null)
                    email_ = "";
                hashMap.put("email", ""+email_);
                final TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
                if (ActivityCompat.checkSelfPermission(Login.this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                    hashMap.put("device_id", "" + tm.getDeviceId());
                }
                hashMap.put("device_token", ""+firebaseToken);
                hashMap.put("device_type", "android");

                System.out.println(Arrays.asList(hashMap));

                CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.social_sigin, hashMap, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("loginUser", response.toString());
                        hideProgress(false);
                        try {
                            String status = response.getString("status");
                            String msg = response.getString("message");
                            if (status.equalsIgnoreCase("success")) {
                                String uid = response.getString("memberid");
                                MyPref.setLogin(Login.this, 1);
                                MyPref.setId(Login.this, uid);
                                String cid = response.getString("campaign_id");
                                MyPref.setCampId(Login.this, cid);
                                startActivity(new Intent(Login.this, CreateCampaign.class));
                                finishAffinity();
                            }
                            else{
                                Toast.makeText(Login.this, ""+msg, Toast.LENGTH_SHORT).show();
                                LoginManager.getInstance().logOut();
                                Intent intent = new Intent(Login.this, Signup.class);
                                intent.putExtra("email", email_);
                                intent.putExtra("fname", fname_);
                                intent.putExtra("lname", lname_);
                                startActivity(intent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            showFailedAlert();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress(false);
                        showFailedAlert();
                        Log.e("loginUser", error.toString());
                    }
                });
                VolleyLibrary.getInstance(Login.this).addToRequestQueue(customRequest, "", false);
            }
        };
        handler.postDelayed(runnable, 10);
    }

    public void forgotPassword(View view) {
        startActivity(new Intent(this, KotlinTest.class));
    }

    public void signup(View view) {
        startActivity(new Intent(this, Signup.class));
    }

    public void loginUser(View view) {
        if(email.getText().toString().isEmpty()){
            AppMsg.makeText(this, "Please enter email!", AppMsg.STYLE_ALERT).show();
        }
        else if(password.getText().toString().isEmpty()){
            AppMsg.makeText(this, "Please enter password!", AppMsg.STYLE_ALERT).show();
        }
        else {
            loginUser();
        }
    }

    private void loginUser() {
        showProgress(false);
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                firebaseToken = FirebaseInstanceId.getInstance().getToken();
                if (firebaseToken == null) {
                    handler.postDelayed(runnable, 1000);
                    return;
                }
                if (firebaseToken.equals("")) {
                    handler.postDelayed(runnable, 1000);
                    return;
                }
                // login now
                HashMap<String, String> hashMap = Constants.getSHA256();
                if(hashMap == null)
                    hashMap = new HashMap<>();
                hashMap.put("email", email.getText().toString());
                hashMap.put("password", password.getText().toString());
                final TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
                if (ActivityCompat.checkSelfPermission(Login.this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                    hashMap.put("device_id", "" + tm.getDeviceId());
                }
                hashMap.put("device_token", ""+firebaseToken);
                hashMap.put("device_type", "android");

                System.out.println(Arrays.asList(hashMap));

                CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.signin, hashMap, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("loginUser", response.toString());
                        hideProgress(false);
                        try {
                            String status = response.getString("status");
                            String msg = response.getString("message");
                            if (status.equalsIgnoreCase("success")) {
                                String uid = response.getString("memberid");
                                String cid = response.getString("campaign_id");
                                MyPref.setLogin(Login.this, 1);
                                MyPref.setId(Login.this, uid);
                                MyPref.setCampId(Login.this, cid);
                                startActivity(new Intent(Login.this, CreateCampaign.class));
                                finishAffinity();
                            }
                            else{
                                AppMsg.makeText(Login.this, ""+msg, AppMsg.STYLE_ALERT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            showFailedAlert();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress(false);
                        showFailedAlert();
                        Log.e("loginUser", error.toString());
                    }
                });
                VolleyLibrary.getInstance(Login.this).addToRequestQueue(customRequest, "", false);
            }
        };
        handler.postDelayed(runnable, 10);
    }

    public void finish(View view) {
        finish();
    }

    public void openMenu(View view) {
        super.drawerToggle();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("tag", "onConnectionFailed");
    }

    public void googleLogin(View view) {
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(intent, 100);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleResult(result);
        }
        else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            Log.e("tag", "success");
            GoogleSignInAccount account = result.getSignInAccount();
            email_ = account.getEmail();
            fname_ = account.getDisplayName();
            socialLogin();
        }
    }
}
