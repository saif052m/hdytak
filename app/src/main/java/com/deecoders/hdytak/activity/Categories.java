package com.deecoders.hdytak.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ScrollView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deecoders.hdytak.R;
import com.deecoders.hdytak.adapter.CategoryAdapter;
import com.deecoders.hdytak.model.CategoryModel;
import com.deecoders.hdytak.model.ProductModel;
import com.deecoders.hdytak.network.CustomRequest;
import com.deecoders.hdytak.network.VolleyLibrary;
import com.deecoders.hdytak.util.Constants;
import com.deecoders.hdytak.view.ExpandableHeightGridView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Categories extends BaseActivity {
    ExpandableHeightGridView listView;
    CategoryAdapter categoryAdapter;
    ArrayList<CategoryModel> categoryModels = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.replaceContentLayout(R.layout.activity_categories);

        ViewGroup main = (ViewGroup)findViewById(R.id.main);

        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            //code for portrait mode
            getWindow().setBackgroundDrawableResource(R.drawable.background);
        } else {
            //code for landscape mode
            getWindow().setBackgroundDrawableResource(R.drawable.landscape);
        }

        listView = (ExpandableHeightGridView)findViewById(R.id.listView);

        final ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView);
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                Log.e("Tag", "scroll");
                scrollView.scrollTo(0,0);
            }
        });

        getCategories();
    }

    public void finish(View view) {
        finish();
    }

    public void openMenu(View view) {
        super.drawerToggle();
    }

    private void getCategories() {
        showProgress(true);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.product_categories, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(true);
                try {
                    String status = response.getString("status");
                    String msg = response.getString("message");
                    if (status.equalsIgnoreCase("success")) {
                        categoryModels.clear();
                        JSONArray categories = response.getJSONArray("product_categories");
                        for (int i = 0; i < categories.length(); i++) {
                            JSONObject object = categories.getJSONObject(i);
                            CategoryModel categoryModel = new CategoryModel();
                            categoryModel.setId(object.getString("id"));
                            categoryModel.setDate(object.getString("created_date"));
                            categoryModel.setUrlTitle(object.getString("url_title"));
                            categoryModel.setTitle(object.getString("title"));
                            categoryModel.setActive(object.getString("active"));
                            categoryModels.add(categoryModel);
                        }

                        categoryAdapter = new CategoryAdapter(Categories.this, R.layout.category_group, categoryModels);
                        // setting list adapter
                        listView.setAdapter(categoryAdapter);
                        listView.setExpanded(true);
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                                Intent intent = new Intent(Categories.this, SubCategories.class);
                                intent.putExtra("cat_id", categoryModels.get(pos).getId());
                                intent.putExtra("cat_name", categoryModels.get(pos).getTitle());
                                startActivity(intent);
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }
}
