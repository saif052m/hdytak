package com.deecoders.hdytak.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deecoders.hdytak.R;
import com.deecoders.hdytak.adapter.ProductAdapter;
import com.deecoders.hdytak.model.ProductModel;
import com.deecoders.hdytak.network.CustomRequest;
import com.deecoders.hdytak.network.VolleyLibrary;
import com.deecoders.hdytak.util.Constants;
import com.deecoders.hdytak.view.ExpandableHeightGridView;
import com.deecoders.hdytak.view.MyTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Products extends BaseActivity {
    ProductAdapter productAdapter;
    ArrayList<ProductModel> productModels = new ArrayList<>();
    @BindView(R.id.title1)
    MyTextView title1;
    @BindView(R.id.title2)
    MyTextView title2;
    @BindView(R.id.gridView)
    ExpandableHeightGridView gridView;
    @BindView(R.id.btn)
    MyTextView btn;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.main)
    RelativeLayout main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.replaceContentLayout(R.layout.activity_products);

        ButterKnife.bind(this);

        ViewGroup main = (ViewGroup) findViewById(R.id.main);

        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            //code for portrait mode
            getWindow().setBackgroundDrawableResource(R.drawable.background);
        } else {
            //code for landscape mode
            getWindow().setBackgroundDrawableResource(R.drawable.landscape);
        }

        String subcatId = getIntent().getStringExtra("subcat_id");
        String subcatName = getIntent().getStringExtra("subcat_name");
        String catName = getIntent().getStringExtra("cat_name");

        //title1.setText("PRODUCTS - "+catName+" - ");
        String [] arr = subcatName.split(" ");
        if(arr.length > 3){
            subcatName = arr[0]+" "+arr[1]+" "+arr[2]+"...";
        }
        title2.setText(subcatName);

        products(subcatId);
    }

    private void products(String subcatId) {
        showProgress(true);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("sub_category_id", ""+subcatId);

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.products, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(true);
                try {
                    String status = response.getString("status");
                    if(status.equals("success")){
                        productModels.clear();
                        JSONArray products = response.getJSONArray("products");
                        for(int i=0; i<products.length(); i++){
                            //id, created_date, title, salling_price, description, image;
                            ProductModel model = new ProductModel();
                            model.setId(products.getJSONObject(i).getString("id"));
                            model.setCreated_date(products.getJSONObject(i).getString("created_date"));
                            model.setTitle(products.getJSONObject(i).getString("title"));
                            model.setSalling_price(products.getJSONObject(i).getString("price"));
                            model.setDescription(products.getJSONObject(i).getString("description"));
                            model.setImage(products.getJSONObject(i).getString("image"));
                            model.setCanAddToCamp(products.getJSONObject(i).getString("can_add_to_campaign"));
                            model.setAccessoriesRequired(products.getJSONObject(i).getString("accessories_required"));
                            model.setUrlTitle(products.getJSONObject(i).getString("url_title"));
                            model.setMinCharge(products.getJSONObject(i).getString("min_charge"));
                            model.setMaxCharge(products.getJSONObject(i).getString("max_charge"));
                            model.setType(0);
                            productModels.add(model);
                        }

                        if(productModels.size()==1)
                            gridView.setNumColumns(1);
                        else
                            gridView.setNumColumns(2);

                        productAdapter = new ProductAdapter(Products.this, R.layout.product_item, productModels);
                        gridView.setAdapter(productAdapter);
                        gridView.setExpanded(true);

                        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                                Intent intent = new Intent(Products.this, ProductDetails.class);
                                intent.putExtra("cat_name", getIntent().getStringExtra("cat_name"));
                                intent.putExtra("subcat_name", getIntent().getStringExtra("subcat_name"));
                                intent.putExtra("product_model", productModels.get(pos));
                                startActivity(intent);
                            }
                        });

                        final ScrollView scrollView = findViewById(R.id.scrollView);
                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {
                                scrollView.scrollTo(0, 0);
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }


    public void finish(View view) {
        finish();
    }

    public void openMenu(View view) {
        super.drawerToggle();
    }

    public void openCampaign(View view) {
        startActivity(new Intent(this, MyCampaign.class));
    }
}
