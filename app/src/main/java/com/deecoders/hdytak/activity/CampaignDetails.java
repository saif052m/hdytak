package com.deecoders.hdytak.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deecoders.hdytak.R;
import com.deecoders.hdytak.adapter.CampaignDetailsAdapter;
import com.deecoders.hdytak.model.CampaignModel;
import com.deecoders.hdytak.network.CustomRequest;
import com.deecoders.hdytak.network.VolleyLibrary;
import com.deecoders.hdytak.util.Constants;
import com.deecoders.hdytak.view.ExpandableHeightGridView;
import com.deecoders.hdytak.view.MyTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CampaignDetails extends BaseActivity {
    ExpandableHeightGridView gridView;
    CampaignDetailsAdapter campaignDetailsAdapter;
    ArrayList<CampaignModel> campaignModels = new ArrayList<>();
    @BindView(R.id.title1)
    MyTextView title1;
    @BindView(R.id.title2)
    MyTextView title2;
    @BindView(R.id.search)
    EditText search;
    @BindView(R.id.submit)
    ImageView submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.replaceContentLayout(R.layout.activity_campaign_details);

        ButterKnife.bind(this);

        ViewGroup main = (ViewGroup) findViewById(R.id.main);

        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            //code for portrait mode
            getWindow().setBackgroundDrawableResource(R.drawable.background);
        } else {
            //code for landscape mode
            getWindow().setBackgroundDrawableResource(R.drawable.landscape);
        }

        gridView = (ExpandableHeightGridView) findViewById(R.id.gridView);

        String subcatId = getIntent().getStringExtra("camp_subcat_id");
        String subcatName = getIntent().getStringExtra("camp_subcat_name");
        String catName = getIntent().getStringExtra("camp_cat_name");

        //title1.setText("CAMPAIGNS - " + catName + " - ");
        String [] arr = subcatName.split(" ");
        if(arr.length > 3){
            subcatName = arr[0]+" "+arr[1]+" "+arr[2]+"...";
        }
        Log.e("taggg", "space: "+arr.length);
        title2.setText(subcatName);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(search.getText().toString().isEmpty())
                    return;
                searchCampaigns(search.getText().toString());
            }
        });

        campaigns(subcatId);
    }

    private void campaigns(String subcatId) {
        showProgress(true);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("sub_category_id", "" + subcatId);

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.campaigns, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(true);
                try {
                    String status = response.getString("status");
                    if (status.equals("success")) {
                        campaignModels.clear();
                        JSONArray campaigns = response.getJSONArray("campaigns");
                        for (int i = 0; i < campaigns.length(); i++) {
                            //id, created_date, title, salling_price, description, image;
                            CampaignModel model = new CampaignModel();
                            model.setId(campaigns.getJSONObject(i).getString("id"));
                            model.setCreated_date(campaigns.getJSONObject(i).getString("created_date"));
                            model.setTitle(campaigns.getJSONObject(i).getString("title"));
                            model.setDescription(campaigns.getJSONObject(i).getString("description"));
                            model.setImage(campaigns.getJSONObject(i).getString("image"));
                            model.setUrlTitle(campaigns.getJSONObject(i).getString("url_title"));
                            model.setVisible(campaigns.getJSONObject(i).getString("visible"));
                            model.setOnePayment(campaigns.getJSONObject(i).getString("one_payment"));
                            model.setQrImage(campaigns.getJSONObject(i).getString("qr_image"));
                            campaignModels.add(model);
                        }

                        if(campaignModels.size()==1)
                            gridView.setNumColumns(1);
                        else
                            gridView.setNumColumns(2);

                        campaignDetailsAdapter = new CampaignDetailsAdapter(CampaignDetails.this, R.layout.product_item, campaignModels);
                        gridView.setAdapter(campaignDetailsAdapter);
                        gridView.setExpanded(true);

                        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                                Intent intent = new Intent(CampaignDetails.this, PayNow.class);
                                intent.putExtra("camp_model", campaignModels.get(pos));
                                startActivity(intent);
                            }
                        });

                        final ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView);
                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {
                                //scrollView.scrollTo(0, 0);
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    private void searchCampaigns(String keyword) {
        showProgress(false);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("keyword", "" + keyword);

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.campaign_search, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(false);
                try {
                    campaignModels.clear();
                    JSONArray campaigns = response.getJSONArray("campaign_search");
                    for (int i = 0; i < campaigns.length(); i++) {
                        //id, created_date, title, salling_price, description, image;
                        CampaignModel model = new CampaignModel();
                        model.setId(campaigns.getJSONObject(i).getString("id"));
                        model.setCreated_date(campaigns.getJSONObject(i).getString("created_date"));
                        model.setTitle(campaigns.getJSONObject(i).getString("title"));
                        model.setDescription(campaigns.getJSONObject(i).getString("description"));
                        model.setImage(campaigns.getJSONObject(i).getString("image"));
                        model.setUrlTitle(campaigns.getJSONObject(i).getString("url_title"));
                        model.setVisible(campaigns.getJSONObject(i).getString("visible"));
                        model.setOnePayment(campaigns.getJSONObject(i).getString("one_payment"));
                        model.setQrImage(campaigns.getJSONObject(i).getString("qr_image"));
                        campaignModels.add(model);
                    }

                    if(campaignModels.size()==1)
                        gridView.setNumColumns(1);
                    else
                        gridView.setNumColumns(2);

                    if (campaignDetailsAdapter != null) {
                        campaignDetailsAdapter.setModels(campaignModels);
                    } else {
                        campaignDetailsAdapter = new CampaignDetailsAdapter(CampaignDetails.this, R.layout.product_item, campaignModels);
                        gridView.setAdapter(campaignDetailsAdapter);
                        gridView.setExpanded(true);
                    }

                    gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                            Intent intent = new Intent(CampaignDetails.this, PayNow.class);
                            intent.putExtra("camp_model", campaignModels.get(pos));
                            startActivity(intent);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    public void finish(View view) {
        finish();
    }

    public void openMenu(View view) {
        super.drawerToggle();
    }

    public void openCampaign(View view) {
        startActivity(new Intent(this, MyCampaign.class));
    }
}
