package com.deecoders.hdytak.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deecoders.hdytak.R;
import com.deecoders.hdytak.adapter.CampaignDetailsAdapter;
import com.deecoders.hdytak.adapter.CategoryAdapter;
import com.deecoders.hdytak.model.CampaignModel;
import com.deecoders.hdytak.model.CategoryModel;
import com.deecoders.hdytak.network.CustomRequest;
import com.deecoders.hdytak.network.VolleyLibrary;
import com.deecoders.hdytak.util.Constants;
import com.deecoders.hdytak.view.ExpandableHeightGridView;
import com.deecoders.hdytak.view.MyTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Campaigns extends BaseActivity {
    ExpandableHeightGridView listView;
    ExpandableHeightGridView gridView;
    CampaignDetailsAdapter campaignDetailsAdapter;
    ArrayList<CampaignModel> campaignModels = new ArrayList<>();
    CategoryAdapter categoryAdapter;
    ArrayList<CategoryModel> categoryModels = new ArrayList<>();
    @BindView(R.id.search)
    EditText search;
    @BindView(R.id.submit)
    ImageView submit;
    @BindView(R.id.noResult)
    TextView noResult;
    @BindView(R.id.main)
    RelativeLayout main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        replaceContentLayout(R.layout.activity_campaigns);

        ButterKnife.bind(this);

        ViewGroup main = (ViewGroup) findViewById(R.id.main);

        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            //code for portrait mode
            getWindow().setBackgroundDrawableResource(R.drawable.background);
        } else {
            //code for landscape mode
            getWindow().setBackgroundDrawableResource(R.drawable.landscape);
        }

        listView = findViewById(R.id.listView);
        gridView = findViewById(R.id.campaignList);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (search.getText().toString().isEmpty())
                    return;
                searchCampaigns(search.getText().toString());
            }
        });

        getCategories();
    }

    private void searchCampaigns(String keyword) {
        showProgress(false);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if (hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("keyword", "" + keyword);

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.campaign_search, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(false);
                listView.setVisibility(View.GONE);
                gridView.setVisibility(View.VISIBLE);
                try {
                    campaignModels.clear();
                    JSONArray campaigns = response.getJSONArray("campaign_search");
                    for (int i = 0; i < campaigns.length(); i++) {
                        //id, created_date, title, salling_price, description, image;
                        CampaignModel model = new CampaignModel();
                        model.setId(campaigns.getJSONObject(i).getString("id"));
                        model.setCreated_date(campaigns.getJSONObject(i).getString("created_date"));
                        model.setTitle(campaigns.getJSONObject(i).getString("title"));
                        model.setDescription(campaigns.getJSONObject(i).getString("description"));
                        model.setImage(campaigns.getJSONObject(i).getString("image"));
                        model.setUrlTitle(campaigns.getJSONObject(i).getString("url_title"));
                        model.setVisible(campaigns.getJSONObject(i).getString("visible"));
                        model.setOnePayment(campaigns.getJSONObject(i).getString("one_payment"));
                        model.setQrImage(campaigns.getJSONObject(i).getString("qr_image"));
                        campaignModels.add(model);
                    }

                    if (campaignModels.size() == 1)
                        gridView.setNumColumns(1);
                    else
                        gridView.setNumColumns(2);

                    if (campaignDetailsAdapter != null) {
                        campaignDetailsAdapter.setModels(campaignModels);
                    } else {
                        campaignDetailsAdapter = new CampaignDetailsAdapter(Campaigns.this, R.layout.product_item, campaignModels);
                        gridView.setAdapter(campaignDetailsAdapter);
                        gridView.setExpanded(true);
                    }

                    if(campaignModels.size() == 0)
                        noResult.setVisibility(View.VISIBLE);
                    else
                        noResult.setVisibility(View.GONE);

                    gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                            Intent intent = new Intent(Campaigns.this, PayNow.class);
                            intent.putExtra("camp_model", campaignModels.get(pos));
                            startActivity(intent);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    private void getCategories() {
        showProgress(true);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if (hashMap == null)
            hashMap = new HashMap<>();

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.campaign_categories, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(true);
                try {
                    String status = response.getString("status");
                    String msg = response.getString("message");
                    if (status.equalsIgnoreCase("success")) {
                        categoryModels.clear();
                        JSONArray categories = response.getJSONArray("campaign_categories");
                        for (int i = 0; i < categories.length(); i++) {
                            JSONObject object = categories.getJSONObject(i);
                            CategoryModel categoryModel = new CategoryModel();
                            categoryModel.setId(object.getString("id"));
                            categoryModel.setDate(object.getString("created_date"));
                            categoryModel.setUrlTitle(object.getString("url_title"));
                            categoryModel.setTitle(object.getString("title"));
                            categoryModel.setActive(object.getString("active"));
                            categoryModels.add(categoryModel);
                        }

                        categoryAdapter = new CategoryAdapter(Campaigns.this, R.layout.category_group, categoryModels);
                        // setting list adapter
                        listView.setAdapter(categoryAdapter);
                        listView.setExpanded(true);
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                                Intent intent = new Intent(Campaigns.this, SubCategories.class);
                                intent.putExtra("camp_cat_id", categoryModels.get(pos).getId());
                                intent.putExtra("camp_cat_name", categoryModels.get(pos).getTitle());
                                startActivity(intent);
                            }
                        });

                        final ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView);
                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {
                                scrollView.scrollTo(0, 0);
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    public void finish(View view) {
        finish();
    }

    public void openMenu(View view) {
        super.drawerToggle();
    }

    public void allCampaigns(View view) {
        Intent intent = new Intent(this, AllCampaigns.class);
        startActivity(intent);
    }
}
