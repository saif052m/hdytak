package com.deecoders.hdytak.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.deecoders.hdytak.R;
import com.deecoders.hdytak.adapter.GalleryAdapter;
import com.deecoders.hdytak.model.ProductModel;
import com.deecoders.hdytak.network.CustomRequest;
import com.deecoders.hdytak.network.LruBitmapCache;
import com.deecoders.hdytak.network.VolleyLibrary;
import com.deecoders.hdytak.util.Constants;
import com.deecoders.hdytak.util.MyPref;
import com.deecoders.hdytak.view.MyEditText;
import com.deecoders.hdytak.view.MyTextView;
import com.devspark.appmsg.AppMsg;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductDetails extends BaseActivity {
    @BindView(R.id.title1)
    MyTextView title1;
    @BindView(R.id.title2)
    MyTextView title2;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;
    @BindView(R.id.name)
    MyTextView name;
    @BindView(R.id.sold)
    MyTextView sold;
    @BindView(R.id.price)
    MyTextView price;
    @BindView(R.id.left)
    MyTextView left;
    @BindView(R.id.spinner)
    TextView spinner;
    @BindView(R.id.value)
    MyEditText value;
    @BindView(R.id.rname)
    MyEditText rname;
    @BindView(R.id.amount)
    MyEditText amount;
    @BindView(R.id.addToCamp)
    MyTextView addToCamp;
    @BindView(R.id.desc)
    MyTextView desc;
    @BindView(R.id.rating1)
    RatingBar rating1;
    @BindView(R.id.rating2)
    RatingBar rating2;
    @BindView(R.id.rating3)
    RatingBar rating3;
    @BindView(R.id.reviews)
    LinearLayout reviews;
    @BindView(R.id.share)
    MyTextView share;
    @BindView(R.id.rtitle)
    MyTextView rtitle;
    @BindView(R.id.main)
    RelativeLayout main;
    ProductModel model;
    RequestQueue mRequestQueue;
    ImageLoader mImageLoader;
    ArrayList<String> optionIds = new ArrayList<>();
    ArrayList<String> optionValues = new ArrayList<>();
    int selectedOption = -1;
    GalleryAdapter galleryAdapter;
    ArrayList<String> galleryUrls = new ArrayList<>();
    @BindView(R.id.spinnerBox)
    RelativeLayout spinnerBox;
    @BindView(R.id.valueTxt)
    MyTextView valueTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        replaceContentLayout(R.layout.activity_product_details);

        ButterKnife.bind(this);

        mRequestQueue = Volley.newRequestQueue(this);
        mImageLoader = new ImageLoader(mRequestQueue, new LruBitmapCache());

        model = (ProductModel) getIntent().getSerializableExtra("product_model");

        String subcatName = getIntent().getStringExtra("subcat_name");
        String catName = getIntent().getStringExtra("cat_name");

        //title1.setText("PRODUCTS - " + catName + " - ");
        if(subcatName != null) {
            String[] arr = subcatName.split(" ");
            if (arr.length > 3) {
                subcatName = arr[0] + " " + arr[1] + " " + arr[2] + "...";
            }
            Log.e("taggg", "space: " + arr.length);
        }
        title2.setText(""+subcatName);

        if(model == null){
            AppMsg.makeText(this, "Unable to load product data!", AppMsg.STYLE_ALERT).show();
            return;
        }

        //image.setImageUrl(Constants.originalProductImageUrl+model.getImage(), mImageLoader);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        galleryUrls.add(model.getImage());
        galleryAdapter = new GalleryAdapter(this, galleryUrls);
        recyclerView.setAdapter(galleryAdapter);

        name.setText(model.getTitle());
        if(model.getSalling_price().equals("0")){
            price.setVisibility(View.GONE);
        }
        else {
            price.setText(Constants.getFormatedAmount(model.getSalling_price()) + "$");
        }

        if(model.getDescription().isEmpty()){
            desc.setText("No description available");
            showReviews(null);
        }
        else {
            desc.setText(fromHtml(model.getDescription()));
        }

        if (model.getMinCharge().equals("0") || model.getMinCharge().equals("")) {
            rtitle.setVisibility(View.GONE);
            rname.setVisibility(View.GONE);
            valueTxt.setVisibility(View.GONE);
            value.setVisibility(View.GONE);
        }

        main.setVisibility(View.GONE);

        getGallery(model.getId());
        getRating(model.getId());
        getAccessories(model.getId());
    }

    private void getGallery(String pid) {
        showProgress(true);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("product_id", "" + pid);

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.product_gallery, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                main.setVisibility(View.VISIBLE);
                hideProgress(true);
                try {
                    String status = response.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray gallery = response.getJSONArray("product_gallery");
                        for (int i = 0; i < gallery.length(); i++) {
                            if (galleryUrls.contains(gallery.getJSONObject(i).getString("image")))
                                continue;
                            galleryUrls.add(gallery.getJSONObject(i).getString("image"));
                        }
                        galleryAdapter.setData(galleryUrls);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    private void getRating(String pid) {
        showProgress(false);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("product_id", "" + pid);

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.product_rating, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                main.setVisibility(View.VISIBLE);
                hideProgress(false);
                try {
                    String status = response.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        String quality = response.getJSONObject("product_rate").getString("quality");
                        String price = response.getJSONObject("product_rate").getString("price");
                        String value = response.getJSONObject("product_rate").getString("value");
                        rating1.setRating(Float.parseFloat(quality));
                        rating2.setRating(Float.parseFloat(price));
                        rating3.setRating(Float.parseFloat(value));
                        ratingBar.setRating((Float.parseFloat(quality) + Float.parseFloat(price) + Float.parseFloat(value)) / 3);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    private void getAccessories(String pid) {
        showProgress(false);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("product_id", "" + pid);

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.product_accessories, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                main.setVisibility(View.VISIBLE);
                hideProgress(false);
                try {
                    String status = response.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        optionIds.clear();
                        optionValues.clear();
                        JSONArray options = response.getJSONArray("options");
                        for (int i = 0; i < options.length(); i++) {
                            optionIds.add(options.getJSONObject(i).getString("id"));
                            optionValues.add(options.getJSONObject(i).getString("title"));
                        }

                        if (optionIds.size() == 0) {
                            spinnerBox.setVisibility(View.GONE);
                        }

                        spinner.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showPopup();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    private void showPopup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose");
        String[] codes = new String[optionValues.size()];
        for (int i = 0; i < optionValues.size(); i++) {
            codes[i] = optionValues.get(i);
        }
        builder.setSingleChoiceItems(codes, selectedOption, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedOption = which;
                spinner.setText(optionValues.get(which));
                dialog.dismiss();
            }
        });
        builder.create();
        builder.show();
    }

    public static Spanned fromHtml(String source) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(source);
        }
    }

    public void addToCamp(View view) {
        if (!model.getMinCharge().equals("0") && !model.getMinCharge().equals("")) {
            double minCharge = Double.parseDouble(model.getMinCharge());
            double maxCharge = Double.parseDouble(model.getMaxCharge());
            String value_ = value.getText().toString();
            if (model.getCanAddToCamp().equals("0")) {
                AppMsg.makeText(this, "Not allowed to add to campaign!", AppMsg.STYLE_ALERT).show();
            } else if (model.getAccessoriesRequired().equals("1") && spinner.getText().toString().isEmpty()) {
                AppMsg.makeText(this, "Please select accessory!", AppMsg.STYLE_ALERT).show();
            } else if (value.getText().toString().isEmpty()) {
                AppMsg.makeText(this, "Please enter value!", AppMsg.STYLE_ALERT).show();
            } else if (rname.getText().toString().isEmpty()) {
                AppMsg.makeText(this, "Please enter recipient name!", AppMsg.STYLE_ALERT).show();
            } else if (amount.getText().toString().isEmpty() || amount.getText().toString().equals("0")) {
                AppMsg.makeText(this, "Please enter quantity!", AppMsg.STYLE_ALERT).show();
            } else {
                submitToServer();
            }
        } else {
            if (model.getCanAddToCamp().equals("0")) {
                AppMsg.makeText(this, "Not allowed to add to campaign!", AppMsg.STYLE_ALERT).show();
            } else if (model.getAccessoriesRequired().equals("1") && spinner.getText().toString().isEmpty()) {
                AppMsg.makeText(this, "Please select accessory!", AppMsg.STYLE_ALERT).show();
            } else if (amount.getText().toString().isEmpty() || amount.getText().toString().equals("0")) {
                AppMsg.makeText(this, "Please enter quantity!", AppMsg.STYLE_ALERT).show();
            } else {
                if(!spinner.getText().toString().isEmpty()){
                    submitToServer();
                }
                else{
                    submitToServer1();
                }
            }
        }
    }

    private void submitToServer() {
        showProgress(false);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("product_id", "" + model.getId());
        hashMap.put("member_id", MyPref.getId(this));
        hashMap.put("campaign_id", MyPref.getCampId(this));
        hashMap.put("recipient_name", rname.getText().toString());
        if (selectedOption == -1) {
            hashMap.put("accessories", "");
        } else {
            hashMap.put("accessories", "" + optionIds.get(selectedOption));
        }
        hashMap.put("charge_amount", value.getText().toString());
        hashMap.put("quantity", amount.getText().toString());

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.add_to_campaign_with_accessories, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(false);
                try {
                    String status = response.getString("status");
                    String message = response.getString("message");
                    if (status.equalsIgnoreCase("success")) {
                        AppMsg.makeText(ProductDetails.this, "" + message, AppMsg.STYLE_INFO).show();
                    } else {
                        AppMsg.makeText(ProductDetails.this, "" + message, AppMsg.STYLE_ALERT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    private void submitToServer1() {
        showProgress(false);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("product_id", "" + model.getId());
        hashMap.put("member_id", MyPref.getId(this));
        hashMap.put("campaign_id", MyPref.getCampId(this));
        hashMap.put("quantity", amount.getText().toString());

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.add_to_campaign, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(false);
                try {
                    String status = response.getString("status");
                    String message = response.getString("message");
                    if (status.equalsIgnoreCase("success")) {
                        AppMsg.makeText(ProductDetails.this, "" + message, AppMsg.STYLE_INFO).show();
                    } else {
                        AppMsg.makeText(ProductDetails.this, "" + message, AppMsg.STYLE_ALERT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    public void shareProduct(View view) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, model.getTitle());
        Log.e("Tag", "url: " + Constants.productShareUrl + model.getUrlTitle());
        i.putExtra(Intent.EXTRA_TEXT, Constants.productShareUrl + model.getUrlTitle());
        startActivity(Intent.createChooser(i, "Share Product"));
    }

    public void finish(View view) {
        finish();
    }

    public void openMenu(View view) {
        super.drawerToggle();
    }

    public void showDescription(View view) {
        desc.setVisibility(View.VISIBLE);
        reviews.setVisibility(View.GONE);
    }

    public void showReviews(View view) {
        desc.setVisibility(View.GONE);
        reviews.setVisibility(View.VISIBLE);
    }
}
