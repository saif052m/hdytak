package com.deecoders.hdytak.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deecoders.hdytak.R;
import com.deecoders.hdytak.model.CategoryModel;
import com.deecoders.hdytak.model.SubCategoryModel;
import com.deecoders.hdytak.network.CustomRequest;
import com.deecoders.hdytak.network.VolleyLibrary;
import com.deecoders.hdytak.util.Constants;
import com.deecoders.hdytak.util.GetFilePathFromDevice;
import com.deecoders.hdytak.util.MyPref;
import com.deecoders.hdytak.view.MyCheckBox;
import com.deecoders.hdytak.view.MyEditText;
import com.deecoders.hdytak.view.MyTextView;
import com.deecoders.hdytak.view.MyTextView1;
import com.devspark.appmsg.AppMsg;
import com.theartofdev.edmodo.cropper.CropImage;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditCampaign extends BaseActivity {
    @BindView(R.id.category)
    ViewGroup category;
    @BindView(R.id.subcategory)
    ViewGroup subcategory;
    @BindView(R.id.titleTxt)
    MyEditText titleTxt;
    @BindView(R.id.titleTxt2)
    MyTextView titleTxt2;
    @BindView(R.id.title)
    LinearLayout title;
    @BindView(R.id.descTxt)
    MyEditText descTxt;
    @BindView(R.id.descTxt2)
    TextView descTxt2;
    @BindView(R.id.desc)
    LinearLayout desc;
    @BindView(R.id.imageTxt)
    TextView imageTxt;
    @BindView(R.id.imageTxt2)
    TextView imageTxt2;
    @BindView(R.id.image)
    RelativeLayout image;
    @BindView(R.id.submit)
    MyTextView submit;
    @BindView(R.id.main)
    RelativeLayout main;
    @BindView(R.id.otherCategory)
    MyEditText otherCategory;
    @BindView(R.id.otherSubCategory)
    MyEditText otherSubCategory;
    @BindView(R.id.visible)
    MyCheckBox visible;
    @BindView(R.id.onePayment)
    MyCheckBox onePayment;
    @BindView(R.id.agree)
    MyCheckBox agree;

    @BindView(R.id.unlimited)
    MyCheckBox unlimited;
    @BindView(R.id.show_paid)
    MyCheckBox show_paid;

    @BindView(R.id.categoryTxt)
    MyTextView1 categoryTxt;
    @BindView(R.id.subcategoryTxt)
    MyTextView1 subcategoryTxt;
    @BindView(R.id.photo)
    ImageView photo;
    File cameraFile;
    String filePath;
    ArrayList<CategoryModel> categoryModels = new ArrayList<>();
    ArrayList<String> categoryModelsTxt = new ArrayList<>();
    ArrayList<SubCategoryModel> subcategoryModels = new ArrayList<>();
    ArrayList<String> subcategoryModelsTxt = new ArrayList<>();
    int selectedCat = -1, selectedSubcat = -1;
    String selectedCatId, selectedSubcatId;
    Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.replaceContentLayout(R.layout.activity_edit_campaign);

        ButterKnife.bind(this);

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 11);
        }
        else if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 12);
        }
        else if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA}, 13);
        }

        ViewGroup main = (ViewGroup) findViewById(R.id.main);

        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            //code for portrait mode
            getWindow().setBackgroundDrawableResource(R.drawable.background);
        } else {
            //code for landscape mode
            getWindow().setBackgroundDrawableResource(R.drawable.landscape);
        }

        String desc_ = getIntent().getStringExtra("desc");
        String image_ = getIntent().getStringExtra("image");
        String visible_ = getIntent().getStringExtra("visible");
        String payment_ = getIntent().getStringExtra("payment");
        String unlimited_ = getIntent().getStringExtra("unlimited");
        String show_paid_ = getIntent().getStringExtra("show_paid");
        selectedCatId = getIntent().getStringExtra("selected_cat");
        selectedSubcatId = getIntent().getStringExtra("selected_sub_cat");

        /*RequestQueue mRequestQueue = Volley.newRequestQueue(this);
        ImageLoader mImageLoader = new ImageLoader(mRequestQueue, new LruBitmapCache());
        photo.setImageUrl(Constants.campaignImageUrl+image_, mImageLoader);*/

        descTxt.setText(desc_);
        if(visible_.equals("0")){
            visible.setChecked(false);
        }
        else{
            visible.setChecked(true);
        }
        if(payment_.equals("0")){
            onePayment.setChecked(false);
        }
        else{
            onePayment.setChecked(true);
        }

        if(unlimited_.equals("0")){
            unlimited.setChecked(false);
        }
        else{
            unlimited.setChecked(true);
        }
        if(show_paid_.equals("0")){
            show_paid.setChecked(false);
        }
        else{
            show_paid.setChecked(true);
        }

        getCategories();
        getSubCategories(selectedCatId);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 11);
        }
        else if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 12);
        }
        else if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA}, 13);
        }
    }

    public void finish(View view) {
        finish();
    }

    public void editCampaign(View view) {
        if(descTxt.getText().toString().isEmpty()){
            AppMsg.makeText(this, "Please enter description!", AppMsg.STYLE_ALERT).show();
        }
        else{
            new UploadTask().execute();
        }

    }

    public void showPopup(View view) {
        final CharSequence options[] = new CharSequence[] {"Camera", "Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Source");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(options[which].equals("Camera")){
                    openCamera();
                }
                else{
                    openGallery();
                }
            }
        });
        builder.show();
    }

    public class UploadTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            String response = submitCampaign();
            return response;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(false);
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            Log.e("server response", ""+response);
            hideProgress(false);
            if(response != null) {
                try {
                    JSONObject object = new JSONObject(response);
                    String status = object.getString("status");
                    String msg = object.getString("message");
                    if (status.equalsIgnoreCase("success")) {
                        Toast.makeText(EditCampaign.this, "" + msg, Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else {
                        AppMsg.makeText(EditCampaign.this, "" + msg, AppMsg.STYLE_ALERT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
            else{
                showFailedAlert();
            }
        }
    }

    public static Bitmap getResizedBitmap(Bitmap image, int newHeight, int newWidth) {
        int width = image.getWidth();
        int height = image.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // create a matrix for the manipulation
        Matrix matrix = new Matrix();
        // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);
        // recreate the new Bitmap
        Bitmap resizedBitmap = Bitmap.createBitmap(image, 0, 0, width, height,
                matrix, false);
        return resizedBitmap;
    }

    private String submitCampaign() {
        //Looper.prepare();
        MultipartEntity multiPart = new MultipartEntity();

        File sourceFile = null;
        if(filePath != null){
            sourceFile = new File(filePath);
        }

        try {
            // sending parameters
            HashMap<String, String> hashMap = Constants.getSHA256();
            if(hashMap != null){
                multiPart.addPart("username", new StringBody(hashMap.get("username")));
                multiPart.addPart("access_password", new StringBody(hashMap.get("access_password")));
                multiPart.addPart("key", new StringBody(hashMap.get("key")));
                multiPart.addPart("signature", new StringBody(hashMap.get("signature")));
            }
            multiPart.addPart("campaign_id", new StringBody(MyPref.getCampId(this)));
            multiPart.addPart("member_id", new StringBody(MyPref.getId(this)));
            multiPart.addPart("description", new StringBody(descTxt.getText().toString()));
            if(visible.isChecked()) {
                multiPart.addPart("check_visible", new StringBody("1"));
            }
            else{
                multiPart.addPart("check_visible", new StringBody("0"));
            }
            if(onePayment.isChecked()) {
                multiPart.addPart("one_payment", new StringBody("1"));
            }
            else{
                multiPart.addPart("one_payment", new StringBody("0"));
            }

            if(unlimited.isChecked()){
                multiPart.addPart("unlimited", new StringBody("1"));
            }
            else{
                multiPart.addPart("unlimited", new StringBody("0"));
            }
            if(show_paid.isChecked()){
                multiPart.addPart("show_paid", new StringBody("1"));
            }
            else{
                multiPart.addPart("show_paid", new StringBody("0"));
            }

            if(categoryTxt.getText().toString().toLowerCase(Locale.getDefault()).contains("other")){
                multiPart.addPart("category", new StringBody("0"));
            }
            else {
                Log.e("tag", ""+categoryModels.get(selectedCat).getId());
                multiPart.addPart("category", new StringBody(categoryModels.get(selectedCat).getId()));
            }
            if(subcategoryTxt.getText().toString().toLowerCase(Locale.getDefault()).contains("other")){
                multiPart.addPart("sub_category", new StringBody("0"));
            }
            else {
                Log.e("tag", ""+subcategoryModels.get(selectedSubcat).getId());
                multiPart.addPart("sub_category", new StringBody(subcategoryModels.get(selectedSubcat).getId()));
            }

            if(sourceFile != null) {
                multiPart.addPart("image", new FileBody(sourceFile));
            }

            String response = multipost(Constants.edit_campaign, multiPart);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
        }

        //Looper.loop();

        return null;
    }

    private static String multipost(String urlString, MultipartEntity reqEntity) {
        try {
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(30000);
            conn.setRequestMethod("POST");
            conn.setUseCaches(false);
            //conn.setChunkedStreamingMode(1024);
            conn.setDoInput(true);
            conn.setDoOutput(true);

            conn.setRequestProperty("connection", "Keep-Alive");
            conn.addRequestProperty("Content-length", reqEntity.getContentLength()+"");
            conn.setRequestProperty("Cache-Control", "no-cache");
            conn.addRequestProperty(reqEntity.getContentType().getName(), reqEntity.getContentType().getValue());

            OutputStream os = conn.getOutputStream();
            reqEntity.writeTo(conn.getOutputStream());
            os.close();
            conn.connect();
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                return readStream(conn.getInputStream());
            }
            else{
                Log.e("tag", "code: "+conn.getResponseCode());
                Log.e("tag", "msg: "+conn.getResponseMessage());
                Log.e("tag", "cannot read stream");
            }
        }
        catch (Exception e) {
            Log.e("Uploading", "multipart post error " + e + "(" + urlString + ")");
        }
        return null;
    }

    private static String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuilder builder = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return builder.toString();
    }

    private File convertBitmapIntoFile(Bitmap bitmap){
        Random r = new Random();
        int no = 10000 + r.nextInt(99999)+r.nextInt(99999);
        File filesDir = getFilesDir();
        File imageFile = new File(filesDir, no + ".jpg");
        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            //Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
        return imageFile;
    }

    public void openMenu(View view) {
        super.drawerToggle();
    }

    public void titleFocus(View view) {
        titleTxt.requestFocus();
    }

    public void descFocus(View view) {
        descTxt.requestFocus();
    }

    private void openGallery() {
        // gallery code
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 11);
        }
        else {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            try {
                startActivityForResult(intent, 100);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(this, "Please install a File Manager.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void openCamera() {
        File extDir = Environment.getExternalStorageDirectory();
        Random r = new Random();
        String name = (r.nextInt(100000000)+r.nextInt(100000000))+".jpg";
        cameraFile = new File(extDir, name);
        if(checkExternalStorage()){
            String text ="writing into externanal storage";
            FileOutputStream fos;
            try {
                fos = new FileOutputStream(cameraFile);
                fos.write(text.getBytes());
                fos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(cameraFile.isFile()){
                Log.e("tag", "is file");
                Intent cameraintent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                cameraintent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cameraFile));
                startActivityForResult(cameraintent, 200);
            }
            else{
                Log.e("tag", "oops not file");
            }
        }
        else{
            AppMsg.makeText(this, "External storage not available!", AppMsg.STYLE_ALERT).show();
        }
    }

    public boolean checkExternalStorage() {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 100:
                if(resultCode==RESULT_OK){
                    if(data != null) {
                        Log.e("tag", "uri: "+data.getData());
                        Uri uri = data.getData();
                        String path = data.getData().getPath();
                        if (Build.VERSION.SDK_INT < 11) {
                            filePath = GetFilePathFromDevice.getPath(this, uri);
                            //filePath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, data.getData());
                        }
                        else if (Build.VERSION.SDK_INT < 19) {
                            //filePath = RealPathUtil.getRealPathFromURI_API11to18(this, data.getData());
                            filePath = GetFilePathFromDevice.getPath(this, uri);
                        }
                        else {
                            filePath = GetFilePathFromDevice.getPath(this, uri);;
                            //filePath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());
                        }
                        Log.e("File path", "File Path: " + filePath);
                        Bitmap bmp = BitmapFactory.decodeFile(filePath);
                        File file = new File(filePath);
                        Log.e("tagg", "size: "+(file.length()/(1024*1024)));
                        if(bmp != null) {
                            showSmallIcon();
                            showImageCropper(uri);
                        }
                    }
                }
                break;
            case 200:
                if(data != null) {
                    Uri uri = null;
                    if (data.getData() == null) {
                        //from camera
                        if(cameraFile == null)
                            return;
                        uri = Uri.fromFile(cameraFile);
                    }
                    else{
                        uri = data.getData();
                    }
                    Log.e("tag", "uri: "+uri);
                    if (Build.VERSION.SDK_INT < 11){
                        filePath = GetFilePathFromDevice.getPath(this, uri);;
                        //filePath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, data.getData());

                    }else if (Build.VERSION.SDK_INT < 19){
                        //filePath = RealPathUtil.getRealPathFromURI_API11to18(this, data.getData());
                        filePath = GetFilePathFromDevice.getPath(this, uri);;
                    }
                    else{
                        filePath = GetFilePathFromDevice.getPath(this, uri);;
                        //filePath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());
                    }
                    Log.e("File path", "File Path: " + filePath);

                    Bitmap bmp = BitmapFactory.decodeFile(filePath);
                    if(bmp != null) {
                        showSmallIcon();
                        showImageCropper(uri);
                    }
                }
                else{
                    Uri uri = null;
                    if(cameraFile == null) {
                        AppMsg.makeText(this, "Unable to fetch photo!", AppMsg.STYLE_ALERT).show();
                        return;
                    }
                    uri = Uri.fromFile(cameraFile);
                    Log.e("tag", "uri: "+uri);
                    if (Build.VERSION.SDK_INT < 11){
                        filePath = GetFilePathFromDevice.getPath(this, uri);;
                        //filePath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, data.getData());
                    }
                    else if (Build.VERSION.SDK_INT < 19){
                        //filePath = RealPathUtil.getRealPathFromURI_API11to18(this, data.getData());
                        filePath = GetFilePathFromDevice.getPath(this, uri);;
                    }
                    else{
                        filePath = GetFilePathFromDevice.getPath(this, uri);;
                        //filePath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());
                    }

                    Log.e("File path", "File Path: " + filePath);

                    Bitmap bmp = BitmapFactory.decodeFile(filePath);
                    if(bmp != null) {
                        showSmallIcon();
                        showImageCropper(uri);
                    }
                    else{
                        AppMsg.makeText(this, "Cannot retrieve photo!", AppMsg.STYLE_ALERT).show();
                    }
                }
                break;
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri uri = result.getUri();
                    Log.e("tag", "uri: "+uri);
                    if (Build.VERSION.SDK_INT < 11){
                        filePath = GetFilePathFromDevice.getPath(this, uri);;
                        //filePath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, data.getData());
                    }
                    else if (Build.VERSION.SDK_INT < 19){
                        //filePath = RealPathUtil.getRealPathFromURI_API11to18(this, data.getData());
                        filePath = GetFilePathFromDevice.getPath(this, uri);;
                    }
                    else{
                        filePath = GetFilePathFromDevice.getPath(this, uri);;
                        //filePath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());
                    }
                    Bitmap bmp = BitmapFactory.decodeFile(filePath);
                    Log.e("tagg", "w: "+bmp.getWidth());
                    Log.e("tagg", "h: "+bmp.getHeight());
                    if(bmp.getWidth()<500 || bmp.getHeight()<500){
                        filePath = null;
                        photo.setImageBitmap(null);
                        AppMsg.makeText(EditCampaign.this, "Image too small. Should be at least 500x500!", AppMsg.STYLE_ALERT).show();
                    }
                    if(bmp.getWidth()>2500 && bmp.getHeight()>2500){
                        filePath = null;
                        photo.setImageBitmap(null);
                        AppMsg.makeText(EditCampaign.this, "Image too large!", AppMsg.STYLE_ALERT).show();
                    }
                }
                else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                    showImageCropper(uri);
                }
                else{
                    Log.e("tagg", "cropped cancelled");
                    Toast.makeText(this, "Cropping is required for better image usage!", Toast.LENGTH_SHORT).show();
                    showImageCropper(uri);
                }

            default:
                break;
        }

    }

    private void getCategories() {
        showProgress(true);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.campaign_categories, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(true);
                try {
                    String status = response.getString("status");
                    String msg = response.getString("message");
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray categories = response.getJSONArray("campaign_categories");
                        for (int i = 0; i < categories.length(); i++) {
                            JSONObject object = categories.getJSONObject(i);
                            CategoryModel categoryModel = new CategoryModel();
                            categoryModel.setId(object.getString("id"));
                            categoryModel.setDate(object.getString("created_date"));
                            categoryModel.setUrlTitle(object.getString("url_title"));
                            categoryModel.setTitle(object.getString("title"));
                            categoryModel.setActive(object.getString("active"));
                            categoryModels.add(categoryModel);
                            categoryModelsTxt.add(categoryModel.getTitle());
                            if(categoryModel.getId().equals(selectedCatId)){
                                categoryTxt.setText(categoryModel.getTitle());
                                selectedCat = i;
                            }
                        }
                        category.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showCategoryDialog();
                            }
                        });
                        categoryTxt.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showCategoryDialog();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    private void showCategoryDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Category");
        String[] codes = new String[categoryModelsTxt.size()];
        for (int i = 0; i < categoryModelsTxt.size(); i++) {
            codes[i] = categoryModelsTxt.get(i);
        }
        builder.setSingleChoiceItems(codes, selectedCat, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedCat = which;
                if(categoryModelsTxt.get(which).length() > 30 ) {
                    categoryTxt.setText(categoryModelsTxt.get(which).substring(0, 29)+"...");
                }
                else{
                    categoryTxt.setText(categoryModelsTxt.get(which));
                }
                subcategoryTxt.setText("");
                getSubCategories(categoryModels.get(which).getId());
                dialog.dismiss();
            }
        });
        builder.create();
        builder.show();
    }

    private void getSubCategories(String catId) {
        showProgress(false);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("category_id", catId);
        Log.e("tag", "id: "+catId);

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.campaign_sub_categories, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                hideProgress(false);
                try {
                    String status = response.getString("status");
                    String msg = response.getString("message");
                    if (status.equalsIgnoreCase("success")) {
                        subcategoryModels.clear();
                        subcategoryModelsTxt.clear();
                        JSONArray categories = response.getJSONArray("campaign_sub_categories");
                        for (int i = 0; i < categories.length(); i++) {
                            JSONObject object = categories.getJSONObject(i);
                            SubCategoryModel categoryModel = new SubCategoryModel();
                            categoryModel.setId(object.getString("id"));
                            categoryModel.setDate(object.getString("created_date"));
                            categoryModel.setUrlTitle(object.getString("url_title"));
                            categoryModel.setTitle(object.getString("title"));
                            categoryModel.setActive(object.getString("active"));
                            subcategoryModels.add(categoryModel);
                            subcategoryModelsTxt.add(categoryModel.getTitle());
                            if(categoryModel.getId().equals(selectedSubcatId)){
                                subcategoryTxt.setText(categoryModel.getTitle());
                                selectedSubcat = i;
                            }
                        }
                        subcategory.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showSubCategoryDialog();
                            }
                        });
                        subcategoryTxt.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showSubCategoryDialog();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress(false);
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(this).addToRequestQueue(customRequest, "", false);
    }

    private void showSubCategoryDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Subcategory");
        String[] codes = new String[subcategoryModelsTxt.size()];
        for (int i = 0; i < subcategoryModelsTxt.size(); i++) {
            codes[i] = subcategoryModelsTxt.get(i);
        }
        builder.setSingleChoiceItems(codes, selectedSubcat, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedSubcat = which;
                //subcategoryTxt.setText(subcategoryModelsTxt.get(which));
                if(subcategoryModelsTxt.get(which).length() > 30 ) {
                    subcategoryTxt.setText(subcategoryModelsTxt.get(which).substring(0, 29)+"...");
                }
                else{
                    subcategoryTxt.setText(subcategoryModelsTxt.get(which));
                }
                dialog.dismiss();
            }
        });
        builder.create();
        builder.show();
    }

    private void showSmallIcon() {
        photo.setImageResource(R.drawable.tick);
    }

    private void showImageCropper(Uri uri) {
        Log.e("tagg", "uri1: "+uri);
        this.uri = uri;
        CropImage.activity(uri)
                .setMinCropResultSize(500, 500)
                .setMaxCropResultSize(2500, 2500)
                .start(this);
    }

//    private File getCompressedFile(String filePath) {
//        try {
//            Bitmap bmp = BitmapFactory.decodeFile(filePath);
//            bmp = bmpAfterRotationCheck(bmp, filePath);
//            int width = 500, height = 500;
//            if(bmp.getWidth()<500){
//                width = bmp.getWidth();
//            }
//            if(bmp.getHeight()<500){
//                height = bmp.getHeight();
//            }
//            bmp = getResizedBitmap(bmp, height, width);
//            File sourceFile = convertBitmapIntoFile(bmp);
//            if (sourceFile.isFile()) {
//                Log.e("tag", "is file");
//            }
//            else {
//                Log.e("tag", "not a file");
//            }
//            return sourceFile;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    private Bitmap bmpAfterRotationCheck(Bitmap bmp, String filePath) {
//        try {
//            ExifInterface exif = new ExifInterface(filePath);
//            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
//            Log.e("EXIF", "Exif: " + orientation);
//            Matrix matrix = new Matrix();
//            if (orientation == 6) {
//                matrix.postRotate(90);
//            }
//            else if (orientation == 3) {
//                matrix.postRotate(180);
//            }
//            else if (orientation == 8) {
//                matrix.postRotate(270);
//            }
//            bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true); // rotating bitmap
//            return bmp;
//        }
//        catch (Exception e) {
//        }
//        return bmp;
//    }

}
