package com.deecoders.hdytak.fragment;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deecoders.hdytak.R;
import com.deecoders.hdytak.activity.AccountInfo;
import com.deecoders.hdytak.activity.AddItem;
import com.deecoders.hdytak.activity.BaseActivity;
import com.deecoders.hdytak.activity.Campaigns;
import com.deecoders.hdytak.activity.CreateCampaign;
import com.deecoders.hdytak.activity.Login;
import com.deecoders.hdytak.activity.MyCampaign;
import com.deecoders.hdytak.activity.PaymentsSent;
import com.deecoders.hdytak.model.CampaignModel;
import com.deecoders.hdytak.network.CustomRequest;
import com.deecoders.hdytak.network.VolleyLibrary;
import com.deecoders.hdytak.util.Constants;
import com.deecoders.hdytak.util.MyPref;
import com.deecoders.hdytak.view.MyTextView;
import com.facebook.login.LoginManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MainMenu extends Fragment {
    ImageView back;
    MyTextView account, addtolist, payments, website, logout, campaign, campaigns, scan, login;
    @BindView(R.id.loginPanel)
    LinearLayout loginPanel;
    LinearLayout loginSection;
    Unbinder unbinder;
    CampaignModel campaignModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_main_menu, container);
        back = (ImageView) view.findViewById(R.id.back);
        account = (MyTextView) view.findViewById(R.id.account);
        addtolist = (MyTextView) view.findViewById(R.id.addtolist);
        payments = (MyTextView) view.findViewById(R.id.payments);
        website = (MyTextView) view.findViewById(R.id.website);
        logout = (MyTextView) view.findViewById(R.id.logout);
        campaign = (MyTextView) view.findViewById(R.id.campaign);
        campaigns = (MyTextView) view.findViewById(R.id.campaigns);
        scan = (MyTextView) view.findViewById(R.id.scan);
        login = (MyTextView) view.findViewById(R.id.login);
        loginSection = view.findViewById(R.id.loginSection);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseActivity activity = (BaseActivity) getActivity();
                activity.closeDrawer();
                startActivity(new Intent(getActivity(), Login.class));
            }
        });
        scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseActivity activity = (BaseActivity) getActivity();
                //activity.closeDrawer();
                activity.scanQRCode();
            }
        });
        website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String urlString="https://www.hdytak.com";
                Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse(urlString));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setPackage("com.android.chrome");
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException ex) {
                    // Chrome browser presumably not installed so allow user to choose instead
                    intent.setPackage(null);
                    startActivity(intent);
                }
                BaseActivity activity = (BaseActivity) getActivity();
                activity.closeDrawer();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BaseActivity activity = (BaseActivity) getActivity();
                activity.closeDrawer();
            }
        });
        payments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(MyPref.getCampId(getActivity()) == null)
                    startActivity(new Intent(getActivity(), CreateCampaign.class));
                else {
                    Intent intent = new Intent(getActivity(), PaymentsSent.class);
                    intent.putExtra("camp_model", campaignModel);
                    startActivity(intent);
                }
                BaseActivity activity = (BaseActivity) getActivity();
                activity.closeDrawer();
            }
        });
        addtolist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(MyPref.getCampId(getActivity()) == null) {
                    startActivity(new Intent(getActivity(), CreateCampaign.class));
                }
                else {
                    if(campaignModel == null)
                        return;
                    Intent intent = new Intent(getActivity(), AddItem.class);
                    intent.putExtra("title", campaignModel.getTitle());
                    intent.putExtra("desc", campaignModel.getDescription());
                    intent.putExtra("image", campaignModel.getImage());
                    startActivity(intent);
                }

                BaseActivity activity = (BaseActivity) getActivity();
                activity.closeDrawer();
            }
        });
        account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(MyPref.getCampId(getActivity()) == null)
                    startActivity(new Intent(getActivity(), CreateCampaign.class));
                else
                    startActivity(new Intent(getActivity(), AccountInfo.class));
                BaseActivity activity = (BaseActivity) getActivity();
                activity.closeDrawer();
            }
        });
        campaign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(MyPref.getCampId(getActivity()) == null)
                    startActivity(new Intent(getActivity(), CreateCampaign.class));
                else
                    startActivity(new Intent(getActivity(), MyCampaign.class));
                BaseActivity activity = (BaseActivity) getActivity();
                activity.closeDrawer();
            }
        });
        campaigns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), Campaigns.class));
                BaseActivity activity = (BaseActivity) getActivity();
                activity.closeDrawer();
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyPref.setLogin(getActivity(), 0);
                MyPref.setId(getActivity(), null);
                MyPref.setCampId(getActivity(), null);
                MyPref.setEmail(getActivity(), null);
                MyPref.setUsername(getActivity(), null);
                try {
                    LoginManager.getInstance().logOut();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getActivity().finishAffinity();
                startActivity(new Intent(getActivity(), Campaigns.class));
            }
        });

        if(MyPref.getLogin(getActivity()) == 0){
            loginSection.setVisibility(View.VISIBLE);
            loginPanel.setVisibility(View.GONE);
        }
        else{
            loginSection.setVisibility(View.GONE);
            loginPanel.setVisibility(View.VISIBLE);
        }

        if(MyPref.getCampId(getActivity()) != null)
            campaignDetails();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void campaignDetails() {
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("campaign_id", ""+MyPref.getCampId(getActivity()));

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.campaign_details, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                try {
                    JSONObject data = response.getJSONObject("campaign");
                    campaignModel = new CampaignModel();
                    campaignModel.setId(data.getString("id"));
                    campaignModel.setTitle(data.getString("title"));
                    campaignModel.setDescription(data.getString("description"));
                    campaignModel.setImage(data.getString("image"));
                    campaignModel.setUrlTitle(data.getString("url_title"));
                    campaignModel.setVisible(data.getString("visible"));
                    campaignModel.setOnePayment(data.getString("one_payment"));
                    campaignModel.setQrImage(data.getString("qr_image"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(getActivity()).addToRequestQueue(customRequest, "", false);
    }
}
