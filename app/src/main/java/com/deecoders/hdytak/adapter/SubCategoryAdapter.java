package com.deecoders.hdytak.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.deecoders.hdytak.R;
import com.deecoders.hdytak.model.SubCategoryModel;

import java.util.ArrayList;

/**
 * Created by saif on 2/17/2018.
 */

public class SubCategoryAdapter extends ArrayAdapter<SubCategoryModel> {
    private ArrayList<SubCategoryModel> models = new ArrayList<>();
    private LayoutInflater mInflater;

    public SubCategoryAdapter(Context context, int resource, ArrayList<SubCategoryModel> items) {
        super(context, resource, items);
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        models = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.category_group_item, null);
            holder.text = (TextView)convertView.findViewById(R.id.text);
            holder.line = convertView.findViewById(R.id.line);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        holder.text.setText(models.get(position).getTitle());

        if(position == models.size()-1){
            holder.line.setVisibility(View.GONE);
        }
        else{
            holder.line.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    public static class ViewHolder {
        public TextView text;
        public View line;
    }

    public void setModels(ArrayList<SubCategoryModel> models){
        this.models = models;
        notifyDataSetChanged();
    }
}

