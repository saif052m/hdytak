package com.deecoders.hdytak.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.deecoders.hdytak.R;
import com.deecoders.hdytak.activity.CampaignDetails;
import com.deecoders.hdytak.activity.PayNow;
import com.deecoders.hdytak.activity.ProductDetails;
import com.deecoders.hdytak.model.CampaignModel;
import com.deecoders.hdytak.model.ProductModel;
import com.deecoders.hdytak.network.LruBitmapCache;
import com.deecoders.hdytak.util.Constants;
import com.deecoders.hdytak.util.CustomNetworkImageView;
import com.deecoders.hdytak.view.MyTextView;

import java.util.ArrayList;

/**
 * Created by saif on 2/17/2018.
 */

public class CampaignDetailsAdapter extends ArrayAdapter<CampaignModel> {
    private ArrayList<CampaignModel> models = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;
    RequestQueue mRequestQueue;
    ImageLoader mImageLoader;
    Typeface tf1, tf2;

    public CampaignDetailsAdapter(Context context, int resource, ArrayList<CampaignModel> items) {
        super(context, resource, items);
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        models = items;
        this.context = context;
        mRequestQueue = Volley.newRequestQueue(context);
        mImageLoader = new ImageLoader(mRequestQueue, new LruBitmapCache());
        tf1 = Typeface.createFromAsset(getContext().getAssets(), "fonts/amatic_sc.otf");
        tf2 = Typeface.createFromAsset(getContext().getAssets(), "fonts/arabic_font.OTF");
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.campaign_item, null);
            holder.imageView = (CustomNetworkImageView) convertView.findViewById(R.id.image);
            holder.title = (MyTextView) convertView.findViewById(R.id.title);
            holder.viewMore = (MyTextView) convertView.findViewById(R.id.viewMore);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder)convertView.getTag();
        }

        CampaignModel model = models.get(position);
        holder.imageView.setDefaultImageResId(R.drawable.loading_2);
        holder.imageView.setImageUrl(Constants.campaignImageUrl+model.getImage(), mImageLoader);
        if(isArabic(model.getTitle())){
            holder.title.setTypeface(tf2, 1);
            holder.title.setTextSize(15);
        }
        else{
            holder.title.setTypeface(tf1, 1);
            holder.title.setTextSize(22);
        }
        holder.title.setText(model.getTitle());
        holder.viewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PayNow.class);
                intent.putExtra("camp_model", models.get(position));
                context.startActivity(intent);
            }
        });

        return convertView;
    }

    public static class ViewHolder {
        public CustomNetworkImageView imageView;
        public MyTextView title, viewMore;
    }

    public void setModels(ArrayList<CampaignModel> models){
        this.models = models;
        notifyDataSetChanged();
    }

    private boolean isArabic(String s){
        for (int i = 0; i < s.length();) {
            int c = s.codePointAt(i);
            if (c >= 0x0600 && c <= 0x06E0)
                return true;
            i += Character.charCount(c);
        }
        return false;
    }
}
