package com.deecoders.hdytak.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.deecoders.hdytak.R;

import java.util.List;

public class CountryAdapter extends ArrayAdapter<String> {

    LayoutInflater flater;

    public CountryAdapter(Activity context, int resouceId, int textviewId, List<String> list){
        super(context,resouceId,textviewId, list);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return rowview1(convertView, position);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return rowview2(convertView, position);
    }

    private View rowview1(View convertView , int position){
        Log.e("tag", "1");
        viewHolder holder ;
        View rowview = convertView;
        if (rowview==null) {
            holder = new viewHolder();
            flater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowview = flater.inflate(R.layout.country, null, false);
            holder.txtTitle = (TextView) rowview.findViewById(R.id.title);
            rowview.setTag(holder);
        }
        else{
            holder = (viewHolder) rowview.getTag();
        }

        holder.txtTitle.setText(getItem(position));

        return rowview;
    }

    private View rowview2(View convertView , int position){
        Log.e("tag", "2");
        String country = getItem(position);
        viewHolder holder ;
        View rowview = convertView;
        if (rowview==null) {
            holder = new viewHolder();
            flater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowview = flater.inflate(R.layout.country, null, false);
            holder.txtTitle = (TextView) rowview.findViewById(R.id.title);
            rowview.setTag(holder);
        }
        else{
            holder = (viewHolder) rowview.getTag();
        }
        holder.txtTitle.setText(country);
        return rowview;
    }

    private class viewHolder{
        TextView txtTitle;
    }
}
