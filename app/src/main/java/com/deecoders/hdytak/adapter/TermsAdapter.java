package com.deecoders.hdytak.adapter;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.deecoders.hdytak.R;
import com.deecoders.hdytak.model.CategoryModel;
import com.deecoders.hdytak.model.ProductModel;
import com.deecoders.hdytak.view.MyTextView;

import java.util.ArrayList;

/**
 * Created by saif on 2/17/2018.
 */

public class TermsAdapter extends ArrayAdapter<String> {
    private ArrayList<String> titles = new ArrayList<>();
    private ArrayList<String> desc = new ArrayList<>();
    private LayoutInflater mInflater;

    public TermsAdapter(Context context, int resource,  ArrayList<String> titles,  ArrayList<String> desc) {
        super(context, resource, titles);
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.titles = titles;
        this.desc = desc;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.terms_item, null);
            holder.title = (MyTextView)convertView.findViewById(R.id.title);
            holder.desc = (MyTextView)convertView.findViewById(R.id.desc);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        holder.title.setText(fromHtml(titles.get(position)));
        holder.desc.setText(fromHtml(desc.get(position)));

        return convertView;
    }

    public static class ViewHolder {
        public MyTextView title, desc;
    }

    public static Spanned fromHtml(String source) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(source);
        }
    }
}

