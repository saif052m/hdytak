package com.deecoders.hdytak.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.deecoders.hdytak.R;
import com.deecoders.hdytak.activity.BaseActivity;
import com.deecoders.hdytak.model.ProductModel;
import com.deecoders.hdytak.network.CustomRequest;
import com.deecoders.hdytak.network.LruBitmapCache;
import com.deecoders.hdytak.network.VolleyLibrary;
import com.deecoders.hdytak.util.Constants;
import com.deecoders.hdytak.util.CustomNetworkImageView;
import com.deecoders.hdytak.util.MyPref;
import com.deecoders.hdytak.view.MyTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by saif on 2/17/2018.
 */

public class MyCampaignProductAdapter extends ArrayAdapter<ProductModel> {
    private ArrayList<ProductModel> models = new ArrayList<>();
    private LayoutInflater mInflater;
    boolean showEditDelete;
    RequestQueue mRequestQueue;
    ImageLoader mImageLoader;
    Context context;
    Typeface tf1, tf2;

    public MyCampaignProductAdapter(Context context, int resource, ArrayList<ProductModel> items, boolean showEditDelete) {
        super(context, resource, items);
        this.context=context;
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        models = items;
        this.showEditDelete = showEditDelete;
        mRequestQueue = Volley.newRequestQueue(context);
        mImageLoader = new ImageLoader(mRequestQueue, new LruBitmapCache());
        tf1 = Typeface.createFromAsset(getContext().getAssets(), "fonts/amatic_sc.otf");
        tf2 = Typeface.createFromAsset(getContext().getAssets(), "fonts/arabic_font.OTF");

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.product_item1, null);
            holder.editDelete = (ImageView) convertView.findViewById(R.id.edit_delete);
            holder.imageView = (CustomNetworkImageView) convertView.findViewById(R.id.image);
            holder.title = (MyTextView) convertView.findViewById(R.id.title);
            holder.price = (MyTextView) convertView.findViewById(R.id.price);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        final ProductModel model = models.get(position);
        holder.imageView.setDefaultImageResId(R.drawable.loading_2);
        if(model.getType() == 1) {
            holder.imageView.setImageUrl(Constants.originalItemsImageUrl + model.getImage(), mImageLoader);
        }
        else if(model.getType() == 0){
            holder.imageView.setImageUrl(Constants.originalProductImageUrl + model.getImage(), mImageLoader);
        }

        //holder.title.setText(model.getTitle());
        String txt = model.getTitle();
        if(txt != null) {
            String[] arr = txt.split(" ");
            if (arr.length > 3) {
                txt = arr[0] + " " + arr[1] + " " + arr[2] + "...";
            }
        }
        if(isArabic(model.getTitle())){
            holder.title.setTypeface(tf2, 1);
            holder.title.setTextSize(15);
        }
        else{
            holder.title.setTypeface(tf1, 1);
            holder.title.setTextSize(20);
        }
        holder.price.setTypeface(tf1, 1);
        holder.price.setTextSize(20);
        holder.title.setText(txt);
        holder.price.setText(Constants.getFormatedAmount(model.getSalling_price())+"$");

        Log.e("tag", "url: "+Constants.originalItemsImageUrl+model.getImage());

        if(showEditDelete){
            holder.editDelete.setVisibility(View.VISIBLE);
            holder.editDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Confirm Delete!");
                    builder.setMessage("Are you sure you want to delete this product from campaign?");
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Log.e("Tag", "delete");
                            if(model.getType() == 1)
                                removeItem(model);
                            else if(model.getType() == 0)
                                removeProduct(model);
                            dialog.dismiss();
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                }
            });
        }
        else{
            holder.editDelete.setVisibility(View.GONE);
        }

        return convertView;
    }

    private boolean isArabic(String s){
        for (int i = 0; i < s.length();) {
            int c = s.codePointAt(i);
            if (c >= 0x0600 && c <= 0x06E0)
                return true;
            i += Character.charCount(c);
        }
        return false;
    }

    public static class ViewHolder {
        public ImageView editDelete;
        public CustomNetworkImageView imageView;
        public MyTextView title, price;
    }

    public void setModels(ArrayList<ProductModel> models){
        this.models = models;
        notifyDataSetChanged();
    }

    private void removeItem(final ProductModel model) {
        final BaseActivity activity = (BaseActivity)context;
        activity.showProgress(false);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("campaign_id", ""+ MyPref.getCampId(context));
        hashMap.put("member_id", ""+MyPref.getId(context));
        hashMap.put("product_id", ""+model.getId());

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.remove_item_from_campaign, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                activity.hideProgress(false);
                try {
                    String status = response.getString("status");
                    String message = response.getString("message");
                    if(status.equals("success")){
                        if(models.contains(model)) {
                            models.remove(model);
                            notifyDataSetChanged();
                        }
                    }
                    else{
                        Toast.makeText(context, ""+message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    activity.showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.hideProgress(false);
                activity.showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(context).addToRequestQueue(customRequest, "", false);
    }

    private void removeProduct(final ProductModel model) {
        final BaseActivity activity = (BaseActivity)context;
        activity.showProgress(false);
        HashMap<String, String> hashMap = Constants.getSHA256();
        if(hashMap == null)
            hashMap = new HashMap<>();
        hashMap.put("campaign_id", ""+ MyPref.getCampId(context));
        hashMap.put("member_id", ""+MyPref.getId(context));
        hashMap.put("product_id", ""+model.getId());

        System.out.println(Arrays.asList(hashMap));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.remove_from_campaign, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("success", response.toString());
                activity.hideProgress(false);
                try {
                    String status = response.getString("status");
                    String message = response.getString("message");
                    if(status.equals("success")){
                        if(models.contains(model)) {
                            models.remove(model);
                            notifyDataSetChanged();
                        }
                    }
                    else{
                        Toast.makeText(context, ""+message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    activity.showFailedAlert();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.hideProgress(false);
                activity.showFailedAlert();
                Log.e("error", error.toString());
            }
        });
        VolleyLibrary.getInstance(context).addToRequestQueue(customRequest, "", false);
    }

}

