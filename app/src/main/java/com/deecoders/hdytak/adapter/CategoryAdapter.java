package com.deecoders.hdytak.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.deecoders.hdytak.R;
import com.deecoders.hdytak.model.CategoryModel;
import com.deecoders.hdytak.model.ProductModel;

import java.util.ArrayList;

/**
 * Created by saif on 2/17/2018.
 */

public class CategoryAdapter extends ArrayAdapter<CategoryModel> {
    private ArrayList<CategoryModel> models = new ArrayList<>();
    private LayoutInflater mInflater;

    public CategoryAdapter(Context context, int resource, ArrayList<CategoryModel> items) {
        super(context, resource, items);
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        models = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.category_group, null);
            holder.text = (TextView)convertView.findViewById(R.id.text);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        holder.text.setText(models.get(position).getTitle());

        return convertView;
    }

    public static class ViewHolder {
        public TextView text;
    }

    public void setModels(ArrayList<CategoryModel> models){
        this.models = models;
        notifyDataSetChanged();
    }

}

