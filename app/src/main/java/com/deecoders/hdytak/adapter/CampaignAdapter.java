package com.deecoders.hdytak.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.deecoders.hdytak.R;
import com.deecoders.hdytak.model.CampaignModel;
import com.deecoders.hdytak.model.CategoryModel;
import com.deecoders.hdytak.model.ProductModel;

import java.util.ArrayList;

/**
 * Created by saif on 2/17/2018.
 */

public class CampaignAdapter extends ArrayAdapter<CampaignModel> {
    private ArrayList<CampaignModel> models = new ArrayList<>();
    private LayoutInflater mInflater;

    public CampaignAdapter(Context context, int resource, ArrayList<CampaignModel> items) {
        super(context, resource, items);
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        models = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.category_group, null);
            //holder.sno = (TextView)convertView.findViewById(R.id.sno);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        return convertView;
    }

    public static class ViewHolder {
        public TextView sno;
    }

    public void setModels(ArrayList<CampaignModel> models){
        this.models = models;
        notifyDataSetChanged();
    }

}

