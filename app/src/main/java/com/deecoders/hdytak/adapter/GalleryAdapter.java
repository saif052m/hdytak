package com.deecoders.hdytak.adapter;

/**
 * Created by saif on 3/22/2018.
 */

import android.content.Context;
import android.graphics.Movie;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.deecoders.hdytak.R;
import com.deecoders.hdytak.activity.BaseActivity;
import com.deecoders.hdytak.network.LruBitmapCache;
import com.deecoders.hdytak.util.Constants;
import com.deecoders.hdytak.util.CustomNetworkImageView;

import java.util.ArrayList;
import java.util.List;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.MyViewHolder> {
    private ArrayList<String> urls;
    RequestQueue mRequestQueue;
    ImageLoader mImageLoader;
    Context context;

    public GalleryAdapter(Context context, ArrayList<String> urls) {
        this.urls = urls;
        this.context = context;
        mRequestQueue = Volley.newRequestQueue(context);
        mImageLoader = new ImageLoader(mRequestQueue, new LruBitmapCache());
    }

    public void setData(ArrayList<String> urls){
        this.urls = urls;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CustomNetworkImageView imageView;
        public MyViewHolder(View view) {
            super(view);
            imageView = (CustomNetworkImageView) view.findViewById(R.id.image);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        String url = urls.get(position);
        holder.imageView.setDefaultImageResId(R.drawable.loading_1);
        if(position == 0) {
            holder.imageView.setImageUrl(Constants.productOriginalUrl + url, mImageLoader);
        }
        else {
            holder.imageView.setImageUrl(Constants.productGalleryOriginalUrl + url, mImageLoader);
        }

        holder.imageView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                BaseActivity activity = (BaseActivity) context;
                if(urls.size() > 1)
                    activity.showSuccessToast("Please scroll to see other photos.");
                else
                    activity.showSuccessToast("No more photos to view!");
            }
        });
    }

    @Override
    public int getItemCount() {
        return urls.size();
    }
}