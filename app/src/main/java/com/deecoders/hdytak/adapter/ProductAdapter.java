package com.deecoders.hdytak.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.deecoders.hdytak.R;
import com.deecoders.hdytak.model.ProductModel;
import com.deecoders.hdytak.network.LruBitmapCache;
import com.deecoders.hdytak.util.Constants;
import com.deecoders.hdytak.util.CustomNetworkImageView;
import com.deecoders.hdytak.view.MyTextView;

import java.util.ArrayList;

/**
 * Created by saif on 2/17/2018.
 */

public class ProductAdapter extends ArrayAdapter<ProductModel> {
    private ArrayList<ProductModel> models = new ArrayList<>();
    private LayoutInflater mInflater;
    RequestQueue mRequestQueue;
    ImageLoader mImageLoader;
    Context context;
    Typeface tf1, tf2;

    public ProductAdapter(Context context, int resource, ArrayList<ProductModel> items) {
        super(context, resource, items);
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        models = items;
        this.context=context;
        mRequestQueue = Volley.newRequestQueue(context);
        mImageLoader = new ImageLoader(mRequestQueue, new LruBitmapCache());
        tf1 = Typeface.createFromAsset(getContext().getAssets(), "fonts/amatic_sc.otf");
        tf2 = Typeface.createFromAsset(getContext().getAssets(), "fonts/arabic_font.OTF");
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.product_item, null);
            holder.imageView = (CustomNetworkImageView) convertView.findViewById(R.id.image);
            holder.title = (MyTextView) convertView.findViewById(R.id.title);
            holder.price = (MyTextView) convertView.findViewById(R.id.price);
            holder.viewMore = (MyTextView) convertView.findViewById(R.id.viewMore);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        ProductModel model = models.get(position);
        holder.imageView.setDefaultImageResId(R.drawable.loading_2);
        holder.imageView.setImageUrl(Constants.originalProductImageUrl+model.getImage(), mImageLoader);

        if(isArabic(model.getTitle())){
            holder.title.setTypeface(tf2, 1);
            holder.title.setTextSize(15);
        }
        else{
            holder.title.setTypeface(tf1, 1);
            holder.title.setTextSize(20);
        }
        holder.price.setTypeface(tf1, 1);
        holder.price.setTextSize(20);

        holder.title.setText(model.getTitle());
        if(!model.getSalling_price().equals("0")) {
            holder.price.setVisibility(View.VISIBLE);
            holder.price.setText(Constants.getFormatedAmount(model.getSalling_price()) + "$");
        }
        else{
            holder.price.setVisibility(View.GONE);
        }

        return convertView;
    }

    private boolean isArabic(String s){
        for (int i = 0; i < s.length();) {
            int c = s.codePointAt(i);
            if (c >= 0x0600 && c <= 0x06E0)
                return true;
            i += Character.charCount(c);
        }
        return false;
    }

    public static class ViewHolder {
        public CustomNetworkImageView imageView;
        public TextView title, price, viewMore;
    }

    public void setModels(ArrayList<ProductModel> models){
        this.models = models;
        notifyDataSetChanged();
    }

}

