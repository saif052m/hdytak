package com.deecoders.hdytak.model;

import java.io.Serializable;

/**
 * Created by saif on 2/22/2018.
 */

public class ProductModel implements Serializable{
    private String id;
    private String created_date;
    private String title;
    private String salling_price;
    private String description;
    private String image;
    private String canAddToCamp;
    private String accessoriesRequired;
    private String minCharge;
    private String maxCharge;

    public String getMinCharge() {
        return minCharge;
    }

    public void setMinCharge(String minCharge) {
        this.minCharge = minCharge;
    }

    public String getMaxCharge() {
        return maxCharge;
    }

    public void setMaxCharge(String maxCharge) {
        this.maxCharge = maxCharge;
    }

    public String getUrlTitle() {
        return urlTitle;
    }

    public void setUrlTitle(String urlTitle) {
        this.urlTitle = urlTitle;
    }

    private String urlTitle;

    public String getCanAddToCamp() {
        return canAddToCamp;
    }

    public void setCanAddToCamp(String canAddToCamp) {
        this.canAddToCamp = canAddToCamp;
    }

    public String getAccessoriesRequired() {
        return accessoriesRequired;
    }

    public void setAccessoriesRequired(String accessoriesRequired) {
        this.accessoriesRequired = accessoriesRequired;
    }

    int type;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSalling_price() {
        return salling_price;
    }

    public void setSalling_price(String salling_price) {
        this.salling_price = salling_price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
