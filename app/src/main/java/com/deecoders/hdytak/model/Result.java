
package com.deecoders.hdytak.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("paid")
    @Expose
    private String paid;
    @SerializedName("remaining_value")
    @Expose
    private String remainingValue;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public String getRemainingValue() {
        return remainingValue;
    }

    public void setRemainingValue(String remainingValue) {
        this.remainingValue = remainingValue;
    }

}
