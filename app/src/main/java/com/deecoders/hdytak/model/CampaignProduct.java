
package com.deecoders.hdytak.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CampaignProduct extends ProductModel{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("url_title")
    @Expose
    private String urlTitle;
    @SerializedName("deleted")
    @Expose
    private String deleted;
    @SerializedName("sort_order")
    @Expose
    private String sortOrder;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("old_price")
    @Expose
    private String oldPrice;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("can_print_on_it")
    @Expose
    private String canPrintOnIt;
    @SerializedName("active")
    @Expose
    private String active;
    @SerializedName("video")
    @Expose
    private String video;
    @SerializedName("print_image_width")
    @Expose
    private String printImageWidth;
    @SerializedName("print_image_height")
    @Expose
    private String printImageHeight;
    @SerializedName("sale")
    @Expose
    private String sale;
    @SerializedName("new")
    @Expose
    private String _new;
    @SerializedName("special")
    @Expose
    private String special;
    @SerializedName("availability")
    @Expose
    private String availability;
    @SerializedName("sold")
    @Expose
    private String sold;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("short_description")
    @Expose
    private String shortDescription;
    @SerializedName("description_arabic")
    @Expose
    private String descriptionArabic;
    @SerializedName("short_description_arabic")
    @Expose
    private String shortDescriptionArabic;
    @SerializedName("sub_category_table")
    @Expose
    private String subCategoryTable;
    @SerializedName("title_arabic")
    @Expose
    private String titleArabic;
    @SerializedName("brands_table")
    @Expose
    private String brandsTable;
    @SerializedName("types_table")
    @Expose
    private String typesTable;
    @SerializedName("supplires_price")
    @Expose
    private String suppliresPrice;
    @SerializedName("supplires_table")
    @Expose
    private String suppliresTable;
    @SerializedName("min_charge")
    @Expose
    private String minCharge;
    @SerializedName("max_charge")
    @Expose
    private String maxCharge;
    @SerializedName("can_add_to_campaign")
    @Expose
    private String canAddToCampaign;
    @SerializedName("views")
    @Expose
    private String views;
    @SerializedName("add_common_select")
    @Expose
    private String addCommonSelect;
    @SerializedName("accessories_required")
    @Expose
    private String accessoriesRequired;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUrlTitle() {
        return urlTitle;
    }

    public void setUrlTitle(String urlTitle) {
        this.urlTitle = urlTitle;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(String oldPrice) {
        this.oldPrice = oldPrice;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCanPrintOnIt() {
        return canPrintOnIt;
    }

    public void setCanPrintOnIt(String canPrintOnIt) {
        this.canPrintOnIt = canPrintOnIt;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getPrintImageWidth() {
        return printImageWidth;
    }

    public void setPrintImageWidth(String printImageWidth) {
        this.printImageWidth = printImageWidth;
    }

    public String getPrintImageHeight() {
        return printImageHeight;
    }

    public void setPrintImageHeight(String printImageHeight) {
        this.printImageHeight = printImageHeight;
    }

    public String getSale() {
        return sale;
    }

    public void setSale(String sale) {
        this.sale = sale;
    }

    public String getNew() {
        return _new;
    }

    public void setNew(String _new) {
        this._new = _new;
    }

    public String getSpecial() {
        return special;
    }

    public void setSpecial(String special) {
        this.special = special;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getSold() {
        return sold;
    }

    public void setSold(String sold) {
        this.sold = sold;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDescriptionArabic() {
        return descriptionArabic;
    }

    public void setDescriptionArabic(String descriptionArabic) {
        this.descriptionArabic = descriptionArabic;
    }

    public String getShortDescriptionArabic() {
        return shortDescriptionArabic;
    }

    public void setShortDescriptionArabic(String shortDescriptionArabic) {
        this.shortDescriptionArabic = shortDescriptionArabic;
    }

    public String getSubCategoryTable() {
        return subCategoryTable;
    }

    public void setSubCategoryTable(String subCategoryTable) {
        this.subCategoryTable = subCategoryTable;
    }

    public String getTitleArabic() {
        return titleArabic;
    }

    public void setTitleArabic(String titleArabic) {
        this.titleArabic = titleArabic;
    }

    public String getBrandsTable() {
        return brandsTable;
    }

    public void setBrandsTable(String brandsTable) {
        this.brandsTable = brandsTable;
    }

    public String getTypesTable() {
        return typesTable;
    }

    public void setTypesTable(String typesTable) {
        this.typesTable = typesTable;
    }

    public String getSuppliresPrice() {
        return suppliresPrice;
    }

    public void setSuppliresPrice(String suppliresPrice) {
        this.suppliresPrice = suppliresPrice;
    }

    public String getSuppliresTable() {
        return suppliresTable;
    }

    public void setSuppliresTable(String suppliresTable) {
        this.suppliresTable = suppliresTable;
    }

    public String getMinCharge() {
        return minCharge;
    }

    public void setMinCharge(String minCharge) {
        this.minCharge = minCharge;
    }

    public String getMaxCharge() {
        return maxCharge;
    }

    public void setMaxCharge(String maxCharge) {
        this.maxCharge = maxCharge;
    }

    public String getCanAddToCampaign() {
        return canAddToCampaign;
    }

    public void setCanAddToCampaign(String canAddToCampaign) {
        this.canAddToCampaign = canAddToCampaign;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getAddCommonSelect() {
        return addCommonSelect;
    }

    public void setAddCommonSelect(String addCommonSelect) {
        this.addCommonSelect = addCommonSelect;
    }

    public String getAccessoriesRequired() {
        return accessoriesRequired;
    }

    public void setAccessoriesRequired(String accessoriesRequired) {
        this.accessoriesRequired = accessoriesRequired;
    }

}
