
package com.deecoders.hdytak.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Country {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("url_title")
    @Expose
    private String urlTitle;
    @SerializedName("deleted")
    @Expose
    private String deleted;
    @SerializedName("sort_order")
    @Expose
    private String sortOrder;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("sortname")
    @Expose
    private String sortname;
    @SerializedName("phonecode")
    @Expose
    private String phonecode;
    @SerializedName("active")
    @Expose
    private String active;
    @SerializedName("fast")
    @Expose
    private String fast;
    @SerializedName("normal")
    @Expose
    private String normal;
    @SerializedName("free")
    @Expose
    private String free;
    @SerializedName("active_shipping")
    @Expose
    private String activeShipping;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUrlTitle() {
        return urlTitle;
    }

    public void setUrlTitle(String urlTitle) {
        this.urlTitle = urlTitle;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSortname() {
        return sortname;
    }

    public void setSortname(String sortname) {
        this.sortname = sortname;
    }

    public String getPhonecode() {
        return phonecode;
    }

    public void setPhonecode(String phonecode) {
        this.phonecode = phonecode;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getFast() {
        return fast;
    }

    public void setFast(String fast) {
        this.fast = fast;
    }

    public String getNormal() {
        return normal;
    }

    public void setNormal(String normal) {
        this.normal = normal;
    }

    public String getFree() {
        return free;
    }

    public void setFree(String free) {
        this.free = free;
    }

    public String getActiveShipping() {
        return activeShipping;
    }

    public void setActiveShipping(String activeShipping) {
        this.activeShipping = activeShipping;
    }

}
