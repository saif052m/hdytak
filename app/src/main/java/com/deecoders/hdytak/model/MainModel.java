
package com.deecoders.hdytak.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MainModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private Result result;
    @SerializedName("user_account")
    @Expose
    private UserAccount userAccount;
    @SerializedName("country")
    @Expose
    private Country country;

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
    @SerializedName("accessories_selected")
    @Expose
    private List<AccessoriesSelected> accessoriesSelected = null;
    @SerializedName("details")
    @Expose
    private List<Detail> details = null;
    @SerializedName("campaign_products")
    @Expose
    private List<CampaignProduct> campaignProducts = null;
    @SerializedName("campaign_items")
    @Expose
    private List<Object> campaignItems = null;


    public List<AccessoriesSelected> getAccessoriesSelected() {
        return accessoriesSelected;
    }

    public void setAccessoriesSelected(List<AccessoriesSelected> accessoriesSelected) {
        this.accessoriesSelected = accessoriesSelected;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public void setDetails(List<Detail> details) {
        this.details = details;
    }

    public List<CampaignProduct> getCampaignProducts() {
        return campaignProducts;
    }

    public void setCampaignProducts(List<CampaignProduct> campaignProducts) {
        this.campaignProducts = campaignProducts;
    }

    public List<Object> getCampaignItems() {
        return campaignItems;
    }

    public void setCampaignItems(List<Object> campaignItems) {
        this.campaignItems = campaignItems;
    }

}
