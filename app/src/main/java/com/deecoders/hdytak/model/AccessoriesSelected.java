package com.deecoders.hdytak.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AccessoriesSelected {

    @Expose
    @SerializedName("selected")
    public String selected;
    @Expose
    @SerializedName("price")
    public String price;
    @Expose
    @SerializedName("active")
    public String active;
    @Expose
    @SerializedName("title_arabic")
    public String title_arabic;
    @Expose
    @SerializedName("title")
    public String title;
    @Expose
    @SerializedName("sort_order")
    public String sort_order;
    @Expose
    @SerializedName("deleted")
    public String deleted;
    @Expose
    @SerializedName("url_title")
    public String url_title;
    @Expose
    @SerializedName("created_date")
    public String created_date;
    @Expose
    @SerializedName("id")
    public String id;

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getTitle_arabic() {
        return title_arabic;
    }

    public void setTitle_arabic(String title_arabic) {
        this.title_arabic = title_arabic;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSort_order() {
        return sort_order;
    }

    public void setSort_order(String sort_order) {
        this.sort_order = sort_order;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUrl_title() {
        return url_title;
    }

    public void setUrl_title(String url_title) {
        this.url_title = url_title;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
