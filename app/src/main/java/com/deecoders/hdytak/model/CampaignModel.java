package com.deecoders.hdytak.model;

import java.io.Serializable;

/**
 * Created by saif on 3/1/2018.
 */

public class CampaignModel implements Serializable{
    private String id;
    private String created_date;
    private String title;
    private String description;

    public String getQrImage() {
        return qrImage;
    }

    public void setQrImage(String qrImage) {
        this.qrImage = qrImage;
    }

    private String image;
    private String visible;
    private String onePayment;
    private String qrImage;

    public String getUrlTitle() {
        return urlTitle;
    }

    public void setUrlTitle(String urlTitle) {
        this.urlTitle = urlTitle;
    }

    private String urlTitle;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getVisible() {
        return visible;
    }

    public void setVisible(String visible) {
        this.visible = visible;
    }

    public String getOnePayment() {
        return onePayment;
    }

    public void setOnePayment(String onePayment) {
        this.onePayment = onePayment;
    }
}
