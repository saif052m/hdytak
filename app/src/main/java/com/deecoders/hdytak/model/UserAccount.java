
package com.deecoders.hdytak.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserAccount {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("url_title")
    @Expose
    private String urlTitle;
    @SerializedName("deleted")
    @Expose
    private String deleted;
    @SerializedName("sort_order")
    @Expose
    private String sortOrder;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("active")
    @Expose
    private String active;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("geo_ip")
    @Expose
    private String geoIp;
    @SerializedName("geo_city")
    @Expose
    private String geoCity;
    @SerializedName("geo_state")
    @Expose
    private String geoState;
    @SerializedName("geo_country")
    @Expose
    private String geoCountry;
    @SerializedName("geo_country_code")
    @Expose
    private String geoCountryCode;
    @SerializedName("geo_continent")
    @Expose
    private String geoContinent;
    @SerializedName("geo_continent_code")
    @Expose
    private String geoContinentCode;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("balance")
    @Expose
    private String balance;
    @SerializedName("birthday")
    @Expose
    private String birthday;
    @SerializedName("bank_name")
    @Expose
    private String bankName;
    @SerializedName("bank_branch")
    @Expose
    private String bankBranch;
    @SerializedName("bank_account_name")
    @Expose
    private String bankAccountName;
    @SerializedName("bank_iban_usd")
    @Expose
    private String bankIbanUsd;
    @SerializedName("bank_iban_local")
    @Expose
    private String bankIbanLocal;
    @SerializedName("countries_table")
    @Expose
    private String countriesTable;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUrlTitle() {
        return urlTitle;
    }

    public void setUrlTitle(String urlTitle) {
        this.urlTitle = urlTitle;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getGeoIp() {
        return geoIp;
    }

    public void setGeoIp(String geoIp) {
        this.geoIp = geoIp;
    }

    public String getGeoCity() {
        return geoCity;
    }

    public void setGeoCity(String geoCity) {
        this.geoCity = geoCity;
    }

    public String getGeoState() {
        return geoState;
    }

    public void setGeoState(String geoState) {
        this.geoState = geoState;
    }

    public String getGeoCountry() {
        return geoCountry;
    }

    public void setGeoCountry(String geoCountry) {
        this.geoCountry = geoCountry;
    }

    public String getGeoCountryCode() {
        return geoCountryCode;
    }

    public void setGeoCountryCode(String geoCountryCode) {
        this.geoCountryCode = geoCountryCode;
    }

    public String getGeoContinent() {
        return geoContinent;
    }

    public void setGeoContinent(String geoContinent) {
        this.geoContinent = geoContinent;
    }

    public String getGeoContinentCode() {
        return geoContinentCode;
    }

    public void setGeoContinentCode(String geoContinentCode) {
        this.geoContinentCode = geoContinentCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public String getBankIbanUsd() {
        return bankIbanUsd;
    }

    public void setBankIbanUsd(String bankIbanUsd) {
        this.bankIbanUsd = bankIbanUsd;
    }

    public String getBankIbanLocal() {
        return bankIbanLocal;
    }

    public void setBankIbanLocal(String bankIbanLocal) {
        this.bankIbanLocal = bankIbanLocal;
    }

    public String getCountriesTable() {
        return countriesTable;
    }

    public void setCountriesTable(String countriesTable) {
        this.countriesTable = countriesTable;
    }

}
