package com.deecoders.hdytak.view;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;

import java.util.regex.Pattern;

/**
 * Created by saif on 2/24/2018.
 */

public class MyEditText extends AppCompatEditText {
    Typeface tf2;

    public MyEditText(Context context) {
        super(context);
        init();
    }

    public MyEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MyEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    void init() {
        tf2 = Typeface.createFromAsset(getContext().getAssets(), "fonts/arabic_font.OTF");
        this.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String txt = s.toString();
                if(txt.toLowerCase().contains("a")
                        ||txt.toLowerCase().contains("e")
                        ||txt.toLowerCase().contains("i")
                        ||txt.toLowerCase().contains("o")
                        ||txt.toLowerCase().contains("u")){
                    return;
                }

                if(isArabic(txt)){
                    Log.e("tagg", "arabic: "+txt);
                    setTypeface(tf2, 1);
                    setTextSize(15);
                }
                invalidate();
            }
        });
    }

    private boolean isArabic(String s){
        for (int i = 0; i < s.length();) {
            int c = s.codePointAt(i);
            if (c >= 0x0600 && c <= 0x06E0)
                return true;
            i += Character.charCount(c);
        }
        return false;
    }

    public static final Pattern VALID_NAME_PATTERN_REGEX = Pattern.compile("[a-zA-Z_0-9]+$");

    public static boolean isEnglishWord(String string) {
        return VALID_NAME_PATTERN_REGEX.matcher(string).find();
    }
}
