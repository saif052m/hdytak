package com.deecoders.hdytak.view;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.AttributeSet;

/**
 * Created by saif on 2/24/2018.
 */

public class MyCheckBox extends AppCompatCheckBox {

    public MyCheckBox(Context context) {
        super(context);
        init();
    }

    public MyCheckBox(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MyCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/amatic_sc.otf");
        setTypeface(tf, 1);
    }
}
