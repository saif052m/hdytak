package com.deecoders.hdytak.view;

import android.content.Context;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.util.AttributeSet;

/**
 * Created by saif on 2/24/2018.
 */

public class MyAutoCompleteTextView extends AppCompatAutoCompleteTextView {

    public MyAutoCompleteTextView(Context context) {
        super(context);
        init();
    }

    public MyAutoCompleteTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MyAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    void init() {
        //Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "amatic_sc.ttf");
        //setTypeface(tf ,1);
        //setTextSize(14);
    }
}
